############################################################
# 		COMPILER OPTIONS
############################################################

WARN_FLAGS=-Wall -Wextra -Werror -Wshadow -Wpedantic
CFLAGS := -fPIC -std=c99 -D_GNU_SOURCE $(CFLAGS)

ifeq ($(origin DEBUG), undefined)
CFLAGS := -DNDEBUG -O3 $(CFLAGS)
else
CFLAGS := -g -O0 $(CFLAGS)
endif


############################################################
# 		BUILD TARGETS
############################################################

BIN=bin
BLD=build
LIB=lib
SRC=src

SRC_FILES=$(shell find $(SRC)/ -name '*.c' ! -name '*_main.c')
OBJ_FILES=$(patsubst $(SRC)/%.c,$(BLD)/%.o,$(SRC_FILES))

all: $(LIB)/libgrimoire.a $(LIB)/libgrimoire.so $(BIN)/datalog

$(LIB)/libgrimoire.a: $(OBJ_FILES) | $(LIB)
	$(AR) crs $@ $^

$(LIB)/libgrimoire.so: $(OBJ_FILES) | $(LIB)
	$(CC) $(WARN_FLAGS) $(CFLAGS) -shared -o $@ $^

$(BIN)/datalog: $(BLD)/datalog_main.o $(LIB)/libgrimoire.a | $(BIN)
	$(CC) $(WARN_FLAGS) $(CFLAGS) -o $@ $^ -lreadline

$(BIN) $(BLD) $(LIB):
	mkdir -p $@


############################################################
# 		OBJECT FILE DEPENDENCIES
############################################################

-include $(OBJ_FILES:.o=.d)

$(BLD)/%.o: $(SRC)/%.c | $(BLD)
	$(CC) $(WARN_FLAGS) $(CFLAGS) -MMD -c -o $@ $<


############################################################
# 		TEST CODE
############################################################

CHECK_LIBS=-lcheck -lm -lrt -lpthread -lsubunit

TEST_FILES=$(wildcard test/check_*.c)

check: $(TEST_FILES) $(LIB)/libgrimoire.a
	$(CC) -o $@ $(CFLAGS) -I$(SRC) $^ $(CHECK_LIBS)
	-./$@


############################################################
# 		CLEAN-UP
############################################################

clean:
	rm -rf $(BIN) $(BLD) $(LIB) check
