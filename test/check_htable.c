#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <stdio.h>
#include <htable.h>

START_TEST (test_dict)
{
	grm_dict_t h;
	ck_assert_int_eq(0, grm_dict_init(&h));
	int values[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	const char *keys[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
	for (int i = 0; i < 9; i++) {
		ck_assert_int_eq(0, grm_dict_set(&h, keys[i], values + i));
	}
	for (int i = 8; i >= 0; i--) {
		ck_assert_int_eq(values[i], *(int *)grm_dict_get(&h, keys[i]));
	}
	ck_assert_int_eq(0, grm_dict_free(&h));
}
END_TEST

START_TEST (test_dict_errors)
{
	grm_dict_t h;
	ck_assert_int_eq(0, grm_dict_init(&h));
	ck_assert_int_eq(0, grm_dict_set(&h, "hello", "world"));
	ck_assert_int_eq(1, grm_dict_set(&h, "hello", "all"));
	ck_assert_int_eq(-1, grm_dict_set(&h, "foobar", NULL));
	ck_assert_ptr_eq(NULL, grm_dict_get(&h, "foobar"));
	ck_assert_int_eq(0, grm_dict_free(&h));
}
END_TEST

START_TEST (test_set_union)
{
	grm_set_t s1, s2;
	ck_assert_int_eq(0, grm_set_init(&s1));
	ck_assert_int_eq(0, grm_set_init(&s2));
	int values[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

	ck_assert_int_eq(0, grm_set_add(&s1, values + 0, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 2, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 4, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 6, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 8, sizeof(int)));

	ck_assert_int_eq(0, grm_set_add(&s2, values + 1, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 3, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 4, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 5, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 6, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 7, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 9, sizeof(int)));

	ck_assert_int_eq(0, grm_set_union(&s1, &s2));
	for (size_t i = 0; i < 10; i++) {
		ck_assert_ptr_eq(values + i,
				grm_htable_get(&s1,
					(const unsigned char *)&i,
					sizeof(int)));
	}

	ck_assert_int_eq(0, grm_set_free(&s1));
	ck_assert_int_eq(0, grm_set_free(&s2));
}
END_TEST

START_TEST (test_set_diff)
{
	grm_set_t s1, s2, s3;
	ck_assert_int_eq(0, grm_set_init(&s1));
	ck_assert_int_eq(0, grm_set_init(&s2));
	ck_assert_int_eq(0, grm_set_init(&s3));
	int values[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

	ck_assert_int_eq(0, grm_set_add(&s1, values + 0, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 2, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 4, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 6, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s1, values + 8, sizeof(int)));

	ck_assert_int_eq(0, grm_set_add(&s2, values + 1, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 3, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 4, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 5, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 6, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 7, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s2, values + 9, sizeof(int)));

	ck_assert_int_eq(0, grm_set_add(&s3, values + 0, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s3, values + 2, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s3, values + 4, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s3, values + 6, sizeof(int)));
	ck_assert_int_eq(0, grm_set_add(&s3, values + 8, sizeof(int)));

	ck_assert_int_eq(0, grm_set_diff(&s1, &s2));
	for (size_t i = 0; i < 10; i++) {
		void *ptr = NULL;
		if (i == 0 || i == 2 || i == 8) {
			ptr = values + i;
		}
		ck_assert_ptr_eq(ptr, grm_htable_get(&s1,
					(const unsigned char *)&i,
					sizeof(int)));
	}

	ck_assert_int_eq(0, grm_set_diff(&s2, &s3));
	for (size_t i = 0; i < 10; i++) {
		void *ptr = NULL;
		if (i % 2 == 1) {
			ptr = values + i;
		}
		ck_assert_ptr_eq(ptr, grm_htable_get(&s2,
					(const unsigned char *)&i,
					sizeof(int)));
	}

	ck_assert_int_eq(0, grm_set_free(&s1));
	ck_assert_int_eq(0, grm_set_free(&s2));
	ck_assert_int_eq(0, grm_set_free(&s3));
}
END_TEST

/*
 * XXX The next 4 tests rely on understanding the htable internals.  If the
 * implemenation of the htable changes, then these tests may need to be
 * revisited.
 */

START_TEST (test_collision_resolution)
{
	grm_dict_t h;
	ck_assert_int_eq(0, grm_dict_init(&h));
	// XXX force the 9 keys into the first 2 buckets
	h.size = 2;
	int values[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	const char *keys[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
	for (int i = 0; i < 9; i++) {
		ck_assert_int_eq(0, grm_dict_set(&h, keys[i], values + i));
	}
	ck_assert_int_eq(1, grm_dict_set(&h, "9", values));
	for (int i = 8; i >= 0; i--) {
		ck_assert_int_eq(values[i], *(int *)grm_dict_get(&h, keys[i]));
	}
	ck_assert_int_eq(0, grm_dict_free(&h));
}
END_TEST

START_TEST (test_htable_iteration)
{
	grm_dict_t h;
	ck_assert_int_eq(0, grm_dict_init(&h));
	// check empty hash table
	ck_assert_ptr_eq(NULL, grm_htable_begin(&h, NULL));

	// XXX force the 9 keys into the first 4 buckets
	h.size = 4;
	int values[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	const char *keys[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
	for (int i = 0; i < 9; i++) {
		ck_assert_int_eq(0, grm_dict_set(&h, keys[i], values + i));
	}

	unsigned char *key;
	int *value;
	GRM_HTABLE_FOREACH(&key, value, &h) {
		ck_assert_ptr_eq(value, grm_dict_get(&h, (const char *)key));
	}
	// make sure we can do it when key is NULL without crashing
	GRM_HTABLE_FOREACH(NULL, value, &h) {
		ck_assert(!!value);
	}
	// make sure we can do it with values few and far between
	ck_assert_int_eq(0, grm_dict_free(&h));
	ck_assert_int_eq(0, grm_dict_init(&h));
	for (int i = 0; i < 4; i++) {
		ck_assert_int_eq(0, grm_dict_set(&h, keys[i], values + i));
	}
	GRM_HTABLE_FOREACH(&key, value, &h) {
		ck_assert_ptr_eq(value, grm_dict_get(&h, (const char *)key));
	}

	ck_assert_int_eq(0, grm_dict_free(&h));
}
END_TEST

START_TEST (test_dynamic_resizing)
{
	grm_dict_t h;
	ck_assert_int_eq(0, grm_dict_init(&h));
	// XXX force the htable to resize half-way through adding values
	h.resize_limit = 5;
	int values[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	const char *keys[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
	for (int i = 0; i < 9; i++) {
		ck_assert_int_eq(0, grm_dict_set(&h, keys[i], values + i));
	}
	for (int i = 8; i >= 0; i--) {
		ck_assert_int_eq(values[i], *(int *)grm_dict_get(&h, keys[i]));
	}
	ck_assert_int_eq(0, grm_dict_free(&h));
}
END_TEST

START_TEST (test_dict_del)
{
	grm_dict_t h;
	ck_assert_int_eq(0, grm_dict_init(&h));

	// XXX force the 9 keys into a single bucket
	h.size = 1;
	int values[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	// check the condition that only one entry is in a bucket
	ck_assert_int_eq(0, grm_dict_set(&h, "1", values));
	ck_assert_int_eq(0, grm_dict_del(&h, "1"));
	ck_assert_int_eq(1, grm_dict_del(&h, "1"));
	ck_assert_ptr_eq(NULL, grm_dict_get(&h, "1"));

	const char *keys[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
	for (int i = 0; i < 9; i++) {
		ck_assert_int_eq(0, grm_dict_set(&h, keys[i], values + i));
	}
	ck_assert_int_eq(1, *(int *)grm_dict_get(&h, "1"));

	// check removing from the front of the list
	ck_assert_int_eq(0, grm_dict_del(&h, "1"));
	ck_assert_int_eq(1, grm_dict_del(&h, "1"));
	ck_assert_ptr_eq(NULL, grm_dict_get(&h, "1"));
	for (int i = 8; i > 0; i--) {
		ck_assert_int_eq(values[i], *(int *)grm_dict_get(&h, keys[i]));
	}

	// check removing from the back of the list
	ck_assert_int_eq(0, grm_dict_del(&h, "9"));
	ck_assert_int_eq(1, grm_dict_del(&h, "9"));
	ck_assert_ptr_eq(NULL, grm_dict_get(&h, "9"));
	for (int i = 7; i > 0; i--) {
		ck_assert_int_eq(values[i], *(int *)grm_dict_get(&h, keys[i]));
	}

	// check removing from the middle of the list
	ck_assert_int_eq(0, grm_dict_del(&h, "5"));
	ck_assert_int_eq(1, grm_dict_del(&h, "5"));
	ck_assert_ptr_eq(NULL, grm_dict_get(&h, "5"));
	for (int i = 7; i > 0; i--) {
		if (i == 4) {
			continue;
		}
		ck_assert_int_eq(values[i], *(int *)grm_dict_get(&h, keys[i]));
	}

	ck_assert_int_eq(0, grm_dict_free(&h));
}
END_TEST

Suite *htable_suite (void)
{
	Suite *s = suite_create("HashTable");
	TCase *tc_htable = tcase_create("Core");
	tcase_add_test(tc_htable, test_dict);
	tcase_add_test(tc_htable, test_dict_errors);
	tcase_add_test(tc_htable, test_set_union);
	tcase_add_test(tc_htable, test_set_diff);
	tcase_add_test(tc_htable, test_collision_resolution);
	tcase_add_test(tc_htable, test_htable_iteration);
	tcase_add_test(tc_htable, test_dynamic_resizing);
	tcase_add_test(tc_htable, test_dict_del);
	suite_add_tcase(s, tc_htable);
	return s;
}

#pragma GCC diagnostic pop
