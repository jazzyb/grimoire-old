#include <check.h>
#include <stdlib.h>
#include <syslog.h>

extern Suite *lexer_suite(void);
extern Suite *vector_suite(void);
extern Suite *ast_suite(void);
extern Suite *parser_suite(void);
extern Suite *htable_suite(void);
extern Suite *fact_suite(void);
extern Suite *queue_suite(void);
extern Suite *rule_suite(void);
extern Suite *datalog_suite(void);
extern Suite *game_suite(void);
// DECLARE NEW SUITES HERE (DO NOT REMOVE THIS COMMENT)

int main (int agrc, char **argv)
{
	// log to stderr if logging is enabled
	openlog("Grimoire", LOG_PID | LOG_NDELAY | LOG_PERROR, LOG_USER);

	// NOTE:  This useless suite_create() is used to initialize the
	// Srunner so that I can comment out suites I don't want to run while
	// debugging.  I could have initialized it with NULL instead, but I
	// want all the suite names to print on their own line, and a empty
	// suite name will do that where NULL will not.
	SRunner *sr = srunner_create(suite_create(""));

	srunner_add_suite(sr, lexer_suite());
	srunner_add_suite(sr, vector_suite());
	srunner_add_suite(sr, ast_suite());
	srunner_add_suite(sr, parser_suite());
	srunner_add_suite(sr, htable_suite());
	srunner_add_suite(sr, fact_suite());
	srunner_add_suite(sr, queue_suite());
	srunner_add_suite(sr, rule_suite());
	srunner_add_suite(sr, datalog_suite());
	srunner_add_suite(sr, game_suite());
	// ADD NEW SUITES HERE (DO NOT REMOVE THIS COMMENT)

#ifndef NDEBUG
	// do not fork if we are debugging
	srunner_set_fork_status(sr, CK_NOFORK);
#endif

	srunner_run_all(sr, CK_NORMAL);
	int number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	closelog();
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
