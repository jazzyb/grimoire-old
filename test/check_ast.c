#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <ast.h>
#include <check.h>
#include <stdio.h>
#include <lexer.h>
#include <string.h>

START_TEST (test_add_children)
{
	grm_ast_t *root = grm_ast_new_root();
	ck_assert(!!root);
	grm_ast_t *child = grm_ast_add_child(root);
	ck_assert(!!child);
	ck_assert_ptr_eq(child, grm_ast_get_child(root, 0));
	ck_assert_ptr_eq(root, grm_ast_get_parent(child));
	ck_assert_ptr_eq(NULL, grm_ast_get_parent(root));
	ck_assert_int_eq(0, grm_ast_free_root(root));
}
END_TEST

START_TEST (test_multiple_children)
{
	grm_ast_t *level1[3];
	grm_ast_t *level2[20];

	grm_ast_t *root = grm_ast_new_root();
	ck_assert(!!root);

	for (size_t i = 0; i < 3; i++) {
		level1[i] = grm_ast_add_child(root);
		ck_assert(!!level1[i]);
	}

	int i = 0;
	grm_ast_t *child;
	GRM_AST_FOREACH_CHILD(child, root) {
		ck_assert_ptr_eq(child, level1[i++]);
	}

	for (size_t i = 0; i < 20; i++) {
		level2[i] = grm_ast_add_child(grm_ast_get_child(root, 1));
		ck_assert(!!level2[i]);
	}

	ck_assert_int_eq(24, grm_ast_num_nodes(root));

	i = 0;
	grm_ast_t *kid = grm_ast_get_child(root, 1);
	GRM_AST_FOREACH_CHILD(child, kid) {
		ck_assert_ptr_eq(child, level2[i++]);
	}

	kid = grm_ast_get_child(root, 0);
	GRM_AST_FOREACH_CHILD(child, kid) {
		ck_abort();
	}

	ck_assert_int_eq(0, grm_ast_free_root(root));
}
END_TEST

START_TEST (test_set_get_token)
{
	grm_token_t token = (grm_token_t){GRM_RULE_DEF, "<=", 0};
	grm_ast_t *root = grm_ast_new_root();
	ck_assert(!!root);
	ck_assert_int_eq(0, grm_ast_set_token(root, &token));
	ck_assert_int_eq(GRM_RULE_DEF, grm_ast_get_token(root)->type);
	ck_assert_str_eq(grm_ast_get_token(root)->lexeme, "<=");
	ck_assert_int_eq(0, grm_ast_free_root(root));
}
END_TEST

#include <parser.h>

#define check_ast_string(string)								\
{												\
	char dst[200];										\
	grm_parser_t p;										\
	grm_parser_init(&p, string);								\
	grm_ast_t *tree = grm_parser_parse(&p, NULL);						\
	ck_assert_int_ne(0, grm_ast_to_string(dst, sizeof(dst), grm_ast_get_child(tree, 0)));	\
	ck_assert_str_eq(dst, string);								\
	grm_ast_free_root(tree);								\
	grm_parser_free(&p);									\
}

START_TEST (test_ast_to_string)
{
	char src1[] = "(foo bar baz)";
	char src2[] = "(a (b c) d e (f g h))";
	char src3[] = "(xxx)";
	check_ast_string(src1);
	check_ast_string(src2);
	check_ast_string(src3);
}
END_TEST

Suite *ast_suite (void)
{
	Suite *s = suite_create("AbstractSyntaxTree");
	TCase *tc_ast = tcase_create("Core");
	tcase_add_test(tc_ast, test_add_children);
	tcase_add_test(tc_ast, test_multiple_children);
	tcase_add_test(tc_ast, test_set_get_token);
	tcase_add_test(tc_ast, test_ast_to_string);
	suite_add_tcase(s, tc_ast);
	return s;
}

#pragma GCC diagnostic pop
