#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <stdio.h>
#include <error.h>
#include <fact.h>
#include <parser.h>
#include <stdlib.h>

// FIXME These tests are very reliant on the internal structure of the datalog
// facts to not change.  Eventually, they should be redone to not rely on that
// structure so closely.

static void free_symbols(grm_vector_t *symbols)
{
	for (size_t i = 0; i < grm_vector_length(symbols); i++) {
		char **sym = grm_vector_get(symbols, i);
		if (sym) {
			free(*sym);
		}
	}
	grm_vector_free(symbols);
}

static void free_sym_id_map(grm_dict_t *sym_id_map)
{
	char *key;
	void *_;
	GRM_HTABLE_FOREACH(&key, _, sym_id_map) {
		free(key);
	}
	grm_dict_free(sym_id_map);
}

#define check_fact(fact, _id, _arity, _tot, _off)	\
{							\
	ck_assert_int_eq((fact)->id, _id);		\
	ck_assert_int_eq((fact)->arity, _arity);	\
	ck_assert_int_eq((fact)->total_len, _tot);	\
	ck_assert_int_eq((fact)->child_off, _off);	\
}

START_TEST (test_new_fact)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the AST
	grm_parser_t parser;
	grm_parser_init(&parser, "(A (B E F) C (D (G I) H))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);

	// test fact
	grm_error_t err;
	grm_fact_t *fact = grm_fact_new(&fctx, grm_ast_get_child(root, 0), &err);
	ck_assert(!!fact);
	check_fact(fact + 0, 1, 3, 9, 1); // A
	check_fact(fact + 1, 2, 2, 3, 3); // B
	check_fact(fact + 2, 3, 0, 1, 0); // C
	check_fact(fact + 3, 4, 2, 4, 3); // D
	check_fact(fact + 4, 5, 0, 1, 0); // E
	check_fact(fact + 5, 6, 0, 1, 0); // F
	check_fact(fact + 6, 7, 1, 2, 2); // G
	check_fact(fact + 7, 8, 0, 1, 0); // H
	check_fact(fact + 8, 9, 0, 1, 0); // I
	grm_fact_free(fact);

	// test that IDs are reused for recognized symbols
	grm_parser_free(&parser);
	grm_parser_init(&parser, "(G (B J))");
	grm_ast_free_root(root);
	root = grm_parser_parse(&parser, NULL);
	fact = grm_fact_new(&fctx, grm_ast_get_child(root, 0), &err);
	ck_assert(!!fact);
	check_fact(fact + 0,  7, 1, 3, 1); // G
	check_fact(fact + 1, 10, 1, 2, 1); // B
	check_fact(fact + 2, 11, 0, 1, 0); // J
	grm_fact_free(fact);

	// clean-up
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
}
END_TEST

START_TEST (test_new_query)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the query context
	intptr_t var_id = 0;
	grm_query_ctx_t qctx;
	qctx.rel_id = &var_id;
	qctx.var_id_map = grm_vector_new(intptr_t);

	// initialize the AST
	grm_parser_t parser;
	grm_parser_init(&parser, "(legal (move ?player white pawn ?row ?col))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);

	// test query
	grm_error_t err;
	grm_query_t *query = grm_query_new(&fctx, &qctx, grm_ast_get_child(root, 0), &err);
	ck_assert(!!query);
	ck_assert_int_eq(3, grm_vector_length(qctx.var_id_map));
	ck_assert_int_eq(-3, *(intptr_t *)grm_vector_get(qctx.var_id_map, 0));
	ck_assert_int_eq(-6, *(intptr_t *)grm_vector_get(qctx.var_id_map, 1));
	ck_assert_int_eq(-7, *(intptr_t *)grm_vector_get(qctx.var_id_map, 2));
	check_fact(query + 0,  1, 1, 7, 1); // leagl
	check_fact(query + 1,  2, 5, 6, 1); // move
	check_fact(query + 2,  0, 0, 1, 0); // ?player
	check_fact(query + 3,  4, 0, 1, 0); // white
	check_fact(query + 4,  5, 0, 1, 0); // pawn
	check_fact(query + 5, -1, 0, 1, 0); // ?row
	check_fact(query + 6, -2, 0, 1, 0); // ?col

	// clean-up
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	free_symbols(fctx.symbols);
	grm_vector_free(qctx.var_id_map);
	free_sym_id_map(&dict);
	grm_query_free(query);
}
END_TEST

START_TEST (test_query_match)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the facts
	grm_parser_t parser;
	grm_parser_init(&parser, "(legal (move a (b c) d e))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	grm_fact_t *fact1 = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);
	ck_assert(!!fact1);
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	grm_parser_init(&parser, "(legal (move a x a x))");
	root = grm_parser_parse(&parser, NULL);
	grm_fact_t *fact2 = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);
	ck_assert(!!fact2);
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	grm_parser_init(&parser, "(legal (move player noop))");
	root = grm_parser_parse(&parser, NULL);
	grm_fact_t *fact3 = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);
	ck_assert(!!fact3);
	grm_parser_free(&parser);
	grm_ast_free_root(root);

	// initialize the query context
	intptr_t var_id = 0;
	grm_query_ctx_t qctx;
	qctx.rel_id = &var_id;
	qctx.var_id_map = grm_vector_new(intptr_t);

	// initialize the queries
	grm_parser_init(&parser, "(legal (move a ?x ?y ?z))");
	root = grm_parser_parse(&parser, NULL);
	grm_query_t *query1 = grm_query_new(&fctx, &qctx, grm_ast_get_child(root, 0), NULL);
	ck_assert(!!query1);
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	grm_parser_init(&parser, "(legal (move ?x ?y ?x ?z))");
	root = grm_parser_parse(&parser, NULL);
	grm_query_t *query2 = grm_query_new(&fctx, &qctx, grm_ast_get_child(root, 0), NULL);
	ck_assert(!!query2);
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	grm_parser_init(&parser, "(legal ?x)");
	root = grm_parser_parse(&parser, NULL);
	grm_query_t *query3 = grm_query_new(&fctx, &qctx, grm_ast_get_child(root, 0), NULL);
	ck_assert(!!query3);
	grm_parser_free(&parser);
	grm_ast_free_root(root);

	// test queries
	grm_fact_t *answer[3];
	// query #1
	ck_assert(grm_query_match(answer, 3, query1, fact1, true));
	check_fact(answer[0] + 0, 4, 1, 2, 3);
	check_fact(answer[0] + 3, 7, 0, 1, 0);
	check_fact(answer[1] + 0, 5, 0, 1, 0);
	check_fact(answer[2] + 0, 6, 0, 1, 0);
	ck_assert(grm_query_match(answer, 3, query1, fact2, true));
	check_fact(answer[0] + 0, 8, 0, 1, 0);
	check_fact(answer[1] + 0, 3, 0, 1, 0);
	check_fact(answer[2] + 0, 8, 0, 1, 0);
	ck_assert(!grm_query_match(answer, 3, query1, fact3, true));
	// query #2
	ck_assert(!grm_query_match(answer, 3, query2, fact1, true));
	ck_assert(grm_query_match(answer, 3, query2, fact2, true));
	check_fact(answer[0] + 0, 3, 0, 1, 0);
	check_fact(answer[1] + 0, 8, 0, 1, 0);
	check_fact(answer[2] + 0, 8, 0, 1, 0);
	ck_assert(!grm_query_match(answer, 3, query2, fact3, true));
	// query #3
	ck_assert(grm_query_match(answer, 3, query3, fact1, true));
	ck_assert(grm_query_match(answer, 3, query3, fact2, true));
	ck_assert(grm_query_match(answer, 3, query3, fact3, true));
	check_fact(answer[0] + 0,  9, 2, 3, 1);
	check_fact(answer[0] + 1, 10, 0, 1, 0);
	check_fact(answer[0] + 2, 11, 0, 1, 0);

	// clean-up
	free_symbols(fctx.symbols);
	grm_vector_free(qctx.var_id_map);
	free_sym_id_map(&dict);
	grm_fact_free(fact1);
	grm_fact_free(fact2);
	grm_fact_free(fact3);
	grm_query_free(query1);
	grm_query_free(query2);
	grm_query_free(query3);
}
END_TEST

START_TEST (test_illegal_fact)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the AST
	grm_parser_t parser;
	grm_parser_init(&parser, "(A (B ?C) D)");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);

	// test error
	grm_error_t err;
	grm_fact_t *fact = grm_fact_new(&fctx, grm_ast_get_child(root, 0), &err);
	ck_assert(!fact);
	ck_assert_int_eq(err.type, GRM_ILLEGAL_FACT_ERR);
	ck_assert_int_eq(err.token.index, 6);

	// clean-up
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
	grm_fact_free(fact);
}
END_TEST

#define check_fact_string(fctx, ans, match)				\
{									\
	grm_ast_t *tree = grm_fact_to_ast(fctx, ans);			\
	ck_assert(!!tree);						\
	char dst[200];							\
	ck_assert_int_gt(grm_ast_to_string(dst, sizeof(dst), tree), 0);	\
	ck_assert_str_eq(dst, match);					\
	grm_ast_free_root(tree);					\
}

#define check_query_string(fctx, qctx, q, match)			\
{									\
	grm_ast_t *tree = grm_query_to_ast(fctx, qctx, q);		\
	ck_assert(!!tree);						\
	char dst[200];							\
	ck_assert_int_gt(grm_ast_to_string(dst, sizeof(dst), tree), 0);	\
	ck_assert_str_eq(dst, match);					\
	grm_ast_free_root(tree);					\
}

START_TEST (test_fact_derive_and_string)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the fact
	const char FACT[] = "(legal (move a (b c) d e) d f)";
	grm_parser_t parser;
	grm_parser_init(&parser, FACT);
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	grm_fact_t *fact1 = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);
	grm_parser_free(&parser);
	grm_ast_free_root(root);

	// initialize the query context
	intptr_t var_id = 0;
	grm_query_ctx_t qctx;
	qctx.rel_id = &var_id;
	qctx.var_id_map = grm_vector_new(intptr_t);

	// initialize the queries
	grm_parser_init(&parser, "(legal (move a ?x ?y ?z) d f)");
	root = grm_parser_parse(&parser, NULL);
	grm_query_t *query1 = grm_query_new(&fctx, &qctx, grm_ast_get_child(root, 0), NULL);
	check_query_string(&fctx, &qctx, query1, "(legal (move a ?x ?y ?z) d f)");
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	grm_parser_init(&parser, "(legal (move ?x ?y ?z e) ?z f)");
	root = grm_parser_parse(&parser, NULL);
	grm_query_t *query2 = grm_query_new(&fctx, &qctx, grm_ast_get_child(root, 0), NULL);
	check_query_string(&fctx, &qctx, query2, "(legal (move ?x ?y ?z e) ?z f)");
	grm_parser_free(&parser);
	grm_ast_free_root(root);
	grm_parser_init(&parser, "(legal ?x d f)");
	root = grm_parser_parse(&parser, NULL);
	grm_query_t *query3 = grm_query_new(&fctx, &qctx, grm_ast_get_child(root, 0), NULL);
	check_query_string(&fctx, &qctx, query3, "(legal ?x d f)");
	grm_parser_free(&parser);
	grm_ast_free_root(root);

	// test derived facts
	grm_fact_t *ans1[3];
	ck_assert(grm_query_match(ans1, 3, query1, fact1, true));
	grm_fact_t *ans = grm_fact_derive(ans1, 3, query1);
	ck_assert(!!ans);
	check_fact_string(&fctx, ans, FACT);
	grm_fact_free(ans);

	grm_fact_t *ans2[3];
	ck_assert(grm_query_match(ans2, 3, query2, fact1, true));
	ans = grm_fact_derive(ans2, 3, query2);
	ck_assert(!!ans);
	check_fact_string(&fctx, ans, FACT);
	grm_fact_free(ans);

	grm_fact_t *ans3[1];
	ck_assert(grm_query_match(ans3, 1, query3, fact1, true));
	check_fact_string(&fctx, ans3[0], "(move a (b c) d e)");
	ans = grm_fact_derive(ans3, 1, query3);
	ck_assert(!!ans);
	check_fact_string(&fctx, ans, FACT);
	grm_fact_free(ans);

	// clean-up
	free_symbols(fctx.symbols);
	grm_vector_free(qctx.var_id_map);
	free_sym_id_map(&dict);
	grm_fact_free(fact1);
	grm_query_free(query1);
	grm_query_free(query2);
	grm_query_free(query3);
}
END_TEST

START_TEST (test_fact_dup)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the fact
	const char FACT[] = "(legal (move a (b c) d e) d f)";
	grm_parser_t parser;
	grm_parser_init(&parser, FACT);
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	grm_fact_t *fact = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);

	grm_fact_t *copy = grm_fact_dup(fact);
	ck_assert(!!copy);
	check_fact_string(&fctx, copy, FACT);

	// clean-up
	grm_fact_free(fact);
	grm_fact_free(copy);
	grm_ast_free_root(root);
	grm_parser_free(&parser);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
}
END_TEST

START_TEST (test_fact_dup_middle)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the fact
	const char FACT[] = "(nor (foo (bar baz) (spam eggs)) (hello (world !!!)))";
	grm_parser_t parser;
	grm_parser_init(&parser, FACT);
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	grm_fact_t *fact = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);

	grm_fact_t *copy = grm_fact_dup(fact + fact->child_off);
	ck_assert(!!copy);
	check_fact_string(&fctx, copy, "(foo (bar baz) (spam eggs))");

	// clean-up
	grm_fact_free(fact);
	grm_fact_free(copy);
	grm_ast_free_root(root);
	grm_parser_free(&parser);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
}
END_TEST

static int value = 0;

static bool validate_foo (const grm_fact_t *fact,
		const grm_fact_tbl_t *tbl __attribute__((unused)),
		grm_error_t *error)
{
	if (fact[1].id == 3) {
		error->type = 1000;
		return false;
	}
	value = 1;
	return true;
}

START_TEST (test_fact_validation)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	grm_parser_t parser;
	grm_parser_init(&parser, "(foo bar)");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	grm_fact_t *fact = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);
	grm_fact_tbl_t *tbl = grm_fact_tbl_new();
	ck_assert_int_eq(0, grm_fact_tbl_add_validator(tbl, 1, &validate_foo));
	ck_assert_int_eq(0, grm_fact_tbl_add(tbl, fact, NULL));
	ck_assert_int_eq(1, value);
	grm_ast_free_root(root);
	grm_parser_free(&parser);
	grm_parser_init(&parser, "(foo baz)");
	root = grm_parser_parse(&parser, NULL);
	fact = grm_fact_new(&fctx, grm_ast_get_child(root, 0), NULL);
	grm_error_t error;
	ck_assert_int_gt(0, grm_fact_tbl_add(tbl, fact, &error));
	ck_assert_int_eq(1000, error.type);
	grm_fact_free(fact);

	// free/cleanup
	grm_ast_free_root(root);
	grm_parser_free(&parser);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
	grm_fact_tbl_free(tbl);
}
END_TEST

Suite *fact_suite (void)
{
	Suite *s = suite_create("Fact");
	TCase *tc_fact = tcase_create("Core");
	tcase_add_test(tc_fact, test_new_fact);
	tcase_add_test(tc_fact, test_new_query);
	tcase_add_test(tc_fact, test_query_match);
	tcase_add_test(tc_fact, test_illegal_fact);
	tcase_add_test(tc_fact, test_fact_derive_and_string);
	tcase_add_test(tc_fact, test_fact_dup);
	tcase_add_test(tc_fact, test_fact_dup_middle);
	tcase_add_test(tc_fact, test_fact_validation);
	suite_add_tcase(s, tc_fact);
	return s;
}

#pragma GCC diagnostic pop
