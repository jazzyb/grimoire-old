#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <stdio.h>
#include <game.h>

START_TEST (test_gdl_roles)
{
	char *KIF = "(<= (role black) (foo bar))";
	grm_error_t error;
	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_ILLEGAL_ROLE_RULE_ERR, error.type);
	ck_assert_int_eq(strstr(KIF, "role") - KIF, error.token.index);
	char msg[100];
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "role/1 cannot be a rule"));

	KIF = "(not-a-role foobar)";
	game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_UNDEFINED_RELATION_ERR, error.type);
	ck_assert_str_eq("role/1", error.token.lexeme);
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "relation is defined in the description"));
}
END_TEST

START_TEST (test_gdl_inits)
{
	char *KIF = "(role robot) (<= some-rule (init (this state)))";
	grm_error_t error;
	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_ILLEGAL_INIT_BODY_ERR, error.type);
	char msg[100];
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "init/1 cannot appear in a rule body"));

	KIF =   "(role robot)"
		"(<= (init ?x) (role ?x))"
		"(<= (init ?y) (legal robot ?y))";
	game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_ILLEGAL_INIT_DEP_ERR, error.type);
	ck_assert_int_eq(strstr(KIF, "legal") - KIF, error.token.index);
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "init/1 cannot depend on any other GDL-I terms"));

	KIF = "(role robot)";
	game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_UNDEFINED_RELATION_ERR, error.type);
	ck_assert_str_eq("init/1", error.token.lexeme);
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "relation is defined in the description"));
}
END_TEST

START_TEST (test_gdl_nexts)
{
	char *KIF = "(role robot) (init (this state)) (<= rule (next move))";
	grm_error_t error;
	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_ILLEGAL_NEXT_BODY_ERR, error.type);
	ck_assert_int_eq(strstr(KIF, "next") - KIF, error.token.index);
	char msg[100];
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "next/1 cannot appear in a rule body"));

	KIF = "(role robot) (init (this state))";
	game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_UNDEFINED_RELATION_ERR, error.type);
	ck_assert_str_eq("next/1", error.token.lexeme);
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "relation is defined in the description"));
}
END_TEST

START_TEST (test_gdl_trues)
{
	char *KIF = "(role robot) (init (this state)) (next move) (true (this state))";
	grm_error_t error;
	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_TRUE_DEFINED_ERR, error.type);
	ck_assert_int_eq(strstr(KIF, "true") - KIF, error.token.index);
	char msg[100];
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "true/1 states cannot be explicitly defined"));
}
END_TEST

START_TEST (test_gdl_does)
{
	char *KIF = "(role robot) (init (this state)) (next move) (does robot move)";
	grm_error_t error;
	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_DOES_DEFINED_ERR, error.type);
	ck_assert_int_eq(strstr(KIF, "does") - KIF, error.token.index);
	char msg[100];
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "does/2 states cannot be explicitly defined"));

	KIF = "(role robot) (init (this state)) (next move) (<= (legal robot ?move) (does robot ?move))";
	game = grm_game_start(&state, KIF, &error);
	ck_assert(!game);
	ck_assert_int_eq(GRM_ILLEGAL_DOES_DEP_ERR, error.type);
	ck_assert_int_eq(strstr(KIF, "does") - KIF, error.token.index);
	grm_game_error_message(msg, sizeof(msg), KIF, &error);
	ck_assert_ptr_ne(NULL, strstr(msg, "does/2 cannot be a dependency of legal/2, goal/1, or terminal/0"));
}
END_TEST

#define check_undefined(kif, term)					\
{									\
	grm_error_t error;						\
	grm_state_t state;						\
	grm_game_state_init(&state);					\
	grm_game_t *game = grm_game_start(&state, kif, &error);		\
	ck_assert(!game);						\
	ck_assert_int_eq(GRM_UNDEFINED_RELATION_ERR, error.type);	\
	ck_assert_str_eq(term, error.token.lexeme);			\
	char msg[100];							\
	grm_game_error_message(msg, sizeof(msg), KIF, &error);		\
	ck_assert_ptr_ne(NULL, strstr(msg, "No "			\
				term 					\
				" relation is defined in the description"));\
}

START_TEST (test_gdl_base)
{
	char *KIF = 	"(role robot)"
			//"(base (this state))"
			"(input robot move)"
			"(<= (legal robot move) (true (this state)))"
			"(<= (goal robot 100) (true (this state)))"
			"(<= terminal (true (this state)))"
			"(init (this state))"
			"(next (this state))";
	check_undefined(KIF, "base/1");
}
END_TEST

START_TEST (test_gdl_input)
{
	char *KIF = 	"(role robot)"
			"(base (this state))"
			//"(input robot move)"
			"(<= (legal robot move) (true (this state)))"
			"(<= (goal robot 100) (true (this state)))"
			"(<= terminal (true (this state)))"
			"(init (this state))"
			"(next (this state))";
	check_undefined(KIF, "input/2");
}
END_TEST

START_TEST (test_gdl_legal)
{
	char *KIF = 	"(role robot)"
			"(base (this state))"
			"(input robot move)"
			//"(<= (legal robot move) (true (this state)))"
			"(<= (goal robot 100) (true (this state)))"
			"(<= terminal (true (this state)))"
			"(init (this state))"
			"(next (this state))";
	check_undefined(KIF, "legal/2");
}
END_TEST

START_TEST (test_gdl_goal)
{
	char *KIF = 	"(role robot)"
			"(base (this state))"
			"(input robot move)"
			"(<= (legal robot move) (true (this state)))"
			//"(<= (goal robot 100) (true (this state)))"
			"(<= terminal (true (this state)))"
			"(init (this state))"
			"(next (this state))";
	check_undefined(KIF, "goal/2");
}
END_TEST

START_TEST (test_gdl_terminal)
{
	char *KIF = 	"(role robot)"
			"(base (this state))"
			"(input robot move)"
			"(<= (legal robot move) (true (this state)))"
			"(<= (goal robot 100) (true (this state)))"
			//"(<= terminal (true (this state)))"
			"(init (this state))"
			"(next (this state))";
	check_undefined(KIF, "terminal/0");
}
END_TEST

START_TEST (test_gdl_game)
{
	char *KIF = 	"(role robot)"
			"(base (this state))"
			"(input robot move)"
			"(<= (legal robot move) (true (this state)))"
			"(<= (goal robot 100) (true (this state)))"
			"(<= terminal (true (this state)))"
			"(init (this state))"
			"(next (this state))";
	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, NULL);
	ck_assert(!!game);
	ck_assert(!!state.truths);
	ck_assert(!!state.deeds);
	grm_game_state_free(&state);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_next_state)
{
	char *KIF =	"(role robot) "
			"(init (step 1)) "
			"(i 1) (i 2) (i 3) "
			"(+1 1 2) (+1 2 3) "
			"(<= (base (step ?x)) (i ?x)) "
			"(<= (input robot (move ?x)) (i ?x) (distinct ?x 1)) "
			"(<= (legal robot (move ?x)) "
			"    (+1 ?y ?x) "
			"    (true (step ?y))) "
			"(<= (next (step ?x)) "
			"    (does robot (move ?x))) "
			"(<= (goal robot 100) (true (step 3))) "
			"(<= (goal robot 0) (not terminal)) "
			"(<= terminal (true (step 3)))";

	grm_state_t start, next, finish;
	grm_game_state_init(&start);
	grm_game_state_init(&next);
	grm_game_state_init(&finish);
	grm_game_t *game = grm_game_start(&start, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);
	grm_fact_t *move = grm_dl_query_parse(db, "(legal robot (move 2))", NULL);
	ck_assert(!!move);
	ck_assert_int_eq(0, grm_game_next_state(&next, game, &start, &move));
	grm_fact_free(move);

	// check the previous move
	{
		ck_assert_int_eq(1, grm_set_size(start.deeds));
		grm_fact_t *fact;
		GRM_FACT_SET_FOREACH(fact, start.deeds) {
			char string[100];
			ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, fact));
			ck_assert_str_eq("(does robot (move 2))", string);
		}
	}

	// check the next truths
	{
		ck_assert_int_eq(1, grm_set_size(next.truths));
		grm_fact_t *fact;
		GRM_FACT_SET_FOREACH(fact, next.truths) {
			char string[100];
			ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, fact));
			ck_assert_str_eq("(true (step 2))", string);
		}
	}

	move = grm_dl_query_parse(db, "(legal robot (move 3))", NULL);
	ck_assert(!!move);
	ck_assert_int_eq(0, grm_game_next_state(&finish, game, &next, &move));
	grm_fact_free(move);

	// check the previous move
	{
		ck_assert_int_eq(1, grm_set_size(next.deeds));
		grm_fact_t *fact;
		GRM_FACT_SET_FOREACH(fact, next.deeds) {
			char string[100];
			ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, fact));
			ck_assert_str_eq("(does robot (move 3))", string);
		}
	}

	// check the final truths
	{
		ck_assert_int_eq(1, grm_set_size(finish.truths));
		grm_fact_t *fact;
		GRM_FACT_SET_FOREACH(fact, finish.truths) {
			char string[100];
			ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, fact));
			ck_assert_str_eq("(true (step 3))", string);
		}
	}

	grm_game_state_free(&finish);
	grm_game_state_free(&next);
	grm_game_state_free(&start);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_roles)
{
	char *KIF =	"(role bot1) (role bot2) (role bot3)"
			"(init _)"
			"(base _)"
			"(input _ _)"
			"(legal _ _)"
			"(next _)"
			"(goal _ 0)"
			"(terminal)";

	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);

	size_t count;
	const intptr_t *roles = grm_game_roles(game, &count);
	ck_assert(count == 3);
	char *syms[3] = { "bot1", "bot2", "bot3" };
	for (size_t i = 0; i < 3; i++) {
		ck_assert(strcmp(syms[0], grm_dl_get_symbol(db, roles[i])) == 0
			|| strcmp(syms[1], grm_dl_get_symbol(db, roles[i])) == 0
			|| strcmp(syms[2], grm_dl_get_symbol(db, roles[i])) == 0);
	}

	grm_game_state_free(&state);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_propositions)
{
	char *KIF =	"(role _)"
			"(init _)"
			"(base (this state)) (base (that state))"
			"(input _ _)"
			"(legal _ _)"
			"(next _)"
			"(goal _ 0)"
			"(terminal)";

	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);

	size_t count;
	const grm_fact_t **base = grm_game_propositions(game, &count);
	ck_assert_int_eq(2, count);
	for (size_t i = 0; i < count; i++) {
		char string[100];
		ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, base[i]));
		ck_assert(strcmp("(base (this state))", string) == 0
			|| strcmp("(base (that state))", string) == 0);
	}

	grm_game_state_free(&state);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_actions)
{
	char *KIF =	"(role bot1) (role bot2)"
			"(init _)"
			"(base _)"
			"(input bot1 foo) (input bot2 bar) (input bot2 baz)"
			"(legal _ _)"
			"(next _)"
			"(goal _ 0)"
			"(terminal)";

	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);

	intptr_t bot1, bot2;
	size_t count;
	const intptr_t *roles = grm_game_roles(game, &count);
	ck_assert(count == 2);
	if (strcmp("bot1", grm_dl_get_symbol(db, roles[0])) == 0) {
		bot1 = roles[0];
		bot2 = roles[1];
	} else {
		bot1 = roles[1];
		bot2 = roles[0];
	}

	const grm_fact_t **input = grm_game_actions(game, &count, bot1);
	ck_assert_int_eq(1, count);
	char string[100];
	ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, input[0]));
	ck_assert_str_eq("(input bot1 foo)", string);

	input = grm_game_actions(game, &count, bot2);
	ck_assert_int_eq(2, count);
	for (size_t i = 0; i < count; i++) {
		char string[100];
		ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, input[i]));
		ck_assert(strcmp("(input bot2 bar)", string) == 0
			|| strcmp("(input bot2 baz)", string) == 0);
	}

	grm_game_state_free(&state);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_inits)
{
	char *KIF =	"(role _)"
			"(init (this state)) (init (that state))"
			"(base _)"
			"(input _ _)"
			"(legal _ _)"
			"(next _)"
			"(goal _ 0)"
			"(terminal)";

	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);

	size_t count;
	const grm_fact_t **init = grm_game_inits(game, &count);
	ck_assert_int_eq(2, count);
	for (size_t i = 0; i < count; i++) {
		char string[100];
		ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, init[i]));
		ck_assert(strcmp("(init (this state))", string) == 0
			|| strcmp("(init (that state))", string) == 0);
	}

	grm_game_state_free(&state);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_legal_moves)
{
	char *KIF =	"(role bot1) (role bot2)"
			"(init _)"
			"(base _)"
			"(input _ _)"
			"(legal bot1 foo) (legal bot1 bar) (legal bot2 noop)"
			"(next _)"
			"(goal _ 0)"
			"(terminal)";

	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);

	intptr_t bot1, bot2;
	size_t count;
	const intptr_t *roles = grm_game_roles(game, &count);
	ck_assert(count == 2);
	if (strcmp("bot1", grm_dl_get_symbol(db, roles[0])) == 0) {
		bot1 = roles[0];
		bot2 = roles[1];
	} else {
		bot1 = roles[1];
		bot2 = roles[0];
	}

	grm_fact_t **legal = grm_game_legal_moves(game, &state, &count, bot2);
	ck_assert_int_eq(1, count);
	char string[100];
	ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, legal[0]));
	ck_assert_str_eq("(legal bot2 noop)", string);
	grm_dl_free_facts(legal, count);

	legal = grm_game_legal_moves(game, &state, &count, bot1);
	ck_assert_int_eq(2, count);
	for (size_t i = 0; i < count; i++) {
		char string[100];
		ck_assert_int_lt(0, grm_dl_fact_to_string(db, string, 100, legal[i]));
		ck_assert(strcmp("(legal bot1 bar)", string) == 0
			|| strcmp("(legal bot1 foo)", string) == 0);
	}
	grm_dl_free_facts(legal, count);

	grm_game_state_free(&state);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_reward)
{
	char *KIF =	"(role bot1) (role bot2)"
			"(init (this state))"
			"(base _)"
			"(input _ _)"
			"(legal _ _)"
			"(next _)"
			"(<= (goal ?x 50) (role ?x) (true (that state)))"
			"(<= (goal bot1 0) (true (this state)))"
			"(<= (goal bot2 100) (true (this state)))"
			"(terminal)";

	grm_state_t state;
	grm_game_state_init(&state);
	grm_game_t *game = grm_game_start(&state, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);

	intptr_t bot1, bot2;
	size_t count;
	const intptr_t *roles = grm_game_roles(game, &count);
	ck_assert(count == 2);
	if (strcmp("bot1", grm_dl_get_symbol(db, roles[0])) == 0) {
		bot1 = roles[0];
		bot2 = roles[1];
	} else {
		bot1 = roles[1];
		bot2 = roles[0];
	}

	ck_assert_int_eq(0, grm_game_reward(game, &state, bot1));
	ck_assert_int_eq(100, grm_game_reward(game, &state, bot2));

	grm_game_state_free(&state);
	grm_game_free(game);
}
END_TEST

START_TEST (test_game_is_terminal)
{
	char *KIF =	"(role robot)"
			"(init (this state))"
			"(base _)"
			"(input _ _)"
			"(legal robot _)"
			"(next (that state))"
			"(goal _ 0)"
			"(<= terminal (true (that state)))"
			"(<= terminal (not (true (this state))))";

	grm_state_t start, next;
	grm_game_state_init(&start);
	grm_game_state_init(&next);
	grm_game_t *game = grm_game_start(&start, KIF, NULL);
	ck_assert(!!game);
	grm_dl_t *db = grm_game_database(game);
	grm_fact_t *move = grm_dl_query_parse(db, "(legal robot _)", NULL);
	ck_assert(!!move);
	ck_assert_int_eq(0, grm_game_next_state(&next, game, &start, &move));
	grm_fact_free(move);

	ck_assert(!grm_game_is_terminal(game, &start));
	ck_assert(grm_game_is_terminal(game, &next));

	grm_game_free(game);
	grm_game_state_free(&start);
	grm_game_state_free(&next);
}
END_TEST

Suite *game_suite (void)
{
	Suite *s = suite_create("GDL");
	TCase *tc_game = tcase_create("Core");
	tcase_add_test(tc_game, test_gdl_roles);
	tcase_add_test(tc_game, test_gdl_inits);
	tcase_add_test(tc_game, test_gdl_nexts);
	tcase_add_test(tc_game, test_gdl_trues);
	tcase_add_test(tc_game, test_gdl_does);
	tcase_add_test(tc_game, test_gdl_base);
	tcase_add_test(tc_game, test_gdl_input);
	tcase_add_test(tc_game, test_gdl_legal);
	tcase_add_test(tc_game, test_gdl_goal);
	tcase_add_test(tc_game, test_gdl_terminal);
	tcase_add_test(tc_game, test_gdl_game);
	tcase_add_test(tc_game, test_game_next_state);
	tcase_add_test(tc_game, test_game_roles);
	tcase_add_test(tc_game, test_game_propositions);
	tcase_add_test(tc_game, test_game_actions);
	tcase_add_test(tc_game, test_game_inits);
	tcase_add_test(tc_game, test_game_legal_moves);
	tcase_add_test(tc_game, test_game_reward);
	tcase_add_test(tc_game, test_game_is_terminal);
	suite_add_tcase(s, tc_game);
	return s;
}

#pragma GCC diagnostic pop
