#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <lexer.h>
#include <stdio.h>
#include <string.h>

// NOTE These helpers are macros instead of static functions so that
// validation failures show up with the correct line numbers.

#define check_null_token(_lex, _index, _token, _type)			\
{									\
	int rc = grm_lexer_next_token(&(_lex), &(_token));		\
	ck_assert_int_eq(rc, (((_type) == GRM_END_PROGRAM) ? 1 : 0));	\
	ck_assert_int_eq((_token).type, (_type));			\
	ck_assert_int_eq((_token).index, (_index));			\
	ck_assert_ptr_eq((_token).lexeme, NULL);			\
}

#define check_string_token(_lex, _index, _token, _type, _str)	\
{								\
	int rc = grm_lexer_next_token(&(_lex), &(_token));	\
	ck_assert_int_eq(rc, 0);				\
	ck_assert_int_eq((_token).type, (_type));		\
	ck_assert_int_eq((_token).index, (_index));		\
	ck_assert_str_eq((_token).lexeme, (_str));		\
}

START_TEST (test_predicate)
{
	grm_token_t token;
	char *buf = "(a (boo 1uU (c 2 ?XyZ)))";
	grm_lexer_t lex;
	int rc = grm_lexer_init(&lex, buf);
	ck_assert_int_eq(0, rc);

	check_null_token(lex, 0, token, GRM_OPEN_PAREN);
	check_string_token(lex, 1, token, GRM_ATOM_LIT, "a");
	check_null_token(lex, 3, token, GRM_OPEN_PAREN);
	check_string_token(lex, 4, token, GRM_ATOM_LIT, "boo");
	check_string_token(lex, 8, token, GRM_ATOM_LIT, "1uu");
	check_null_token(lex, 12, token, GRM_OPEN_PAREN);
	check_string_token(lex, 13, token, GRM_ATOM_LIT, "c");
	check_string_token(lex, 15, token, GRM_ATOM_LIT, "2");
	check_string_token(lex, 17, token, GRM_VAR_LIT, "?xyz");
	check_null_token(lex, 21, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 22, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 23, token, GRM_CLOSE_PAREN);
	check_null_token(lex, strlen(buf), token, GRM_END_PROGRAM);

	rc = grm_lexer_free(&lex);
	ck_assert_int_eq(0, rc);
}
END_TEST

START_TEST (test_rule)
{
	grm_token_t token;
	char *buf = " (<= open\n"
		    "     (a  \n"
		    " ; b))   \n"
		    "   c))   \n";
	grm_lexer_t lex;
	int rc = grm_lexer_init(&lex, buf);
	ck_assert_int_eq(0, rc);

	check_null_token(lex, 1, token, GRM_OPEN_PAREN);
	check_string_token(lex, 2, token, GRM_RULE_DEF, "<=");
	check_string_token(lex, 5, token, GRM_ATOM_LIT, "open");
	check_null_token(lex, 15, token, GRM_OPEN_PAREN);
	check_string_token(lex, 16, token, GRM_ATOM_LIT, "a");
	check_string_token(lex, 33, token, GRM_ATOM_LIT, "c");
	check_null_token(lex, 34, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 35, token, GRM_CLOSE_PAREN);
	check_null_token(lex, strlen(buf), token, GRM_END_PROGRAM);

	rc = grm_lexer_free(&lex);
	ck_assert_int_eq(0, rc);
}
END_TEST

START_TEST (test_keywords)
{
	grm_token_t token;
	char *buf = "(<= (rule ?x ?y) (or (not (true (a ?x))) (distinct ?x ?y)))";
	grm_lexer_t lex;
	int rc = grm_lexer_init(&lex, buf);
	ck_assert_int_eq(0, rc);

	check_null_token(lex, 0, token, GRM_OPEN_PAREN);
	check_string_token(lex, 1, token, GRM_RULE_DEF, "<=");
	check_null_token(lex, 4, token, GRM_OPEN_PAREN);
	check_string_token(lex, 5, token, GRM_ATOM_LIT, "rule");
	check_string_token(lex, 10, token, GRM_VAR_LIT, "?x");
	check_string_token(lex, 13, token, GRM_VAR_LIT, "?y");
	check_null_token(lex, 15, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 17, token, GRM_OPEN_PAREN);
	check_string_token(lex, 18, token, GRM_OR_KW, "or");
	check_null_token(lex, 21, token, GRM_OPEN_PAREN);
	check_string_token(lex, 22, token, GRM_NOT_KW, "not");
	check_null_token(lex, 26, token, GRM_OPEN_PAREN);
	check_string_token(lex, 27, token, GRM_ATOM_LIT, "true");
	check_null_token(lex, 32, token, GRM_OPEN_PAREN);
	check_string_token(lex, 33, token, GRM_ATOM_LIT, "a");
	check_string_token(lex, 35, token, GRM_VAR_LIT, "?x");
	check_null_token(lex, 37, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 38, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 39, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 41, token, GRM_OPEN_PAREN);
	check_string_token(lex, 42, token, GRM_DISTINCT_KW, "distinct");
	check_string_token(lex, 51, token, GRM_VAR_LIT, "?x");
	check_string_token(lex, 54, token, GRM_VAR_LIT, "?y");
	check_null_token(lex, 56, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 57, token, GRM_CLOSE_PAREN);
	check_null_token(lex, 58, token, GRM_CLOSE_PAREN);

	rc = grm_lexer_free(&lex);
	ck_assert_int_eq(0, rc);
}
END_TEST

Suite *lexer_suite (void)
{
	Suite *s = suite_create("Lexer");
	TCase *tc_lexer = tcase_create("Core");
	tcase_add_test(tc_lexer, test_predicate);
	tcase_add_test(tc_lexer, test_rule);
	tcase_add_test(tc_lexer, test_keywords);
	suite_add_tcase(s, tc_lexer);
	return s;
}

#pragma GCC diagnostic pop
