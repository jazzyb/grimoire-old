#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <stdio.h>
#include <stdint.h>
#include <vector.h>

START_TEST (test_vector_append)
{
	uint64_t ints[] = {100, 101, 102, 103};
	grm_vector_t *v = grm_vector_new(uint64_t);
	ck_assert(!!v);
	for (int i = 0; i < 4; i++) {
		ck_assert_int_eq(0, grm_vector_append(v, ints + i));
	}
	ck_assert_int_eq(grm_vector_length(v), 4);
	for (int i = 3; i >= 0; i--) {
		ck_assert_uint_eq(*(uint64_t *)grm_vector_get(v, i), ints[i]);
	}
	grm_vector_free(v);
}
END_TEST

START_TEST (test_vector_set)
{
	uint64_t ints[] = {100, 101, 102, 103};
	grm_vector_t *v = grm_vector_new(uint64_t);
	ck_assert(!!v);
	ck_assert_int_eq(grm_vector_set(v, 3, ints + 3), 0);
	ck_assert_int_eq(grm_vector_length(v), 4);
	ck_assert_uint_eq(*(uint64_t *)grm_vector_get(v, 3), ints[3]);
	for (int i = 2; i >= 0; i--) {
		ck_assert_uint_eq(*(uint64_t *)grm_vector_get(v, i), 0L);
	}
	grm_vector_free(v);
}
END_TEST

START_TEST (test_vector_expand)
{
	uint64_t foo = 12345;
	grm_vector_t *v = grm_vector_alloc(1, sizeof(foo));
	ck_assert(!!v);
	ck_assert_int_eq(grm_vector_set(v, 17, &foo), 0);
	ck_assert_int_eq(grm_vector_length(v), 18);
	ck_assert_uint_eq(*(uint64_t *)grm_vector_get(v, 17), foo);
	for (int i = 16; i >= 0; i--) {
		ck_assert_uint_eq(*(uint64_t *)grm_vector_get(v, i), 0L);
	}
	grm_vector_free(v);
}
END_TEST

Suite *vector_suite (void)
{
	Suite *s = suite_create("Vector");
	TCase *tc_vector = tcase_create("Core");
	tcase_add_test(tc_vector, test_vector_append);
	tcase_add_test(tc_vector, test_vector_set);
	tcase_add_test(tc_vector, test_vector_expand);
	suite_add_tcase(s, tc_vector);
	return s;
}

#pragma GCC diagnostic pop
