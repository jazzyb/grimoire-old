#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <stdio.h>
#include <error.h>
#include <parser.h>
#include <string.h>

#define check_node(_node, _type, _value, _children)			\
{									\
	ck_assert_int_eq(grm_ast_get_token(_node)->type, _type);	\
	ck_assert_str_eq(grm_ast_get_token(_node)->lexeme, _value);	\
	ck_assert_int_eq(grm_ast_num_children(_node), _children);	\
}

START_TEST (test_basic_tree)
{
	char *buf = "(role white)\n"
		    "(init (cell 1 2 blank))\n"
		    "(<= (next (control ?XXX))\n"
		    "    (= ?XXX black)\n"
		    "    (true (control white)))\n";

	grm_parser_t p;
	ck_assert_int_eq(grm_parser_init(&p, buf), 0);
	grm_ast_t *ptr, *root = grm_parser_parse(&p, NULL);
	ck_assert_ptr_ne(root, NULL);
	ck_assert_int_eq(grm_ast_num_children(root), 3);

	ptr = grm_ast_get_child(root, 0);
	check_node(ptr, GRM_ATOM_LIT, "role", 1);
	ptr = grm_ast_get_child(ptr, 0);
	check_node(ptr, GRM_ATOM_LIT, "white", 0);

	ptr = grm_ast_get_child(root, 1);
	check_node(ptr, GRM_ATOM_LIT, "init", 1);
	ptr = grm_ast_get_child(ptr, 0);
	check_node(ptr, GRM_ATOM_LIT, "cell", 3);
	char *strings[3] = {"1", "2", "blank"};
	for (int i = 0; i < 3; i++) {
		grm_ast_t *tmp = grm_ast_get_child(ptr, i);
		check_node(tmp, GRM_ATOM_LIT, strings[i], 0);
	}

	ptr = grm_ast_get_child(root, 2);
	check_node(ptr, GRM_RULE_DEF, "<=", 3);
	ptr = grm_ast_get_child(ptr, 0);
	check_node(ptr, GRM_ATOM_LIT, "next", 1);
	ptr = grm_ast_get_child(ptr, 0);
	check_node(ptr, GRM_ATOM_LIT, "control", 1);
	ptr = grm_ast_get_child(ptr, 0);
	check_node(ptr, GRM_VAR_LIT, "?xxx", 0);
	ptr = grm_ast_get_child(root, 2);
	ptr = grm_ast_get_child(ptr, 1);
	check_node(ptr, GRM_ATOM_LIT, "=", 2);
	check_node(grm_ast_get_child(ptr, 0), GRM_VAR_LIT, "?xxx", 0);
	check_node(grm_ast_get_child(ptr, 1), GRM_ATOM_LIT, "black", 0);
	ptr = grm_ast_get_child(root, 2);
	ptr = grm_ast_get_child(ptr, 2);
	check_node(ptr, GRM_ATOM_LIT, "true", 1);
	ptr = grm_ast_get_child(ptr, 0);
	check_node(ptr, GRM_ATOM_LIT, "control", 1);
	ptr = grm_ast_get_child(ptr, 0);
	check_node(ptr, GRM_ATOM_LIT, "white", 0);

	grm_ast_free_root(root);
	ck_assert_int_eq(grm_parser_free(&p), 0);
}
END_TEST

#define check_grm_error(_buf, _type, _index, _msg)		\
{								\
	grm_parser_t p;						\
	ck_assert_int_eq(grm_parser_init(&p, (_buf)), 0);	\
	grm_error_t err;					\
	ck_assert_ptr_eq(NULL, grm_parser_parse(&p, &err));	\
	ck_assert_int_eq(err.type, _type);			\
	ck_assert_int_eq(err.token.index, _index);		\
	char sbuf[200];						\
	grm_error_message(sbuf, sizeof(sbuf), (_buf), &err);	\
	ck_assert(strncmp(sbuf, _msg, strlen(_msg)) == 0);	\
	ck_assert_int_eq(grm_parser_free(&p), 0);		\
}

START_TEST (test_illegal_head_err)
{
	check_grm_error("(node\n(\n(x y) z))", GRM_ILLEGAL_HEAD_ERR, 8,
			"line:3:col:1: ");
}
END_TEST

START_TEST (test_empty_predicate_err)
{
	check_grm_error("(node\nz ())", GRM_EMPTY_PREDICATE_ERR, 9,
			"line:2:col:4: ");
}
END_TEST

START_TEST (test_illegal_rule_def)
{
	check_grm_error("(<=)", GRM_ILLEGAL_RULE_DEF_ERR, 1,
			"line:1:col:2: ");
	check_grm_error("(<= foo)", GRM_ILLEGAL_RULE_DEF_ERR, 1,
			"line:1:col:2: ");
}
END_TEST

START_TEST (test_extra_close_parens_err)
{
	check_grm_error(";---\n(hello\nworld))", GRM_EXTRA_CLOSING_ERR, 18,
			"line:3:col:7: ");
}
END_TEST

START_TEST (test_rule_misuse_err)
{
	check_grm_error("<=", GRM_RULE_SYM_MISUSE_ERR, 0,
			"line:1:col:1: ");
	check_grm_error("(foo\n(bar\n(baz\n<=\nxyz))\n)", GRM_RULE_SYM_MISUSE_ERR, 15,
			"line:4:col:1: ");
}
END_TEST

START_TEST (test_illegal_variable_err)
{
	check_grm_error("(node (?x y z))", GRM_ILLEGAL_VAR_LIT_ERR, 7,
			"line:1:col:8: ");
}
END_TEST

START_TEST (test_lone_atom_err)
{
	check_grm_error("(foo\nbar) aaa (bbb ccc)", GRM_LONE_ATOM_ERR, 10,
			"line:2:col:6: ");
}
END_TEST

START_TEST (test_too_few_close_parens)
{
	check_grm_error("(node\n(x\ny\nz)", GRM_EXTRA_OPENING_ERR, 13,
			"line:4:col:3: ");
}
END_TEST

START_TEST (test_or_misuse)
{
	check_grm_error("(and or but)", GRM_KEYWORD_MISUSE_ERR, 5,
			"line:1:col:6: Keyword 'or' ");
}
END_TEST

START_TEST (test_or_arity_error)
{
	check_grm_error("(<= (a ?x ?y) (or (true blue) (true green) (true indigo)))",
			GRM_KEYWORD_ARITY_ERR, 15,
			"line:1:col:16: Keyword 'or' ");
}
END_TEST

START_TEST (test_not_misuse)
{
	check_grm_error("(<= (not ?foo ?bar) (blah baz))", GRM_KEYWORD_MISUSE_ERR, 5,
			"line:1:col:6: Keyword 'not' ");
}
END_TEST

START_TEST (test_not_arity_error)
{
	check_grm_error("(<= (a ?x ?y) (not foo bar))", GRM_KEYWORD_ARITY_ERR, 15,
			"line:1:col:16: Keyword 'not' ");
}
END_TEST

START_TEST (test_distinct_misuse)
{
	check_grm_error("(<= (foo ?bar) (not (distinct ?bar 1)))", GRM_KEYWORD_MISUSE_ERR, 21,
			"line:1:col:22: Keyword 'distinct' ");
}
END_TEST

START_TEST (test_distinct_arity_error)
{
	check_grm_error("(<= (foo ?bar) (distinct))", GRM_KEYWORD_ARITY_ERR, 16,
			"line:1:col:17: Keyword 'distinct' ");
}
END_TEST

Suite *parser_suite (void)
{
	Suite *s = suite_create("Parser");
	TCase *tc_parser = tcase_create("Core");
	tcase_add_test(tc_parser, test_basic_tree);
	tcase_add_test(tc_parser, test_illegal_head_err);
	tcase_add_test(tc_parser, test_empty_predicate_err);
	tcase_add_test(tc_parser, test_illegal_rule_def);
	tcase_add_test(tc_parser, test_extra_close_parens_err);
	tcase_add_test(tc_parser, test_rule_misuse_err);
	tcase_add_test(tc_parser, test_illegal_variable_err);
	tcase_add_test(tc_parser, test_lone_atom_err);
	tcase_add_test(tc_parser, test_too_few_close_parens);
	tcase_add_test(tc_parser, test_or_misuse);
	tcase_add_test(tc_parser, test_or_arity_error);
	tcase_add_test(tc_parser, test_not_misuse);
	tcase_add_test(tc_parser, test_not_arity_error);
	tcase_add_test(tc_parser, test_distinct_misuse);
	tcase_add_test(tc_parser, test_distinct_arity_error);
	suite_add_tcase(s, tc_parser);
	return s;
}

#pragma GCC diagnostic pop
