#!/usr/bin/env python3

from shutil import copyfile
from tempfile import NamedTemporaryFile
import os
import sys

if len(sys.argv) < 2:
    print('usage: %s NEW_TEST' % (sys.argv[0],))
    sys.exit(1)

for test in sys.argv[1:]:
    path = os.path.join('test', 'check_%s.c' % (test,))
    if os.path.isfile(path):
        print('error: test file "%s" already exists' % (path,))
        sys.exit(1)

    try:
        with open(path, 'w') as f:
            f.write('#pragma GCC diagnostic push\n')
            f.write('// Ignore format errors in case we want to use printf() '
                    'to debug these tests:\n')
            f.write('#pragma GCC diagnostic ignored "-Wformat"\n')
            f.write('\n')
            f.write('#include <check.h>\n')
            f.write('#include <stdio.h>\n')
            f.write('#include <%s.h>\n' % (test,))
            f.write('\n')
            f.write('START_TEST (test_%s)\n' % (test,))
            f.write('{\n')
            f.write('\t// TODO\n')
            f.write('}\n')
            f.write('END_TEST\n')
            f.write('\n')
            f.write('Suite *%s_suite (void)\n' % (test,))
            f.write('{\n')
            f.write('\tSuite *s = suite_create("%s");\n' % (test[0].upper() + test[1:],))
            f.write('\tTCase *tc_%s = tcase_create("Core");\n' % (test,))
            f.write('\ttcase_add_test(tc_%s, test_%s);\n' % (test, test))
            f.write('\tsuite_add_tcase(s, tc_%s);\n' % (test,))
            f.write('\treturn s;\n')
            f.write('}\n')
            f.write('\n');
            f.write('#pragma GCC diagnostic pop\n')
    except:
        os.remove(path)
        raise

    check_main = os.path.join('test', 'check_main.c')
    with NamedTemporaryFile('w') as tmp:
        with open(check_main) as main:
            for line in main:
                if 'DECLARE NEW SUITES HERE' in line:
                    tmp.write('extern Suite *%s_suite(void);\n' % (test,))
                elif 'ADD NEW SUITES HERE' in line:
                    tmp.write('\tsrunner_add_suite(sr, %s_suite());\n' % (test,))
                tmp.write(line)
        tmp.flush()
        copyfile(tmp.name, check_main)
