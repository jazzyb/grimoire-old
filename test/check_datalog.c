#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <stdio.h>
#include <datalog.h>
#include <parser.h>
#include <stdlib.h>

START_TEST (test_datalog_unstratified_err)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	grm_parser_t parser;
	grm_parser_init(&parser, "(<= (p ?x) (q ?x) (not (p ?x)))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	grm_error_t err;
	ck_assert_int_gt(0, grm_dl_update(&db, grm_ast_get_child(root, 0), &err));
	ck_assert_int_eq(GRM_UNSTRATIFIED_RULE_ERR, err.type);
	ck_assert_int_eq(5, err.token.index);
	grm_ast_free_root(root);
	grm_parser_free(&parser);

	grm_parser_init(&parser, "(<= (p ?x) (q ?x))");
	root = grm_parser_parse(&parser, NULL);
	ck_assert_int_eq(0, grm_dl_update(&db, grm_ast_get_child(root, 0), NULL));
	grm_ast_free_root(root);
	grm_parser_free(&parser);

	grm_parser_init(&parser, "(<= (q ?x) (r ?x) (not (p ?x)))");
	root = grm_parser_parse(&parser, NULL);
	ck_assert_int_gt(0, grm_dl_update(&db, grm_ast_get_child(root, 0), &err));
	ck_assert_int_eq(GRM_UNSTRATIFIED_RULE_ERR, err.type);
	ck_assert_int_eq(5, err.token.index);
	grm_ast_free_root(root);
	grm_parser_free(&parser);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

#define updatedb(db, string)	\
	ck_assert_int_eq(0, grm_dl_parse(db, string, NULL));

START_TEST (test_datalog_unrestricted_recursion_err)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	updatedb(&db, "(<= (p ?x ?y) (b1 ?x ?a) (q ?y ?b) (b2 ?a ?b))");
	updatedb(&db, "(<= (q ?x1 ?x2) (p ?x2 ?x1))");

	grm_error_t err;
	grm_parser_t parser;
	grm_parser_init(&parser, "(<= (b2 ?a1 ?a2) (q ?a1 ?a2))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	ck_assert_int_gt(0, grm_dl_update(&db, grm_ast_get_child(root, 0), &err));
	ck_assert_int_eq(GRM_UNRESTRICTED_RECURSION_ERR, err.type);
	ck_assert_int_eq(5, err.token.index);
	grm_ast_free_root(root);
	grm_parser_free(&parser);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

#define make_query(query, db, string)			\
{							\
	query = grm_dl_query_parse(db, string, NULL);	\
	ck_assert(!!query);				\
}

#define assert_facts(db, facts, count, answers)					\
	for (size_t i = 0; i < count; i++) {					\
		bool found = false;						\
		char string[200];						\
		grm_dl_fact_to_string(db, string, sizeof(string), facts[i]);	\
		for (size_t j = 0; j < count; j++) {				\
			if (strcmp(string, (answers)[j]) == 0) {		\
				found = true;					\
				break;						\
			}							\
		}								\
		strcat(string, " not found in answers");			\
		ck_assert_msg(found, string);					\
	}

START_TEST (test_datalog_fact_matching)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	updatedb(&db, "(parent pam bob)");
	updatedb(&db, "(parent tom bob)");
	updatedb(&db, "(parent tom bob)");
	updatedb(&db, "(parent tom liz)");
	updatedb(&db, "(female pat)");
	updatedb(&db, "(male bob)");
	updatedb(&db, "(female pam)");
	updatedb(&db, "(parent bob ann)");
	updatedb(&db, "(parent bob pat)");
	updatedb(&db, "(female liz)");
	updatedb(&db, "(female ann)");
	updatedb(&db, "(male jim)");
	updatedb(&db, "(male tom)");
	updatedb(&db, "(parent pat jim)");

	char *answers[] = {"(parent pat jim)",
		"(parent bob ann)",
		"(parent bob pat)",
		"(parent tom liz)",
		"(parent pam bob)",
		"(parent tom bob)"};

	grm_fact_t **facts;
	size_t count;
	grm_query_t *query;
	make_query(query, &db, "(parent ?parent ?child)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(6, count);
	assert_facts(&db, facts, count, answers);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	make_query(query, &db, "(parent ?parent bob)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(2, count);
	assert_facts(&db, facts, count, answers + 4);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	make_query(query, &db, "(parent tom bob)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(1, count);
	assert_facts(&db, facts, count, answers + 5);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	make_query(query, &db, "(parent liz ?child)");
	ck_assert_int_eq(0, grm_dl_match_query(&db, NULL, NULL, query, NULL));
	grm_query_free(query);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

START_TEST (test_datalog_invalid_query_err)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	grm_error_t err;
	grm_query_t *query = grm_dl_query_parse(&db, "(INVALID) (QUERY)", &err);
	ck_assert_ptr_eq(NULL, query);
	ck_assert_int_eq(GRM_INVALID_QUERY_ERR, err.type);
	char msg[200];
	ck_assert_int_gt(sizeof(msg), grm_error_message(msg, sizeof(msg), "(INVALID) (QUERY)", &err));
	ck_assert_str_eq(msg, "Not a valid query");

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

START_TEST (test_datalog_nonrecursive_rule_match)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	// FACTS
	updatedb(&db, "(x 1)");
	updatedb(&db, "(x 2)");
	updatedb(&db, "(x 3)");
	updatedb(&db, "(x 4)");
	updatedb(&db, "(x 5)");
	updatedb(&db, "(x 6)");
	updatedb(&db, "(x 7)");
	updatedb(&db, "(x 8)");
	updatedb(&db, "(y 1)");
	updatedb(&db, "(y 2)");
	updatedb(&db, "(y 3)");
	updatedb(&db, "(y 4)");
	updatedb(&db, "(y 5)");
	updatedb(&db, "(y 6)");
	updatedb(&db, "(true (cell 1 6 red))");
	updatedb(&db, "(true (cell 2 6 black))");
	updatedb(&db, "(true (cell 3 6 red))");
	updatedb(&db, "(true (cell 4 6 black))");
	//updatedb(&db, "(true (cell 5 6 red))");
	updatedb(&db, "(true (cell 6 6 black))");
	updatedb(&db, "(true (cell 7 6 red))");
	updatedb(&db, "(true (cell 8 6 black))");

	// RULES
	updatedb(&db,   "(<= (cellOpen ?x ?y)"
			"    (x ?x)"
			"    (y ?y)"
			"    (not (true (cell ?x ?y red)))"
			"    (not (true (cell ?x ?y black))))");
	updatedb(&db,   "(<= (columnOpen ?x)"
			"    (cellOpen ?x 6))");
	updatedb(&db,   "(<= boardOpen"
			"    (x ?x)"
			"    (columnOpen ?x))");

	grm_fact_t **facts;
	size_t count;
	grm_query_t *query;
	make_query(query, &db, "(boardOpen)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(1, count);
	char *answers3 = "(boardopen)";
	assert_facts(&db, facts, count, &answers3);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	make_query(query, &db, "(cellOpen ?x 6)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(1, count);
	char *answers1 = "(cellopen 5 6)";
	assert_facts(&db, facts, count, &answers1);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	make_query(query, &db, "(columnOpen ?x)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(1, count);
	char *answers2 = "(columnopen 5)";
	assert_facts(&db, facts, count, &answers2);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	// this new fact shouldn't change anything
	updatedb(&db, "(true (cell 6 5 red))");
	make_query(query, &db, "(boardOpen)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, NULL, NULL, query, NULL));
	grm_query_free(query);

	// but this new fact should
	updatedb(&db, "(true (cell 5 6 red))");
	make_query(query, &db, "(boardOpen)");
	ck_assert_int_eq(0, grm_dl_match_query(&db, NULL, NULL, query, NULL));
	grm_query_free(query);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

START_TEST (test_datalog_recursive_rule_match)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	// FACTS
	updatedb(&db, "(parent pam bob)");
	updatedb(&db, "(parent tom bob)");
	updatedb(&db, "(parent tom liz)");
	updatedb(&db, "(female pat)");
	updatedb(&db, "(male bob)");
	updatedb(&db, "(female pam)");
	updatedb(&db, "(parent bob ann)");
	updatedb(&db, "(parent bob pat)");
	updatedb(&db, "(female liz)");
	updatedb(&db, "(female ann)");
	updatedb(&db, "(male jim)");
	updatedb(&db, "(male tom)");
	updatedb(&db, "(parent pat jim)");

	// RULES
	updatedb(&db,   "(<= (ancestor ?x ?z)"
			"    (ancestor ?y ?z)"
			"    (parent ?x ?y))");
	updatedb(&db,   "(<= (ancestor ?x ?z) (parent ?x ?z))");

	grm_fact_t **facts;
	size_t count;
	grm_query_t *query;
	make_query(query, &db, "(ancestor ?ancestor ?descendant)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(13, count);
	char *answers[13] = {"(ancestor bob ann)",
		"(ancestor bob jim)",
		"(ancestor tom jim)",
		"(ancestor tom bob)",
		"(ancestor pam jim)",
		"(ancestor bob pat)",
		"(ancestor tom pat)",
		"(ancestor tom liz)",
		"(ancestor pam ann)",
		"(ancestor tom ann)",
		"(ancestor pam pat)",
		"(ancestor pat jim)",
		"(ancestor pam bob)"};
	assert_facts(&db, facts, count, answers);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

START_TEST (test_datalog_distinct)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	// FACTS
	updatedb(&db, "(index 1)");
	updatedb(&db, "(index 2)");
	updatedb(&db, "(index 3)");
	updatedb(&db, "(index 4)");
	updatedb(&db, "(index 5)");
	updatedb(&db, "(index 6)");
	updatedb(&db, "(index 7)");

	// RULES
	updatedb(&db,   "(<= (cell ?x ?y)"
			"    (index ?x)"
			"    (index ?y))");
	updatedb(&db,   "(<= (onboard ?x ?y)"
			"    (cell ?x ?y)"
			"    (distinct ?x 1)"
			"    (distinct ?x 7)"
			"    (distinct ?y 1)"
			"    (distinct ?y 7))");

	grm_fact_t **facts;
	size_t count;
	grm_query_t *query;
	make_query(query, &db, "(onboard ?x ?x)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(5, count);
	char *answers[5] = {"(onboard 2 2)",
		"(onboard 3 3)",
		"(onboard 4 4)",
		"(onboard 5 5)",
		"(onboard 6 6)"};
	assert_facts(&db, facts, count, answers);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

START_TEST (test_datalog_or)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	// FACTS
	updatedb(&db, "(true (cell 1 1 b))");
	updatedb(&db, "(true (cell 1 2 b))");
	updatedb(&db, "(true (cell 1 3 b))");
	updatedb(&db, "(true (cell 2 1 b))");
	updatedb(&db, "(true (cell 2 2 b))");
	updatedb(&db, "(true (cell 2 3 b))");
	updatedb(&db, "(true (cell 3 1 b))");
	updatedb(&db, "(true (cell 3 2 b))");
	updatedb(&db, "(true (cell 3 3 b))");
	updatedb(&db, "(does white (mark 1 1))");
	updatedb(&db, "(does black (mark 1 1))");

	// RULES
	updatedb(&db,   "(<= (next (cell ?j ?k x))"
			"    (or (distinct ?j ?m) (distinct ?k ?n))"
			"    (does white (mark ?j ?k))"
			"    (does black (mark ?m ?n))"
			"    (true (cell ?j ?k b)))");

	grm_fact_t **facts;
	size_t count;
	grm_query_t *query;
	make_query(query, &db, "(next (cell ?a ?b x))");
	ck_assert_int_eq(0, grm_dl_match_query(&db, &facts, &count, query, NULL));
	grm_query_free(query);

	updatedb(&db, "(does white (mark 1 2))");
	make_query(query, &db, "(next (cell ?a ?b x))");
	ck_assert_int_eq(1, grm_dl_match_query(&db, &facts, &count, query, NULL));
	ck_assert_int_eq(1, count);
	char *answer = "(next (cell 1 2 x))";
	assert_facts(&db, facts, count, &answer);
	grm_dl_free_facts(facts, count);
	grm_query_free(query);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

START_TEST (test_datalog_0_arity_predicates)
{
	grm_dl_t db;
	ck_assert_int_eq(0, grm_dl_init(&db));

	updatedb(&db, "(<= open (true (cell ?m ?n b)))");
	updatedb(&db, "(<= terminal (not open))");

	grm_query_t *query;
	make_query(query, &db, "(terminal)");
	ck_assert_int_eq(1, grm_dl_match_query(&db, NULL, NULL, query, NULL));
	updatedb(&db, "(true (cell 2 2 b))");
	ck_assert_int_eq(0, grm_dl_match_query(&db, NULL, NULL, query, NULL));
	grm_query_free(query);

	ck_assert_int_eq(0, grm_dl_free(&db));
}
END_TEST

Suite *datalog_suite (void)
{
	Suite *s = suite_create("Datalog");
	TCase *tc_datalog = tcase_create("Core");
	tcase_add_test(tc_datalog, test_datalog_unstratified_err);
	tcase_add_test(tc_datalog, test_datalog_unrestricted_recursion_err);
	tcase_add_test(tc_datalog, test_datalog_fact_matching);
	tcase_add_test(tc_datalog, test_datalog_invalid_query_err);
	tcase_add_test(tc_datalog, test_datalog_nonrecursive_rule_match);
	tcase_add_test(tc_datalog, test_datalog_recursive_rule_match);
	tcase_add_test(tc_datalog, test_datalog_distinct);
	tcase_add_test(tc_datalog, test_datalog_or);
	tcase_add_test(tc_datalog, test_datalog_0_arity_predicates);
	suite_add_tcase(s, tc_datalog);
	return s;
}

#pragma GCC diagnostic pop
