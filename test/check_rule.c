#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <assert.h>
#include <check.h>
#include <htable.h>
#include <keywd.h>
#include <parser.h>
#include <rule.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector.h>

#define ABBA_ID 4
#define BAAB_ID 5

#define MY_NUM_DATALOG_KEYWORDS 5

static grm_keywd_t keywords[MY_NUM_DATALOG_KEYWORDS] = {
	{ .id = GRM_OR_ID, 		.sym = "or", 		.arity = 2 },
	{ .id = GRM_NOT_ID, 		.sym = "not", 		.arity = 1 },
	{ .id = GRM_DISTINCT_ID,	.sym = "distinct", 	.arity = 2 },
	// the next two are used by test_rule_validator
	{ .id = ABBA_ID,		.sym = "abba", 		.arity = 0 },
	{ .id = BAAB_ID,		.sym = "baab", 		.arity = 0 },
};

static int set_keywords (intptr_t *next_id,
		grm_vector_t *symbols,
		grm_dict_t *sym_id_map,
		grm_keywd_t *keywords,
		size_t size)
{
	for (size_t i = 0; i < size; i++) {
		intptr_t id = *next_id;
		*next_id += 1;
		grm_keywd_t *kw = keywords + i;
		assert(id == kw->id);
		char *sym = strdup(kw->sym);
		if (grm_vector_set(symbols, id, &sym) < 0) {
			return -1;
		}
		char *sym_arity = grm_keywd_symbol_arity(kw->sym, kw->arity);
		if (grm_dict_set(sym_id_map, sym_arity, (void *)id) < 0) {
			return -1;
		}
	}
	return 0;
}

static void free_symbols(grm_vector_t *symbols)
{
	for (size_t i = 0; i < grm_vector_length(symbols); i++) {
		char **sym = grm_vector_get(symbols, i);
		if (sym) {
			free(*sym);
		}
	}
	grm_vector_free(symbols);
}

static void free_sym_id_map(grm_dict_t *sym_id_map)
{
	char *key;
	void *_;
	GRM_HTABLE_FOREACH(&key, _, sym_id_map) {
		free(key);
	}
	grm_dict_free(sym_id_map);
}

START_TEST (test_new_rule)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the default keywords
	set_keywords(fctx.next_id, fctx.symbols, fctx.sym_id_map, keywords, MY_NUM_DATALOG_KEYWORDS);

	// initialize the AST
	grm_parser_t parser;
	grm_parser_init(&parser, "(<= (a ?x) (b1 ?y ?x) (not (b2 ?z ?y)) (b3 ?w ?z))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);

	grm_error_t err;
	grm_rule_t *rule = grm_rule_new(&fctx, grm_ast_get_child(root, 0), &err);
	ck_assert(!!rule);

	// TODO What do we need to assert about the rule structure?

	grm_rule_free(rule);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
	grm_ast_free_root(root);
	grm_parser_free(&parser);
}
END_TEST

START_TEST (test_body_safety_err)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the default keywords
	set_keywords(fctx.next_id, fctx.symbols, fctx.sym_id_map, keywords, MY_NUM_DATALOG_KEYWORDS);

	// initialize the AST
	grm_parser_t parser;
	grm_parser_init(&parser, "(<= (a ?x) (b1 ?y ?x) (not (b2 ?z ?t)) (b3 ?w ?z))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);

	grm_error_t err;
	grm_rule_t *rule = grm_rule_new(&fctx, grm_ast_get_child(root, 0), &err);
	ck_assert(!rule);
	ck_assert_int_eq(err.type, GRM_BODY_SAFETY_ERR);
	ck_assert_int_eq(err.token.index, 34);

	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
	grm_ast_free_root(root);
	grm_parser_free(&parser);
}
END_TEST

START_TEST (test_head_safety_err)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the default keywords
	set_keywords(fctx.next_id, fctx.symbols, fctx.sym_id_map, keywords, MY_NUM_DATALOG_KEYWORDS);

	// initialize the AST
	grm_parser_t parser;
	grm_parser_init(&parser, "(<= (a ?x ?t) (b1 ?y ?x) (not (b2 ?z ?y)) (b3 ?w ?z))");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);

	grm_error_t err;
	grm_rule_t *rule = grm_rule_new(&fctx, grm_ast_get_child(root, 0), &err);
	ck_assert(!rule);
	ck_assert_int_eq(err.type, GRM_HEAD_SAFETY_ERR);
	ck_assert_int_eq(err.token.index, 10);

	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
	grm_ast_free_root(root);
	grm_parser_free(&parser);
}
END_TEST

#define configure_rule_table(fctx, table, count, rules)						\
{												\
	for (size_t i = 0; i < count; i++) {							\
		char *string = rules[i];							\
		grm_parser_t parser;								\
		grm_parser_init(&parser, string);						\
		grm_ast_t *root = grm_parser_parse(&parser, NULL);				\
		grm_rule_t *rule = grm_rule_new(fctx, grm_ast_get_child(root, 0), NULL);	\
		grm_rule_tbl_add(table, rule, NULL);						\
		grm_ast_free_root(root);							\
		grm_parser_free(&parser);							\
	}											\
}

START_TEST (test_tbl_is_in_body)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the default keywords
	set_keywords(fctx.next_id, fctx.symbols, fctx.sym_id_map, keywords, MY_NUM_DATALOG_KEYWORDS);

	char *rules[] = { "(<= a b c)", "(<= b c d)", "(<= c x y z)" };
	grm_rule_tbl_t *table = grm_rule_tbl_new();
	configure_rule_table(&fctx, table, 3, rules);
	size_t index;
	ck_assert(grm_rule_tbl_is_in_body(table, 10, &index));
	ck_assert_int_eq(index, 6);
	ck_assert(!grm_rule_tbl_is_in_body(table, 6, NULL));
	grm_rule_tbl_free(table);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
}
END_TEST

START_TEST (test_tbl_depends_on)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the default keywords
	set_keywords(fctx.next_id, fctx.symbols, fctx.sym_id_map, keywords, MY_NUM_DATALOG_KEYWORDS);

	char *rules[] = { "(<= a b c)", "(<= b d e)", "(<= c x y z)" };
	grm_rule_tbl_t *table = grm_rule_tbl_new();
	configure_rule_table(&fctx, table, 3, rules);
	size_t index;
	ck_assert(grm_rule_tbl_depends_on(table, 6, 8, NULL));
	ck_assert(grm_rule_tbl_depends_on(table, 6, 11, &index));
	ck_assert_int_eq(index, 6);
	ck_assert(!grm_rule_tbl_depends_on(table, 7, 8, NULL));
	ck_assert(!grm_rule_tbl_depends_on(table, 6, 6, NULL));
	ck_assert(!grm_rule_tbl_depends_on(table, 9, 7, NULL));
	grm_rule_tbl_free(table);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
}
END_TEST

START_TEST (test_tbl_is_cycle)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the default keywords
	set_keywords(fctx.next_id, fctx.symbols, fctx.sym_id_map, keywords, MY_NUM_DATALOG_KEYWORDS);

	char *rules[] = { "(<= a b c)", "(<= b d e)", "(<= c x y z)", "(<= e a)" };
	grm_rule_tbl_t *table = grm_rule_tbl_new();
	configure_rule_table(&fctx, table, 4, rules);
	ck_assert(grm_rule_tbl_is_cycle(table, 6, 7));
	ck_assert(!grm_rule_tbl_is_cycle(table, 6, 9));
	ck_assert(grm_rule_tbl_is_cycle(table, 7, 7));
	ck_assert(!grm_rule_tbl_is_cycle(table, 8, 8));
	grm_rule_tbl_free(table);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
}
END_TEST

static int value = 0;

static bool validate_b (const grm_rule_t *rule,
		const grm_rule_tbl_t *tbl __attribute__((unused)),
		grm_error_t *error)
{
	for (size_t i = 0; i < rule->body_length; i++) {
		if (rule->body[i]->id == BAAB_ID) {
			error->type = 1000;
			return false;
		}
	}
	value = 1;
	return true;
}

START_TEST (test_rule_validator)
{
	// initialize the fact context
	intptr_t id = 1;
	grm_dict_t dict;
	grm_dict_init(&dict);
	grm_fact_ctx_t fctx;
	fctx.sym_id_map = &dict;
	fctx.next_id = &id;
	fctx.symbols = grm_vector_new(char*);

	// initialize the default keywords
	set_keywords(fctx.next_id, fctx.symbols, fctx.sym_id_map, keywords, MY_NUM_DATALOG_KEYWORDS);

	grm_rule_tbl_t *table = grm_rule_tbl_new();
	ck_assert_int_eq(0, grm_rule_tbl_add_validator(table, ABBA_ID, validate_b));
	char *rules[] = { "(<= a abba c)", "(<= abba d e)", "(<= c x y z)", "(<= e a)" };
	configure_rule_table(&fctx, table, 4, rules);
	ck_assert_int_eq(1, value);

	// make sure the rule with baab in the body fails
	grm_parser_t parser;
	grm_parser_init(&parser, "(<= abba x y baab)");
	grm_ast_t *root = grm_parser_parse(&parser, NULL);
	grm_rule_t *rule = grm_rule_new(&fctx, grm_ast_get_child(root, 0), NULL);
	grm_error_t error;
	ck_assert_int_gt(0, grm_rule_tbl_add(table, rule, &error));
	ck_assert_int_eq(1000, error.type);
	grm_ast_free_root(root);
	grm_parser_free(&parser);

	grm_rule_free(rule);
	grm_rule_tbl_free(table);
	free_symbols(fctx.symbols);
	free_sym_id_map(&dict);
}
END_TEST

Suite *rule_suite (void)
{
	Suite *s = suite_create("Rule");
	TCase *tc_rule = tcase_create("Core");
	tcase_add_test(tc_rule, test_new_rule);
	tcase_add_test(tc_rule, test_body_safety_err);
	tcase_add_test(tc_rule, test_head_safety_err);
	tcase_add_test(tc_rule, test_tbl_is_in_body);
	tcase_add_test(tc_rule, test_tbl_depends_on);
	tcase_add_test(tc_rule, test_tbl_is_cycle);
	tcase_add_test(tc_rule, test_rule_validator);
	suite_add_tcase(s, tc_rule);
	return s;
}

#pragma GCC diagnostic pop
