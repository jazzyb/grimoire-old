#pragma GCC diagnostic push
// Ignore format errors in case we want to use printf() to debug these tests:
#pragma GCC diagnostic ignored "-Wformat"

#include <check.h>
#include <stdio.h>
#include <queue.h>

START_TEST (test_queue_normal)
{
	grm_queue_t *q = grm_queue_new(0);
	ck_assert(!!q);
	int items[3];
	ck_assert_int_eq(grm_queue_push(q, items + 0), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 1), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 2), 0);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 0);
	ck_assert(!grm_queue_contains(q, items + 0));
	ck_assert_ptr_eq(grm_queue_pop(q), items + 1);
	ck_assert(!grm_queue_empty(q));
	ck_assert(grm_queue_contains(q, items + 2));
	ck_assert_ptr_eq(grm_queue_pop(q), items + 2);
	ck_assert_ptr_eq(grm_queue_pop(q), NULL);
	ck_assert(grm_queue_empty(q));
	grm_queue_free(q);
}
END_TEST

#include <stdio.h>

START_TEST (test_queue_resize)
{
	grm_queue_t *q = grm_queue_new(4);
	ck_assert(!!q);
	int items[8];
	ck_assert_int_eq(grm_queue_push(q, items + 0), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 1), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 2), 0);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 0);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 1);
	ck_assert_int_eq(grm_queue_push(q, items + 3), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 4), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 5), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 6), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 7), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 0), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 1), 0);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 2);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 3);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 4);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 5);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 6);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 7);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 0);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 1);
	grm_queue_free(q);
}
END_TEST

START_TEST (test_queue_reverse)
{
	grm_queue_t *q = grm_queue_new(4);
	ck_assert(!!q);
	int items[8];
	ck_assert_int_eq(grm_queue_push(q, items + 0), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 1), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 2), 0);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 0);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 1);
	ck_assert_int_eq(grm_queue_push(q, items + 3), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 4), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 5), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 6), 0);
	ck_assert_int_eq(grm_queue_push(q, items + 7), 0);
	grm_queue_reverse(q);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 7);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 6);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 5);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 4);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 3);
	ck_assert_ptr_eq(grm_queue_pop(q), items + 2);
	grm_queue_free(q);
}
END_TEST

Suite *queue_suite (void)
{
	Suite *s = suite_create("Queue");
	TCase *tc_queue = tcase_create("Core");
	tcase_add_test(tc_queue, test_queue_normal);
	tcase_add_test(tc_queue, test_queue_resize);
	tcase_add_test(tc_queue, test_queue_reverse);
	suite_add_tcase(s, tc_queue);
	return s;
}

#pragma GCC diagnostic pop
