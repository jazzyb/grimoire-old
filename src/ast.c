#include "ast.h"

#include "lexer.h"
#include "vector.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

struct grm_ast_ {
	grm_token_t token;
	struct grm_ast_ *parent;
	grm_vector_t *children;
};

grm_ast_t *grm_ast_new_root (void)
{
	return grm_ast_add_child(NULL);
}

#define DEFAULT_CHILD_CAPACITY 2

static int initialize_ast_node (grm_ast_t *node, grm_ast_t *parent)
{
	node->token.type = GRM_EMPTY_TOKEN;
	node->token.lexeme = NULL;
	node->parent = parent;
	node->children = grm_vector_alloc(DEFAULT_CHILD_CAPACITY, sizeof(node));
	if (!node->children) {
		return -1;
	}

	if (parent && grm_vector_append(parent->children, &node) != 0) {
		return -1;
	}
	return 0;
}

grm_ast_t *grm_ast_add_child (grm_ast_t *parent)
{
	grm_ast_t *child = malloc(sizeof(*child));
	if (child == NULL) {
		return NULL;
	}
	if (initialize_ast_node(child, parent) != 0) {
		free(child);
		return NULL;
	}
	return child;
}

int grm_ast_set_token (grm_ast_t *node, const grm_token_t *token)
{
	return grm_token_copy(&node->token, token);
}

const grm_token_t *grm_ast_get_token (const grm_ast_t *node)
{
	return &node->token;
}

grm_ast_t *grm_ast_get_parent (const grm_ast_t *node)
{
	return node->parent;
}

size_t grm_ast_num_children (const grm_ast_t *node)
{
	return grm_vector_length(node->children);
}

size_t grm_ast_num_nodes (const grm_ast_t *root)
{
	size_t count = 1;
	grm_ast_t *child;
	GRM_AST_FOREACH_CHILD(child, root) {
		count += grm_ast_num_nodes(child);
	}
	return count;
}

grm_ast_t *grm_ast_get_child (const grm_ast_t *node, size_t index)
{
	grm_ast_t **ptr = grm_vector_get(node->children, index);
	if (!ptr) {
		return NULL;
	}
	return *ptr;
}

static size_t strncat_s (char *dst, const char *src, size_t limit)
{
	if (limit == 0) {
		return 0;
	}
	strncat(dst, src, limit);
	limit -= strlen(src);
	return (limit > 0) ? limit : 0;
}

static size_t recursive_strncat (char *dst, size_t limit, const grm_ast_t *node, bool is_root)
{
	size_t arity = grm_ast_num_children(node);
	if (is_root || arity) {
		limit = strncat_s(dst, "(", limit);
	}

	limit = strncat_s(dst, node->token.lexeme, limit);

	grm_ast_t *child;
	GRM_AST_FOREACH_CHILD(child, node) {
		limit = strncat_s(dst, " ", limit);
		limit = recursive_strncat(dst, limit, child, false);
	}

	if (is_root || arity) {
		limit = strncat_s(dst, ")", limit);
	}
	return limit;
}

int grm_ast_to_string (char *dst, size_t limit, const grm_ast_t *tree)
{
	dst[0] = '\0';
	return recursive_strncat(dst, limit, tree, true);
}

int grm_ast_free_root (grm_ast_t *root)
{
	grm_ast_t *child;
	GRM_AST_FOREACH_CHILD(child, root) {
		grm_ast_free_root(child);
	}
	grm_vector_free(root->children);
	free(root);
	return 0;
}
