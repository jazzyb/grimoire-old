#include "queue.h"

#include <stdlib.h>

struct grm_queue_ {
	const void **buffer;
	size_t front_idx;
	size_t back_idx;
	size_t capacity;
};

#define DEFAULT_CAPACITY 32

grm_queue_t *grm_queue_new (size_t init_capacity)
{
	grm_queue_t *q = calloc(1, sizeof(*q));
	if (!q) {
		return NULL;
	}
	q->capacity = (init_capacity) ? init_capacity : DEFAULT_CAPACITY;
	q->buffer = calloc(q->capacity, sizeof(*q->buffer));
	if (!q->buffer) {
		free(q);
		return NULL;
	}
	return q;
}

void grm_queue_free (grm_queue_t *q)
{
	free(q->buffer);
	free(q);
}

#define NEXT_INDEX(q, i) (((i) + 1) % (q)->capacity)

#define QUEUE_FOREACH(ptr, q)	\
	for (size_t _i = (ptr = (q)->buffer[(q)->front_idx], (q)->front_idx);	\
			_i != (q)->back_idx;	\
			_i = NEXT_INDEX(q, _i), ptr = (q)->buffer[_i])

static int resize_circular_buffer (grm_queue_t *q)
{
	size_t newcap = q->capacity * 2;
	const void **newbuf = calloc(newcap, sizeof(*newbuf));
	if (!newbuf) {
		return -1;
	}

	const void *ptr;
	size_t newback = 0;
	QUEUE_FOREACH(ptr, q) {
		newbuf[newback++] = ptr;
	}
	q->front_idx = 0;
	q->back_idx = newback;
	q->capacity = newcap;
	free(q->buffer);
	q->buffer = newbuf;
	return 0;
}

int grm_queue_push (grm_queue_t *q, const void *ptr)
{
	if (NEXT_INDEX(q, q->back_idx) == q->front_idx) {
		if (resize_circular_buffer(q) < 0) {
			return -1;
		}
	}
	q->buffer[q->back_idx] = ptr;
	q->back_idx = NEXT_INDEX(q, q->back_idx);
	return 0;
}

void *grm_queue_pop (grm_queue_t *q)
{
	if (q->front_idx == q->back_idx) {
		return NULL;
	}
	void *ptr = (void *)q->buffer[q->front_idx];
	q->front_idx = NEXT_INDEX(q, q->front_idx);
	return ptr;
}

bool grm_queue_empty (grm_queue_t *q)
{
	return q->front_idx == q->back_idx;
}

bool grm_queue_contains (grm_queue_t *q, const void *candidate)
{
	const void *ptr;
	QUEUE_FOREACH(ptr, q) {
		if (ptr == candidate) {
			return true;
		}
	}
	return false;
}

#define PREV_INDEX(q, i) (((i) - 1) % (q)->capacity)

void grm_queue_reverse (grm_queue_t *q)
{
	size_t start = q->front_idx;
	size_t end = PREV_INDEX(q, q->back_idx);
	while (start != end) {
		const void *tmp = q->buffer[start];
		q->buffer[start] = q->buffer[end];
		q->buffer[end] = tmp;

		start = NEXT_INDEX(q, start);
		if (start == end) {
			break;
		}
		end = PREV_INDEX(q, end);
	}
}
