#include "game.h"

#include "keywd.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct grm_game_ {
	grm_dl_t *db;			// datalog database

	grm_fact_t **bases;		// all base/1 facts
	size_t nbases;

	grm_fact_t **inits;		// all init/1 facts
	size_t ninits;

	intptr_t *roles;		// all roles in the game
	size_t nroles;

	grm_fact_t ***inputs;		// all input/2 facts for each role
	size_t *ninputs;

	char *description;		// copy of the game description

	grm_query_t *next_query;	// saved next/1 query -- used by next_state()
	grm_query_t *legal_query;	// saved legal/2 query -- used by legal_moves()
	grm_query_t *goal_query;	// saved goal/2 query -- used by reward()
};

static grm_keywd_t gdl1_keywords[NUM_GDL1_KEYWORDS] = {
	{ .id = GRM_BASE_ID,		.sym = "base",		.arity = 1 },
	{ .id = GRM_INPUT_ID,		.sym = "input",		.arity = 2 },
	{ .id = GRM_ROLE_ID,		.sym = "role",		.arity = 1 },
	{ .id = GRM_INIT_ID,		.sym = "init",		.arity = 1 },
	{ .id = GRM_TRUE_ID,		.sym = "true",		.arity = 1 },
	{ .id = GRM_NEXT_ID,		.sym = "next",		.arity = 1 },
	{ .id = GRM_LEGAL_ID,		.sym = "legal",		.arity = 2 },
	{ .id = GRM_DOES_ID,		.sym = "does",		.arity = 2 },
	{ .id = GRM_GOAL_ID,		.sym = "goal",		.arity = 2 },
	{ .id = GRM_TERMINAL_ID,	.sym = "terminal",	.arity = 0 },
};

/*
 * Query and save the facts as a null-terminated fact array.
 */
static int query_initial_info (grm_dl_t *db,
		grm_fact_t ***facts,
		size_t *count,
		const char *query_str,
		grm_error_t *error)
{
	grm_query_t *query = grm_dl_query_parse(db, query_str, error);
	if (!query) {
		return -1;
	}

	if (grm_dl_match_query(db, facts, count, query, error) < 0) {
		grm_query_free(query);
		return -1;
	}
	grm_query_free(query);
	return 0;
}

/*
 * Query and save all input/2 facts for each role.
 */
static int query_initial_inputs (grm_game_t *game, grm_error_t *error)
{
	grm_query_t *query = grm_dl_query_parse(game->db, "(input _ROLE_ ?move)", error);
	if (!query) {
		return -1;
	}

	game->inputs = calloc(game->nroles, sizeof(*game->inputs));
	if (!game->inputs) {
		grm_error_log_out_of_mem(error);
		goto free_query;
	}
	game->ninputs = calloc(game->nroles, sizeof(*game->ninputs));
	if (!game->ninputs) {
		grm_error_log_out_of_mem(error);
		goto free_inputs_array;
	}

	for (size_t i = 0; i < game->nroles; i++) {
		query[1].id = game->roles[i];
		if (grm_dl_match_query(game->db, game->inputs + i,
					game->ninputs + i, query, error) < 0) {
			goto free_inputs;
		}
	}

	grm_query_free(query);
	return 0;

free_inputs:
	for (size_t i = 0; i < game->nroles; i++) {
		grm_dl_free_facts(game->inputs[i], game->ninputs[i]);
	}
free_inputs_array:
	free(game->inputs);
free_query:
	grm_query_free(query);
	return -1;
}

/*
 * Query and save the roles for the game.
 */
static int query_roles (grm_game_t *game, grm_error_t *error)
{
	grm_query_t *query = grm_dl_query_parse(game->db, "(role ?role)", error);
	if (!query) {
		return -1;
	}

	grm_fact_t **facts;
	if (grm_dl_match_query(game->db, &facts, &game->nroles, query, error) < 0) {
		grm_query_free(query);
		return -1;
	}
	grm_query_free(query);

	game->roles = calloc(game->nroles, sizeof(*game->roles));
	if (!game->roles) {
		grm_dl_free_facts(facts, game->nroles);
		grm_error_log_out_of_mem(error);
		return -1;
	}
	for (size_t i = 0; i < game->nroles; i++) {
		game->roles[i] = facts[i][1].id;
	}
	grm_dl_free_facts(facts, game->nroles);
	return 0;
}

static void log_error (grm_error_t *error, grm_error_type_t type, size_t index)
{
	grm_token_t token = { .type = GRM_ATOM_LIT, .index = index, .lexeme = NULL };
	grm_error_log(error, type, &token);
}

static void log_undef_error (grm_error_t *error, char *term)
{
	grm_token_t token = { .type = GRM_EMPTY_TOKEN, .index = 0, .lexeme = term };
	grm_error_log(error, GRM_UNDEFINED_RELATION_ERR, &token);
}

/*
 * Return whether or not a fact OR a rule exists for the id.
 */
static bool relation_exists (grm_dl_t *db, intptr_t id, size_t *index)
{
	return  grm_fact_tbl_exists(db->fact_table, id, index) ||
		grm_rule_tbl_exists(db->rule_table, id, index);
}

/*
 * Do all the possible error checking on the game rules to verify they are
 * well-formed.
 *
 * Return true if they are valid; false otherwise and log the error.
 */
static bool is_gdl1_usage_valid (grm_dl_t *db, grm_error_t *error)
{
	size_t index;

	// role/1 expressions can only be facts
	if (grm_rule_tbl_exists(db->rule_table, GRM_ROLE_ID, &index)) {
		log_error(error, GRM_ILLEGAL_ROLE_RULE_ERR, index);
		return false;
	}
	// role/1 facts must exist
	if (!grm_fact_tbl_exists(db->fact_table, GRM_ROLE_ID, NULL)) {
		log_undef_error(error, "role/1");
		return false;
	}

	// init/1 cannot appear in rule bodies
	if (grm_rule_tbl_is_in_body(db->rule_table, GRM_INIT_ID, &index)) {
		log_error(error, GRM_ILLEGAL_INIT_BODY_ERR, index);
		return false;
	}
	// init/1 cannot depend on true, does, next, legal, goal, or terminal
	intptr_t ids[6] = { GRM_GOAL_ID, GRM_TERMINAL_ID, GRM_LEGAL_ID,
			GRM_NEXT_ID, GRM_TRUE_ID, GRM_DOES_ID };
	for (size_t i = 0; i < 6; i++) {
		if (grm_rule_tbl_depends_on(db->rule_table, GRM_INIT_ID, ids[i], &index)) {
			log_error(error, GRM_ILLEGAL_INIT_DEP_ERR, index);
			return false;
		}
	}
	// init/1 relations must exist
	if (!relation_exists(db, GRM_INIT_ID, NULL)) {
		log_undef_error(error, "init/1");
		return false;
	}

	// next/1 cannot appear in rule bodies
	if (grm_rule_tbl_is_in_body(db->rule_table, GRM_NEXT_ID, &index)) {
		log_error(error, GRM_ILLEGAL_NEXT_BODY_ERR, index);
		return false;
	}
	// next/1 relations must exist
	if (!relation_exists(db, GRM_NEXT_ID, NULL)) {
		log_undef_error(error, "next/1");
		return false;
	}

	// true/1 only appears in rule bodies
	if (relation_exists(db, GRM_TRUE_ID, &index)) {
		log_error(error, GRM_TRUE_DEFINED_ERR, index);
		return false;
	}

	// does/2 only appears in rule bodies
	if (relation_exists(db, GRM_DOES_ID, &index)) {
		log_error(error, GRM_DOES_DEFINED_ERR, index);
		return false;
	}

	// does/2 cannot be a dependency of legal, goal, or terminal
	for (size_t i = 0; i < 3; i++) {
		if (grm_rule_tbl_depends_on(db->rule_table, ids[i], GRM_DOES_ID, &index)) {
			log_error(error, GRM_ILLEGAL_DOES_DEP_ERR, index);
			return false;
		}
	}

	// relations must exist for base/1, input/2, legal/2, goal/2, and terminal/0
	if (!relation_exists(db, GRM_BASE_ID, NULL)) {
		log_undef_error(error, "base/1");
		return false;
	}
	if (!relation_exists(db, GRM_INPUT_ID, NULL)) {
		log_undef_error(error, "input/2");
		return false;
	}
	if (!relation_exists(db, GRM_LEGAL_ID, NULL)) {
		log_undef_error(error, "legal/2");
		return false;
	}
	if (!relation_exists(db, GRM_GOAL_ID, NULL)) {
		log_undef_error(error, "goal/2");
		return false;
	}
	if (!relation_exists(db, GRM_TERMINAL_ID, NULL)) {
		log_undef_error(error, "terminal/0");
		return false;
	}

	return true;
}

/*
 * MESSAGES FOR GDL-I DESCRIPTION ERRORS
 */
#define DOES_DEFINED_MSG (GRM_ERROR_LINE_COL_FORMAT "does/2 states cannot be explicitly defined")
#define ILLEGAL_DOES_DEP_MSG (GRM_ERROR_LINE_COL_FORMAT "does/2 cannot be a dependency of legal/2," \
		" goal/1, or terminal/0")
#define ILLEGAL_INIT_BODY_MSG (GRM_ERROR_LINE_COL_FORMAT "init/1 cannot appear in a rule body")
#define ILLEGAL_INIT_DEP_MSG (GRM_ERROR_LINE_COL_FORMAT "init/1 cannot depend on any other GDL-I terms")
#define ILLEGAL_NEXT_BODY_MSG (GRM_ERROR_LINE_COL_FORMAT "next/1 cannot appear in a rule body")
#define ILLEGAL_ROLE_RULE_MSG (GRM_ERROR_LINE_COL_FORMAT "role/1 cannot be a rule")
#define TRUE_DEFINED_MSG (GRM_ERROR_LINE_COL_FORMAT "true/1 states cannot be explicitly defined")
#define UNDEFINED_RELATION_MSG ("No %s relation is defined in the description")

int grm_game_error_message (char *dst,
		size_t limit,
		const char *buffer,
		grm_error_t *error)
{
	const char *fmt;
	switch (error->type) {
	case GRM_DOES_DEFINED_ERR:
		fmt = DOES_DEFINED_MSG; break;
	case GRM_ILLEGAL_DOES_DEP_ERR:
		fmt = ILLEGAL_DOES_DEP_MSG; break;
	case GRM_ILLEGAL_INIT_BODY_ERR:
		fmt = ILLEGAL_INIT_BODY_MSG; break;
	case GRM_ILLEGAL_INIT_DEP_ERR:
		fmt = ILLEGAL_INIT_DEP_MSG; break;
	case GRM_ILLEGAL_NEXT_BODY_ERR:
		fmt = ILLEGAL_NEXT_BODY_MSG; break;
	case GRM_ILLEGAL_ROLE_RULE_ERR:
		fmt = ILLEGAL_ROLE_RULE_MSG; break;
	case GRM_TRUE_DEFINED_ERR:
		fmt = TRUE_DEFINED_MSG; break;
	case GRM_UNDEFINED_RELATION_ERR:
		return snprintf(dst, limit, UNDEFINED_RELATION_MSG, error->token.lexeme);
	default:
		return grm_error_message(dst, limit, buffer, error);
	}

	size_t line = 0, col = 0;
	grm_error_line_column(&line, &col, buffer, error);
	return snprintf(dst, limit, fmt, line, col);
}

/*
 * Erase the given set.  Take an array of facts, modify the initial fact id to
 * be something else (e.g. legal/2 -> does/2), and add them to the given set.
 */
static int initialize_fact_set (grm_fact_set_t *set,
		grm_fact_t **facts,
		size_t count,
		intptr_t id,
		bool duplicate)
{
	grm_fact_t *fact;
	GRM_FACT_SET_FOREACH(fact, set) {
		grm_fact_free(fact);
	}
	grm_fact_set_erase(set);

	for (size_t i = 0; i < count; i++) {
		fact = facts[i];
		grm_fact_t *new = duplicate ? grm_fact_dup(fact) : fact;
		new[0].id = id;
		if (grm_fact_set_add(set, new) < 0) {
			return -1;
		}
	}
	return 0;
}

/*
 * Initialize our true/1 and does/2 fact sets.
 */
static int start_state_init (grm_state_t *state, grm_fact_t **inits, size_t count)
{
	state->truths = calloc(1, sizeof(*state->truths));
	state->deeds = calloc(1, sizeof(*state->deeds));
	if (!state->truths || !state->deeds
			|| grm_fact_set_init(state->truths) < 0
			|| grm_fact_set_init(state->deeds) < 0
			|| initialize_fact_set(state->truths, inits, count,
				GRM_TRUE_ID, true) < 0) {
		grm_game_state_free(state);
		return -1;
	}
	return 0;
}

int grm_game_state_init (grm_state_t *state)
{
	state->truths = state->deeds = NULL;
	return 0;
}

grm_dl_t *grm_game_database (grm_game_t *game)
{
	return game->db;
}

grm_game_t *grm_game_start (grm_state_t *state, const char *description, grm_error_t *error)
{
	// initialize space for our GDL engine
	grm_game_t *new = calloc(1, sizeof(*new));
	if (!new) {
		return NULL;
	}
	new->db = calloc(1, sizeof(*new->db));
	if (!new->db) {
		grm_error_log_out_of_mem(error);
		goto free_gdl;
	}
	// initialize the game database
	if (grm_dl_init(new->db) < 0) {
		grm_error_log_out_of_mem(error);
		goto deallocate_db;
	}
	if (grm_dl_set_keywords(new->db, NULL, gdl1_keywords, NUM_GDL1_KEYWORDS) < 0) {
		grm_error_log_out_of_mem(error);
		goto free_db;
	}
	if (grm_dl_parse(new->db, description, error) < 0) {
		goto free_db;
	}
	// save the game rules
	new->description = strdup(description);
	if (!new->description) {
		goto free_db;
	}
	// verify the GDL-I rules are valid
	if (!is_gdl1_usage_valid(new->db, error)) {
		goto free_game_rules;
	}
	// go ahead and save queries that won't change
	if (query_roles(new, error) < 0) {
		goto free_game_rules;
	}
	if (query_initial_inputs(new, error) < 0) {
		goto free_all_facts;
	}
	if (query_initial_info(new->db, &new->bases, &new->nbases, "(base ?state)", error) < 0) {
		goto free_all_facts;
	}
	if (query_initial_info(new->db, &new->inits, &new->ninits, "(init ?state)", error) < 0) {
		goto free_all_facts;
	}
	// initialize the queries that we will need to use over and over
	new->next_query = grm_dl_query_parse(new->db, "(next ?state)", error);
	if (!new->next_query) {
		goto free_queries;
	}
	new->legal_query = grm_dl_query_parse(new->db, "(legal _ROLE_ ?move)", error);
	if (!new->legal_query) {
		goto free_queries;
	}
	new->goal_query = grm_dl_query_parse(new->db, "(goal _ROLE_ ?prize)", error);
	if (!new->goal_query) {
		goto free_queries;
	}
	// initialize the state
	if (start_state_init(state, new->inits, new->ninits) < 0) {
		goto free_queries;
	}
	return new;

free_queries:
	if (new->next_query) {
		grm_query_free(new->next_query);
	}
	if (new->legal_query) {
		grm_query_free(new->legal_query);
	}
	if (new->goal_query) {
		grm_query_free(new->goal_query);
	}
free_all_facts:
	if (new->inputs) {
		for (size_t i = 0; i < new->nroles; i++) {
			grm_dl_free_facts(new->inputs[i], new->ninputs[i]);
		}
		free(new->inputs);
		free(new->ninputs);
	}
	grm_dl_free_facts(new->bases, new->nbases);
	grm_dl_free_facts(new->inits, new->ninits);
	free(new->roles);
free_game_rules:
	free(new->description);
free_db:
	grm_dl_free(new->db);
deallocate_db:
	free(new->db);
free_gdl:
	free(new);
	return NULL;
}

/*
 * Free the entire fact_set including facts.
 */
static void free_fact_set (grm_fact_set_t *set)
{
	grm_fact_t *fact;
	GRM_FACT_SET_FOREACH(fact, set) {
		grm_fact_free(fact);
	}
	grm_fact_set_free(set);
	free(set);
}

void grm_game_state_free (grm_state_t *state)
{
	if (state->truths) {
		free_fact_set(state->truths);
		state->truths = NULL;
	}
	if (state->deeds) {
		free_fact_set(state->deeds);
		state->deeds = NULL;
	}
}

void grm_game_free (grm_game_t *game)
{
	grm_fact_tbl_unset(game->db->fact_table, GRM_TRUE_ID);
	grm_fact_tbl_unset(game->db->fact_table, GRM_DOES_ID);
	grm_dl_free(game->db);
	free(game->db);
	grm_dl_free_facts(game->bases, game->nbases);
	grm_dl_free_facts(game->inits, game->ninits);
	for (size_t i = 0; i < game->nroles; i++) {
		grm_dl_free_facts(game->inputs[i], game->ninputs[i]);
	}
	free(game->inputs);
	free(game->ninputs);
	free(game->roles);
	free(game->description);
	grm_query_free(game->next_query);
	grm_query_free(game->legal_query);
	grm_query_free(game->goal_query);
	free(game);
}

/*
 * Set the state's truths and deeds as the true/1 and does/2 relations in the
 * database and erase the derived fact table.
 */
static void update_game_db (grm_game_t *game, grm_state_t *state)
{
	grm_fact_tbl_set(game->db->fact_table, GRM_TRUE_ID, state->truths);
	grm_fact_tbl_set(game->db->fact_table, GRM_DOES_ID, state->deeds);
	grm_fact_tbl_erase_all(game->db->derived_fact_table);
}

int grm_game_next_state (grm_state_t *next,
		grm_game_t *game,
		grm_state_t *current,
		grm_fact_t **moves)
{
	// set current::does to moves
	if (!current->deeds) {
		current->deeds = calloc(1, sizeof(*current->deeds));
		if (!current->deeds || grm_fact_set_init(current->deeds) < 0) {
			return -1;
		}
	}
	if (initialize_fact_set(current->deeds, moves, game->nroles, GRM_DOES_ID, true) < 0) {
		return -1;
	}

	// add truths and deeds to the game database
	update_game_db(game, current);

	// set next::true to current::next
	size_t count;
	grm_fact_t **nexts;
	if (grm_dl_match_query(game->db, &nexts, &count, game->next_query, NULL) < 0) {
		return -1;
	}
	if (!next->truths) {
		next->truths = calloc(1, sizeof(*next->truths));
		if (!next->truths || grm_fact_set_init(next->truths) < 0) {
			grm_dl_free_facts(nexts, count);
			return -1;
		}
	}
	if (initialize_fact_set(next->truths, nexts, count, GRM_TRUE_ID, false) < 0) {
		grm_dl_free_facts(nexts, count);
		return -1;
	}
	free(nexts);
	return 0;
}

const intptr_t *grm_game_roles (grm_game_t *game, size_t *count)
{
	*count = game->nroles;
	return game->roles;
}

const grm_fact_t **grm_game_propositions (grm_game_t *game, size_t *count)
{
	*count = game->nbases;
	return (const grm_fact_t **)game->bases;
}

const grm_fact_t **grm_game_actions (grm_game_t *game, size_t *count, intptr_t role)
{
	for (size_t i = 0; i < game->nroles; i++) {
		if (role == game->roles[i]) {
			*count = game->ninputs[i];
			return (const grm_fact_t **)game->inputs[i];
		}
	}
	return NULL;
}

const grm_fact_t **grm_game_inits (grm_game_t *game, size_t *count)
{
	*count = game->ninits;
	return (const grm_fact_t **)game->inits;
}

grm_fact_t **grm_game_legal_moves (grm_game_t *game,
		grm_state_t *state,
		size_t *count,
		intptr_t role)
{
	update_game_db(game, state);
	game->legal_query[1].id = role;
	grm_fact_t **legals;
	if (grm_dl_match_query(game->db, &legals, count, game->legal_query, NULL) < 0) {
		return NULL;
	}
	return legals;
}

int grm_game_reward (grm_game_t *game, grm_state_t *state, intptr_t role)
{
	update_game_db(game, state);
	game->goal_query[1].id = role;
	size_t count;
	grm_fact_t **goal;
	grm_dl_match_query(game->db, &goal, &count, game->goal_query, NULL);
	assert(count == 1);
	const char *reward = grm_dl_get_symbol(game->db, goal[0][2].id);
	assert(reward);
	grm_dl_free_facts(goal, 1);
	return strtol(reward, NULL, 10);
}

bool grm_game_is_terminal (grm_game_t *game, grm_state_t *state)
{
	update_game_db(game, state);
	grm_query_t terminal = { .id = GRM_TERMINAL_ID,
		.arity = 0,
		.total_len = 1,
		.child_off = 0,
		.token_index = 0
	};
	return (bool)grm_dl_match_query(game->db, NULL, NULL, &terminal, NULL);
}
