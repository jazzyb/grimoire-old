#include "fact.h"

#include "ignore_warnings.h"
#include "keywd.h"
#include "lexer.h"
#include "logging.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

/*
 * Initialize a single fact entry from an AST node.
 * Return 0 on success, -1 on error.
 */
static int convert_token_to_fact (grm_fact_t *fact,
		grm_fact_ctx_t *ctx,
		size_t child_off,
		const grm_ast_t *node,
		bool only_atoms,
		grm_error_t *error)
{
	const grm_token_t *token = grm_ast_get_token(node);
	if (only_atoms && token->type == GRM_VAR_LIT) {
		grm_error_log(error, GRM_ILLEGAL_FACT_ERR, token);
		return -1;
	}

	assert(token->type == GRM_ATOM_LIT ||
			token->type == GRM_VAR_LIT ||
			token->type == GRM_OR_KW ||
			token->type == GRM_NOT_KW ||
			token->type == GRM_DISTINCT_KW);

	fact->token_index = token->index;
	fact->total_len = grm_ast_num_nodes(node);
	fact->arity = grm_ast_num_children(node);
	fact->child_off = fact->arity ? child_off : 0;

	char *key = grm_keywd_symbol_arity(token->lexeme, fact->arity);
	if (!key) {
		grm_error_log_out_of_mem(error);
		return -1;
	}

	fact->id = (intptr_t)grm_dict_get(ctx->sym_id_map, key);
	if (fact->id) {
		free(key);
		return 0;
	}

	char *orig_string = strdup(token->lexeme);
	if (!orig_string) {
		grm_error_log_out_of_mem(error);
		return -1;
	}

	// If we have *not* seen this symbol/arity before, save the symbol and
	// new ID.
	intptr_t id = (*ctx->next_id)++;
	fact->id = (token->type == GRM_VAR_LIT) ? -id : id;
	if (grm_vector_set(ctx->symbols, id, &orig_string) < 0 ||
			grm_dict_set(ctx->sym_id_map, key, (void *)fact->id) < 0) {
		grm_error_log_out_of_mem(error);
		return -1;
	}
	grm_log(LOG_DEBUG, "'%s' is ID:%ld", key, fact->id);
	return 0;
}

/*
 * Do a breadth-first traversal of the tree to generate the fact.  BFT
 * guarantees that sibling nodes are consecutive in the fact array which is
 * something we assume in other parts of the code.
 *
 * Return 0 on success, -1 on error.
 */
static int breadth_first_convert (grm_fact_t *fact,
		size_t nelem,
		grm_fact_ctx_t *ctx,
		const grm_ast_t *root,
		bool only_atoms,
		grm_error_t *error)
{
	const grm_ast_t *nodeq[nelem + 1];
	nodeq[0] = root;
	nodeq[nelem] = NULL;

	size_t front = 0, child_idx = 1;
	const grm_ast_t *node = nodeq[front++];
	while (node) {
		size_t child_off = child_idx - front + 1;
		if (convert_token_to_fact(fact++, ctx, child_off, node, only_atoms, error) < 0) {
			return -1;
		}

		grm_ast_t *child;
		GRM_AST_FOREACH_CHILD(child, node) {
			nodeq[child_idx++] = child;
		}
		node = nodeq[front++];
	}
	return 0;
}

/*
 * Create a new fact array.
 * Returns NULL on error.
 */
static grm_fact_t *new_fact (grm_fact_ctx_t *ctx,
		const grm_ast_t *tree,
		size_t nelem,
		bool only_atoms,
		grm_error_t *error)
{
	grm_fact_t *new = calloc(nelem, sizeof(*new));
	if (!new) {
		grm_error_log_out_of_mem(error);
		return NULL;
	}
	if (breadth_first_convert(new, nelem, ctx, tree, only_atoms, error) < 0) {
		free(new);
		return NULL;
	}
	return new;
}

/*
 * Generate a new fact array.  The fact context needs to be initialized before
 * this routine is called.
 */
grm_fact_t *grm_fact_new (grm_fact_ctx_t *ctx,
		const grm_ast_t *tree,
		grm_error_t *error)
{
	size_t nelem = grm_ast_num_nodes(tree);
	return new_fact(ctx, tree, nelem, true, error);
}

void grm_fact_free (grm_fact_t *fact)
{
	free(fact);
}

/*
 * Look through the fact context to see if we have seen this relative variable
 * ID before.
 *
 * Return the ID (a number <= 0), or 1 on error.
 */
static intptr_t find_rel_var_id (grm_query_ctx_t *ctx, intptr_t id)
{
	for (size_t i = 0; i < grm_vector_length(ctx->var_id_map); i++) {
		if (*(intptr_t *)grm_vector_get(ctx->var_id_map, i) == id) {
			return -(intptr_t)i;
		}
	}
	return 1;
}

/*
 * Map the query-relative variable ID to the global variable ID in the vector.
 * Afterwards, we can look-up global IDs via:
 *     global_id = *(intptr_t *)grm_vector_get(v, query_relative_id).
 */
static int set_query_context (grm_query_ctx_t *ctx, size_t nelem, grm_query_t *query)
{
	for (size_t i = 0; i < nelem; i++) {
		grm_query_t *tmp = query + i;
		if (grm_query_is_variable(tmp)) {
			intptr_t id = find_rel_var_id(ctx, tmp->id);
			if (id > 0) {
				// we did not find a relative id for this global var id
				id = (*ctx->rel_id)--;
				if (grm_vector_set(ctx->var_id_map, -id, &tmp->id) < 0) {
					return -1;
				}
			}
			tmp->id = id;
		}
	}
	return 0;
}

/*
 * Generate a new query array from the AST.  Both the fact context and the
 * query context need to be initialized before calling this routine.
 */
grm_query_t *grm_query_new (grm_fact_ctx_t *fctx,
		grm_query_ctx_t *qctx,
		const grm_ast_t *tree,
		grm_error_t *error)
{
	size_t nelem = grm_ast_num_nodes(tree);
	grm_query_t *q = new_fact(fctx, tree, nelem, false, error);
	if (!q) {
		return NULL;
	}
	if (set_query_context(qctx, nelem, q) < 0) {
		grm_query_free(q);
		grm_error_log_out_of_mem(error);
		return NULL;
	}
	return q;
}

void grm_query_free (grm_query_t *query)
{
	free(query);
}

/*
 * Recursively walk a query and see if 'fact' matches 'query'.  When we run
 * across a variable in the query, save the fact that matches the variable in
 * 'answer'.
 *
 * Return true for a match, false otherwise.
 */
static bool recursive_compare (grm_fact_t **answer,
		size_t size,
		grm_query_t *query,
		grm_fact_t *fact)
{
	if (grm_query_is_variable(query)) {
		size_t idx = -query->id;
		assert(idx < size);
		if (answer[idx]) {
			// make sure multiple references to the same variable match
			return recursive_compare(NULL, 0, answer[idx], fact);
		}
		answer[idx] = fact;
		return true;
	} else if (query->id != fact->id) {
		return false;
	}

	assert(query->arity == fact->arity);
	for (size_t i = 0; i < fact->arity; i++) {
		grm_query_t *q = query + query->child_off + i;
		grm_fact_t *f = fact + fact->child_off + i;
		if (!recursive_compare(answer, size, q, f)) {
			return false;
		}
	}
	return true;
}

/*
 * Return true if 'fact' is a match for the query.  Set any values for the
 * variables in query in answer.  Return false if not a match.
 */
bool grm_query_match (grm_fact_t **answer,
		size_t size,
		grm_query_t *query,
		grm_fact_t *fact,
		bool initialize_answers)
{
	if (initialize_answers) {
		memset(answer, 0, sizeof(*answer) * size);
	}
	return recursive_compare(answer, size, query, fact);
}

/*
 * Write a fact value in the place of a variable in the query.
 */
static void recursive_replace_variable (grm_fact_t *dst,
		size_t idx,
		size_t *new_children,
		const grm_fact_t *src)
{
	memcpy(dst, src, sizeof(*dst));
	dst->child_off = dst->arity ? *new_children - idx : 0;
	*new_children += dst->arity;
	for (size_t i = 0; i < dst->arity; i++) {
		size_t new_idx = idx + dst->child_off + i;
		grm_fact_t *d = dst + dst->child_off + i;
		const grm_fact_t *s = src + src->child_off + i;
		recursive_replace_variable(d, new_idx, new_children, s);
	}
}

/*
 * Copy 'query' (along with any variable values) into 'fact'.
 *
 * Return the total nodes below the current fact entry (children,
 * grandchildren, etc. -- including the current entry).
 */
static size_t recursive_copy_fact (grm_fact_t *fact,
		size_t idx,
		size_t *new_children,
		grm_fact_t **vars,
		const grm_query_t *query)
{
	if (grm_query_is_variable(query)) {
		intptr_t id = -query->id;
		recursive_replace_variable(fact, idx, new_children, vars[id]);
		return fact->total_len;
	}

	size_t total_len = 1;
	memcpy(fact, query, sizeof(*fact));
	for (size_t i = 0; i < fact->arity; i++) {
		size_t off = fact->child_off + i;
		total_len += recursive_copy_fact(fact + off,
				idx + off, new_children, vars,
				query + off);
	}
	fact->total_len = total_len;
	return total_len;
}

/*
 * Derive a new fact from the 'query' and 'answer' returned from
 * grm_query_match().
 */
START_IGNORE_UNUSED_PARAMETER("size")
grm_fact_t *grm_fact_derive (grm_fact_t **answer, size_t size, const grm_query_t *query)
{
	size_t new_size, child_off;
	new_size = child_off = query->total_len;
	for (size_t i = 0; i < query->total_len; i++) {
		if (grm_query_is_variable(query + i)) {
			intptr_t id = -query[i].id;
			assert(id >= 0 && (unsigned)id < size);
			new_size += answer[id]->total_len - 1;
		}
	}

	grm_fact_t *new = calloc(new_size, sizeof(*new));
	if (!new) {
		return NULL;
	}
	recursive_copy_fact(new, 0, &child_off, answer, query);
	return new;
}
END_IGNORE_UNUSED_PARAMETER("size")

static void copy_fact (grm_fact_t *dst, grm_fact_t *src)
{
	grm_fact_t *q[src->total_len];
	q[0] = src;
	size_t child_idx = 1;
	for (size_t i = 0; i < src->total_len; i++) {
		size_t child_off = child_idx - i;
		grm_fact_t *d = dst + i, *s = q[i];
		memcpy(d, s, sizeof(*d));
		d->child_off = d->arity ? child_off : 0;
		for (size_t j = 0; j < s->arity; j++) {
			q[child_idx++] = s + s->child_off + j;
		}
	}
}

/*
 * Copy the given fact.  Return a duplicate or NULL on error.
 */
grm_fact_t *grm_fact_dup (grm_fact_t *fact)
{
	grm_fact_t *new = calloc(fact->total_len, sizeof(*new));
	if (!new) {
		return NULL;
	}
	copy_fact(new, fact);
	return new;
}

/*
 * Recusively generate an AST tree from the fact (or query) array.
 * Return 0 on success, -1 otherwise.
 */
static int recursive_generate_tree (grm_ast_t *root,
		grm_fact_ctx_t *fctx,
		grm_query_ctx_t *qctx,
		const grm_fact_t *fact)
{
	grm_token_t token;
	intptr_t id = fact->id;
	if (grm_query_is_variable(fact)) {
		id = -*(intptr_t *)grm_vector_get(qctx->var_id_map, -id);
		token.type = GRM_VAR_LIT;
	} else {
		switch (id) {
		case GRM_OR_ID:
			token.type = GRM_OR_KW;
			break;
		case GRM_NOT_ID:
			token.type = GRM_NOT_KW;
			break;
		case GRM_DISTINCT_ID:
			token.type = GRM_DISTINCT_KW;
			break;
		default:
			token.type = GRM_ATOM_LIT;
		}
	}

	token.lexeme = *(char **)grm_vector_get(fctx->symbols, id);
	token.index = fact->token_index;
	grm_ast_set_token(root, &token);

	for (size_t i = 0; i < fact->arity; i++) {
		grm_ast_t *child = grm_ast_add_child(root);
		if (!child) {
			return -1;
		}
		const grm_fact_t *next = fact + fact->child_off + i;
		if (recursive_generate_tree(child, fctx, qctx, next) < 0) {
			return -1;
		}
	}
	return 0;
}

static grm_ast_t *make_tree (grm_fact_ctx_t *fctx,
		grm_query_ctx_t *qctx,
		const grm_fact_t *fact)
{
	grm_ast_t *root = grm_ast_new_root();
	if (!root) {
		return NULL;
	}
	if (recursive_generate_tree(root, fctx, qctx, fact) < 0) {
		grm_ast_free_root(root);
		return NULL;
	}
	return root;
}

grm_ast_t *grm_fact_to_ast (grm_fact_ctx_t *ctx, const grm_fact_t *fact)
{
	return make_tree(ctx, NULL, fact);
}

grm_ast_t *grm_query_to_ast (grm_fact_ctx_t *fctx,
		grm_query_ctx_t *qctx,
		const grm_fact_t *fact)
{
	return make_tree(fctx, qctx, fact);
}

typedef struct {
	grm_fact_t *fact;
	size_t keylen;
	unsigned char key[];
} fact_entry_t;

int grm_fact_set_free (grm_fact_set_t *set)
{
	grm_fact_set_erase(set);
	grm_set_free(set);
	return 0;
}

/*
 * Generate the canonical sequence (key) that will be used to store the fact
 * in a hash table.
 */
static void canonical_sequence (intptr_t *key, grm_fact_t *fact)
{
	grm_fact_t *q[fact->total_len];
	q[0] = fact;
	size_t back = 1;
	for (size_t i = 0; i < fact->total_len; i++) {
		key[i] = q[i]->id;
		for (size_t j = 0; j < q[i]->arity; j++) {
			q[back++] = q[i] + q[i]->child_off + j;
		}
	}
}

int grm_fact_set_add (grm_fact_set_t *set, grm_fact_t *fact)
{
	size_t keylen = sizeof(fact->id) * fact->total_len;
	fact_entry_t *entry = calloc(1, sizeof(*entry) + keylen);
	if (!entry) {
		return -1;
	}
	entry->fact = fact;
	entry->keylen = keylen;
	canonical_sequence((intptr_t *)entry->key, fact);
	int rc = grm_htable_set(set, entry->key, entry->keylen, entry);
	if (rc) {
		free(entry);
	}
	return rc;
}

int grm_fact_set_erase (grm_fact_set_t *set)
{
	fact_entry_t *entry;
	GRM_HTABLE_FOREACH(NULL, entry, set) {
		free(entry);
	}
	return 0;
}

grm_fact_tbl_t *grm_fact_tbl_new (void)
{
	grm_fact_tbl_t *new = calloc(1, sizeof(*new));
	if (!new) {
		return NULL;
	}
	new->facts = grm_vector_new(grm_fact_set_t*);
	if (!new->facts) {
		free(new);
		return NULL;
	}
	new->validators = grm_vector_new(grm_fact_validator_t);
	if (!new->validators) {
		grm_vector_free(new->facts);
		free(new);
		return NULL;
	}
	return new;
}

int grm_fact_tbl_add_validator (grm_fact_tbl_t *tbl, intptr_t id, grm_fact_validator_t validate)
{
	return grm_vector_set(tbl->validators, id, &validate);
}

void grm_fact_tbl_free (grm_fact_tbl_t *tbl)
{
	grm_fact_tbl_erase_all(tbl);
	grm_vector_free(tbl->facts);
	grm_vector_free(tbl->validators);
	free(tbl);
}

void grm_fact_tbl_erase_all (grm_fact_tbl_t *tbl)
{
	grm_fact_set_t *set;
	GRM_FACT_TBL_FOREACH(set, tbl) {
		grm_fact_t *fact;
		GRM_FACT_SET_FOREACH(fact, set) {
			grm_fact_free(fact);
		}
		grm_fact_set_free(set);
		free(set);
	}
	grm_vector_erase(tbl->facts);
}

int grm_fact_tbl_add (grm_fact_tbl_t *fact_table, grm_fact_t *fact, grm_error_t *error)
{
	intptr_t idx = fact->id;
	grm_fact_validator_t *validate = grm_vector_get(fact_table->validators, idx);
	if (validate && *validate && !((*validate)(fact, fact_table, error))) {
		return -1;
	}

	bool new_set = false;
	grm_fact_set_t *set = grm_fact_tbl_get(fact_table, idx);
	if (!set) {
		set = calloc(1, sizeof(*set));
		if (!set) {
			grm_error_log_out_of_mem(error);
			return -1;
		}
		if (grm_fact_set_init(set) < 0) {
			grm_error_log_out_of_mem(error);
			free(set);
			return -1;
		}
		new_set = true;
	}

	int present = grm_fact_set_add(set, fact);
	if (present < 0) {
		goto err_free_set;
	}
	if (new_set && grm_fact_tbl_set(fact_table, idx, set) < 0) {
		goto err_free_set;
	}
	return present;

err_free_set:
	if (new_set) {
		grm_fact_set_free(set);
		free(set);
	}
	grm_error_log_out_of_mem(error);
	return -1;
}

int grm_fact_tbl_set (grm_fact_tbl_t *tbl, intptr_t fact_id, grm_fact_set_t *set)
{
	return grm_vector_set(tbl->facts, fact_id, &set);
}

int grm_fact_tbl_unset (grm_fact_tbl_t *tbl, intptr_t fact_id)
{
	void *null = NULL;
	return grm_vector_set(tbl->facts, fact_id, &null);
}

grm_fact_set_t *grm_fact_tbl_get(grm_fact_tbl_t *tbl, intptr_t fact_id)
{
	grm_fact_set_t **rowp = grm_vector_get(tbl->facts, fact_id);
	return rowp ? *rowp : NULL;
}

bool grm_fact_tbl_exists(grm_fact_tbl_t *tbl, intptr_t fact_id, size_t *index)
{
	grm_fact_set_t *set = grm_fact_tbl_get(tbl, fact_id);
	if (!set) {
		return false;
	}

	// just grab the index of the first fact in the set which may not be
	// the first defined fact
	grm_fact_t *fact;
	GRM_FACT_SET_FOREACH(fact, set) {
		if (index) {
			*index = fact[0].token_index;
		}
		return true;
	}

	// XXX We should never reach here.
	assert(0);
	return -1;
}
