#pragma once

#include <stddef.h>

enum {
	// datalog keyword IDs
	GRM_OR_ID = 1,		//	or/2
	GRM_NOT_ID,		//	not/1
	GRM_DISTINCT_ID,	//	distinct/2
#define NUM_DATALOG_KEYWORDS 3

	// GDL-I keyword IDs
	GRM_BASE_ID,		//	base/1
	GRM_INPUT_ID,		//  5	input/2
	GRM_ROLE_ID,		//	role/1
	GRM_INIT_ID,		//	init/1
	GRM_TRUE_ID,		//	true/1
	GRM_NEXT_ID,		//	next/1
	GRM_LEGAL_ID,		// 10	legal/2
	GRM_DOES_ID,		//	does/2
	GRM_GOAL_ID,		//	goal/2
	GRM_TERMINAL_ID,	//	terminal/0
#define NUM_GDL1_KEYWORDS 10

	// GDL-II keyword IDs
	GRM_SEES_ID,		//	sees/2
	GRM_RANDOM_ID,		// 15	random/0
#define NUM_GDL2_KEYWORDS 2
};

typedef struct {
	int id;
	char *sym;
	size_t arity;
} grm_keywd_t;

char *grm_keywd_symbol_arity(const char *symbol, size_t arity);
