#pragma once

#include <stddef.h>

struct grm_htable_entry_;

typedef struct {
	size_t size;
	size_t num_entries;
	size_t resize_limit;
	struct grm_htable_entry_ *buckets;
} grm_htable_t;

/******************************************************************************
 * HASH TABLE FUNCTIONS
 *****************************************************************************/

int grm_htable_init(grm_htable_t *htable);
int grm_htable_free(grm_htable_t *htable);
static inline size_t grm_htable_size(grm_htable_t *htable) { return htable->num_entries; }

/*
 * Set the value into the hash table with the given key.  Returns 0 on
 * success.  If the key already exists in the table, returns 1 and does not
 * update the value in the table.  On error, returns -1.
 */
int grm_htable_set(grm_htable_t *htable,
		const unsigned char *key,
		size_t keylen,
		const void *data);

/*
 * Returns the value of the given key or NULL if the key is not in the hash
 * table.
 */
void *grm_htable_get(grm_htable_t *htable, const unsigned char *key, size_t keylen);

/*
 * Removes the entry with the given key from the hash table.  Returns 0 on
 * success.  If the key does not exist in the table, returns 1.
 */
int grm_htable_del(grm_htable_t *htable, const unsigned char *key, size_t keylen);

/*
 * Iterate through the values in the hash table.  If 'keyp' is not NULL, then
 * it is set to the value of the key.
 */
typedef void** grm_htable_iter_t;

grm_htable_iter_t grm_htable_begin(grm_htable_t *htable, unsigned char **keyp);
grm_htable_iter_t grm_htable_next(grm_htable_t *htable,
		grm_htable_iter_t iter,
		unsigned char **keyp);

#define GRM_HTABLE_FOREACH(keyp, value, htable)						\
	for (void **_iter = grm_htable_begin(htable, (unsigned char **)(keyp));		\
			_iter && (value = *_iter);					\
			_iter = grm_htable_next(htable, _iter, (unsigned char **)(keyp)))

/******************************************************************************
 * DICTIONARY FUNCTIONS
 *****************************************************************************/

typedef grm_htable_t grm_dict_t;

/*
 * Wrappers for htable functions when the key is a simple ASCII string.
 */
#define grm_dict_init grm_htable_init
#define grm_dict_free grm_htable_free
#define grm_dict_size grm_htable_size
int grm_dict_set(grm_dict_t *dict, const char *key, const void *data);
void *grm_dict_get(grm_dict_t *dict, const char *key);
int grm_dict_del(grm_dict_t *dict, const char *key);

/******************************************************************************
 * SET FUNCTIONS
 *****************************************************************************/

typedef grm_htable_t grm_set_t;

#define grm_set_init grm_htable_init
#define grm_set_free grm_htable_free
#define grm_set_size grm_htable_size

int grm_set_add(grm_set_t *set, const void *data, size_t length);

/*
 * Add every element that is in s2 to s1.
 */
int grm_set_union(grm_set_t *s1, grm_set_t *s2);

/*
 * Remove every element that is in s2 from s1.
 */
int grm_set_diff(grm_set_t *s1, grm_set_t *s2);
