#include "datalog.h"

#include "logging.h"
#include "parser.h"
#include "queue.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

// datalog specific keywords:
static grm_keywd_t datalog_keywords[NUM_DATALOG_KEYWORDS] = {
	{ .id = GRM_OR_ID, 		.sym = "or", 		.arity = 2 },
	{ .id = GRM_NOT_ID, 		.sym = "not", 		.arity = 1 },
	{ .id = GRM_DISTINCT_ID,	.sym = "distinct", 	.arity = 2 },
};

/******************************************************************************
 * DATALOG INITIALIZATION AND FINALIZATION CODE
 *****************************************************************************/

int grm_dl_init (grm_dl_t *db)
{
	if (grm_dict_init(&db->sym_id_map) < 0) {
		return -1;
	}

	db->symbols = grm_vector_new(char*);
	if (!db->symbols) {
		goto err_free_sym_id_map;
	}

	db->fact_table = grm_fact_tbl_new();
	if (!db->fact_table) {
		goto err_free_symbols;
	}

	db->rule_table = grm_rule_tbl_new();
	if (!db->rule_table) {
		goto err_free_fact_table;
	}

	db->derived_fact_table = grm_fact_tbl_new();
	if (!db->derived_fact_table) {
		goto err_free_rule_table;
	}

	db->next_id = 1;
	if (grm_dl_set_keywords(db, NULL, datalog_keywords, NUM_DATALOG_KEYWORDS) < 0) {
		goto err_free_derived_facts;
	}
	return 0;

err_free_derived_facts:
	grm_fact_tbl_free(db->derived_fact_table);
err_free_rule_table:
	grm_rule_tbl_free(db->rule_table);
err_free_fact_table:
	grm_fact_tbl_free(db->fact_table);
err_free_symbols:
	grm_vector_free(db->symbols);
err_free_sym_id_map:
	grm_dict_free(&db->sym_id_map);
	return -1;
}

int grm_dl_free (grm_dl_t *db)
{
	// free all rules
	grm_rule_tbl_free(db->rule_table);

	// free all facts
	grm_fact_tbl_free(db->fact_table);
	grm_fact_tbl_free(db->derived_fact_table);

	// free all the symbols
	for (size_t i = 0; i < grm_vector_length(db->symbols); i++) {
		char **sym = grm_vector_get(db->symbols, i);
		if (sym) {
			free(*sym);
		}
	}
	grm_vector_free(db->symbols);

	// free all the symbol/arity keys
	char *key;
	void *_;
	GRM_HTABLE_FOREACH(&key, _, &db->sym_id_map) {
		free(key);
	}
	grm_dict_free(&db->sym_id_map);
	return 0;
}

int grm_dl_set_keywords (grm_dl_t *db, intptr_t *ids, grm_keywd_t *keywords, size_t size)
{
	for (size_t i = 0; i < size; i++) {
		intptr_t id = db->next_id++;
		grm_keywd_t *kw = keywords + i;
		if (kw->id) {
			assert(id == kw->id);
		}
		char *sym = strdup(kw->sym);
		if (grm_vector_set(db->symbols, id, &sym) < 0) {
			return -1;
		}
		char *sym_arity = grm_keywd_symbol_arity(kw->sym, kw->arity);
		if (grm_dict_set(&db->sym_id_map, sym_arity, (void *)id) < 0) {
			return -1;
		}
		if (ids) {
			ids[i] = id;
		}
		grm_log(LOG_DEBUG, "'%s' is ID:%ld", sym_arity, id);
	}
	return 0;
}

/******************************************************************************
 * DATALOG UPDATE AND PARSING CODE
 *****************************************************************************/

#define SET_FACT_CTX(db)			\
	{ .symbols = (db)->symbols,		\
	  .sym_id_map = &(db)->sym_id_map,	\
	  .next_id = &(db)->next_id }

int grm_dl_update (grm_dl_t *db, const grm_ast_t *tree, grm_error_t *error)
{
	grm_fact_ctx_t fctx = SET_FACT_CTX(db);
	if (grm_ast_get_token(tree)->type == GRM_RULE_DEF) {
		grm_rule_t *rule = grm_rule_new(&fctx, tree, error);
		if (!rule) {
			return -1;
		}
		if (grm_rule_tbl_add(db->rule_table, rule, error) < 0) {
			grm_rule_free(rule);
			return -1;
		}
	} else {
		grm_fact_t *fact = grm_fact_new(&fctx, tree, error);
		if (!fact) {
			return -1;
		}

		int rc = grm_fact_tbl_add(db->fact_table, fact, error);
		if (rc != 0) {
			grm_fact_free(fact);
			return rc;
		}
	}

	// XXX if we update anything, expire all our derived facts
	grm_fact_tbl_erase_all(db->derived_fact_table);
	return 0;
}

int grm_dl_parse (grm_dl_t *db, const char *input, grm_error_t *error)
{
	grm_parser_t parser;
	if (grm_parser_init(&parser, input) < 0) {
		grm_error_log_out_of_mem(error);
		return -1;
	}

	grm_ast_t *tree = grm_parser_parse(&parser, error);
	if (!tree) {
		grm_parser_free(&parser);
		return -1;
	}

	int ret = 0;
	for (size_t i = 0; i < grm_ast_num_children(tree); i++) {
		grm_ast_t *branch = grm_ast_get_child(tree, i);
		if (grm_dl_update(db, branch, error) < 0) {
			ret = -1;
			break;
		}
	}

	grm_ast_free_root(tree);
	grm_parser_free(&parser);
	return ret;
}

#define BEGIN_TEMP_QUERY_CTX(qctx)			\
do {							\
	intptr_t _rel_id = 0;				\
	grm_query_ctx_t qctx = { .rel_id = &_rel_id,	\
		.var_id_map = grm_vector_new(intptr_t) };

#define END_TEMP_QUERY_CTX(qctx)			\
	grm_vector_free(qctx.var_id_map);		\
} while (false);

grm_query_t *grm_dl_query_new (grm_dl_t *db, const grm_ast_t *tree, grm_error_t *error)
{
	grm_query_t *query;
	grm_fact_ctx_t fctx = SET_FACT_CTX(db);
	BEGIN_TEMP_QUERY_CTX(qctx)
	{
		query = grm_query_new(&fctx, &qctx, tree, error);
	}
	END_TEMP_QUERY_CTX(qctx)
	return query;
}

grm_query_t *grm_dl_query_parse (grm_dl_t *db, const char *string, grm_error_t *error)
{
	grm_parser_t parser;
	if (grm_parser_init(&parser, string) < 0) {
		grm_error_log_out_of_mem(error);
		return NULL;
	}

	grm_ast_t *tree = grm_parser_parse(&parser, error);
	if (!tree) {
		grm_parser_free(&parser);
		return NULL;
	}

	grm_query_t *query = NULL;
	if (grm_ast_num_children(tree) == 1) {
		query = grm_dl_query_new(db, grm_ast_get_child(tree, 0), error);
	} else {
		grm_error_log(error, GRM_INVALID_QUERY_ERR, NULL);
	}

	grm_ast_free_root(tree);
	grm_parser_free(&parser);
	return query;
}

/*
 * Return the number of variables in 'query'.
 */
static size_t count_vars (const grm_query_t *query)
{
	size_t count = 0;
	for (size_t i = 0; i < query->total_len; i++) {
		if (grm_query_is_variable(query + i)) {
			count += 1;
		}
	}
	return count;
}

int grm_dl_fact_to_string (grm_dl_t *db, char *dst, size_t size, const grm_fact_t *src)
{
	if (count_vars(src) > 0) {
		return -1;
	}

	grm_fact_ctx_t fctx = SET_FACT_CTX(db);
	grm_ast_t *tree = grm_fact_to_ast(&fctx, src);
	if (!tree) {
		return -1;
	}
	int ret = grm_ast_to_string(dst, size, tree);
	grm_ast_free_root(tree);
	return size - ret;
}

const char *grm_dl_get_symbol (grm_dl_t *db, intptr_t id)
{
	const char **ptr = grm_vector_get(db->symbols, id);
	return ptr ? *ptr : NULL;
}

/******************************************************************************
 * DATALOG QUERY CODE
 *****************************************************************************/

/*
 * Find the matches for this query in the fact table.
 */
static int fact_table_matches (grm_fact_tbl_t *fact_table,
		grm_fact_set_t *matches,
		grm_query_t *query,
		size_t nvars)
{
	grm_fact_set_t *facts = grm_fact_tbl_get(fact_table, query->id);
	if (!facts) {
		return 0;
	}

	grm_fact_t *vars[nvars];
	memset(vars, 0, sizeof(*vars) * nvars);
	grm_fact_t *fact;
	GRM_FACT_SET_FOREACH(fact, facts) {
		if (grm_query_match(vars, nvars, query, fact, true)) {
			if (grm_fact_set_add(matches, fact) < 0) {
				return -1;
			}
		}
	}
	return 0;
}

/*
 * Push all the dependencies for 'rule' onto the rule queue.
 */
static int push_dependencies (grm_dl_t *db, grm_queue_t *q, grm_rule_t *rule)
{
	for (size_t i = 0; i < rule->dep_length; i++) {
		intptr_t id = rule->dg_info[i].id;
		if (grm_fact_tbl_get(db->derived_fact_table, id)) {
			// we've already derived these facts
			continue;
		}

		grm_rule_entry_t *row;
		GRM_RULE_ROW_FOREACH(row, db->rule_table, id) {
			if (grm_queue_contains(q, row)) {
				continue;
			}
			row->derived_fact_count = -1;
			if (grm_queue_push(q, row) < 0 ||
					push_dependencies(db, q, row->rule) < 0) {
				return -1;
			}
		}
	}
	return 0;
}

/*
 * Initialize the queue of rules that we need to derive facts for.
 */
static grm_queue_t *rule_queue_new (grm_dl_t *db, grm_query_t *query)
{
	grm_queue_t *ruleq = grm_queue_new(0);
	if (!ruleq) {
		return NULL;
	}

	if (grm_fact_tbl_get(db->derived_fact_table, query->id)) {
		// we've already derived these facts
		return ruleq;
	}

	grm_rule_entry_t *row;
	GRM_RULE_ROW_FOREACH(row, db->rule_table, query->id) {
		row->derived_fact_count = -1;
		if (grm_queue_push(ruleq, row) < 0 ||
				push_dependencies(db, ruleq, row->rule) < 0) {
			grm_queue_free(ruleq);
			return NULL;
		}
	}
	// force us to derive facts for dependencies first
	grm_queue_reverse(ruleq);
	return ruleq;
}

/*
 * Set 'all' to the union of the fact_table and derived_fact_table sets.
 */
static int make_fact_union (grm_set_t *all, grm_dl_t *db, intptr_t id)
{
	if (grm_set_init(all) < 0) {
		return -1;
	}
	grm_fact_set_t *facts = grm_fact_tbl_get(db->fact_table, id);
	if (facts) {
		if (grm_set_union(all, facts) < 0) {
			return -1;
		}
	}
	facts = grm_fact_tbl_get(db->derived_fact_table, id);
	if (facts) {
		if (grm_set_union(all, facts) < 0) {
			return -1;
		}
	}
	return 0;
}

// forward declarations of keyword handlers called by recurse_body_literals()
static int not_handler (grm_dl_t *, grm_rule_t *, size_t, grm_query_t *,
		grm_fact_t **, size_t, size_t *);
static int distinct_handler (grm_dl_t *, grm_rule_t *, size_t, grm_query_t *,
		grm_fact_t **, size_t, size_t *);
static int or_handler (grm_dl_t *, grm_rule_t *, size_t, grm_query_t *,
		grm_fact_t **, size_t, size_t *);

/*
 * Check that a specific set of variables matches every literal in the body.
 * If the fact makes it all the way through, then update the
 * derived_fact_table for this rule.
 */
static int recurse_body_literals (grm_dl_t *db,
		grm_rule_t *rule,
		size_t idx,
		grm_query_t *literal,
		grm_fact_t **prev_vars,
		size_t nvars,
		size_t *count)
{
	if (idx == rule->body_length) {
		// this fact successfully passed through the body
		grm_fact_t *new = grm_fact_derive(prev_vars, nvars, rule->head);
		if (!new) {
			return -1;
		}
		int rc = grm_fact_tbl_add(db->derived_fact_table, new, NULL);
		if (rc) {
			grm_fact_free(new);
			return rc;
		}
		*count += 1;
		return 0;
	}

	if (!literal) {
		literal = rule->body[idx];
	}

	intptr_t id = literal->id;
	switch (id) {
	case GRM_NOT_ID:
		return not_handler(db, rule, idx, literal, prev_vars, nvars, count);
	case GRM_DISTINCT_ID:
		return distinct_handler(db, rule, idx, literal, prev_vars, nvars, count);
	case GRM_OR_ID:
		return or_handler(db, rule, idx, literal, prev_vars, nvars, count);
	}

	grm_set_t all_facts;
	if (make_fact_union(&all_facts, db, id) < 0) {
		return -1;
	}

	grm_fact_t *vars[nvars];
	grm_fact_t *fact;
	GRM_FACT_SET_FOREACH(fact, &all_facts) {
		memcpy(vars, prev_vars, sizeof(*vars) * nvars);
		if (grm_query_match(vars, nvars, literal, fact, false)) {
			if (recurse_body_literals(db, rule, idx + 1, NULL, vars, nvars, count) < 0) {
				grm_set_free(&all_facts);
				return -1;
			}
		}
	}

	grm_set_free(&all_facts);
	return 0;
}

/*
 * Derive facts from 'rule'.  New facts are put into the derived_fact_table.
 */
static int derive_new_facts (grm_dl_t *db, grm_rule_t *rule)
{
	size_t nvars = grm_vector_length(rule->var_ids);
	grm_fact_t *vars[nvars];
	memset(vars, 0, sizeof(*vars) * nvars);
	size_t count = 0;
	if (recurse_body_literals(db, rule, 0, NULL, vars, nvars, &count) < 0) {
		return -1;
	}
	return count;
}

/*
 * Cycle through the rules in the queue deriving new facts.
 */
static int rule_table_matches (grm_dl_t *db,
		grm_fact_set_t *matches,
		grm_query_t *query,
		size_t nvars)
{
	grm_queue_t *ruleq = rule_queue_new(db, query);
	if (!ruleq) {
		return -1;
	}

	int count = 0;
	for (grm_rule_entry_t *rule = grm_queue_pop(ruleq); rule; rule = grm_queue_pop(ruleq)) {
		int num_new_facts = derive_new_facts(db, rule->rule);
		if (num_new_facts < 0) {
			grm_queue_free(ruleq);
			return -1;
		}
		count += num_new_facts;
		if (rule->derived_fact_count != count) {
			rule->derived_fact_count = count;
			grm_queue_push(ruleq, rule);
		}
	}
	grm_queue_free(ruleq);

	return fact_table_matches(db->derived_fact_table, matches, query, nvars);
}

/*
 * Convert the variable entries to facts and place them in the response array.
 */
static int convert_set_to_response (grm_fact_t **resp,
		size_t *count,
		grm_fact_set_t *facts)
{
	grm_fact_t *fact;
	GRM_FACT_SET_FOREACH(fact, facts) {
		resp[*count] = grm_fact_dup(fact);
		if (!resp[*count]) {
			return -1;
		}
		*count += 1;
	}
	return 0;
}

int grm_dl_match_query (grm_dl_t *db,
		grm_fact_t ***resp,
		size_t *count,
		grm_query_t *query,
		grm_error_t *error)
{
	grm_fact_set_t answers;
	if (grm_fact_set_init(&answers) < 0) {
		grm_error_log_out_of_mem(error);
		return -1;
	}

	size_t nvars = count_vars(query);
	if (fact_table_matches(db->fact_table, &answers, query, nvars) < 0) {
		goto err_out_of_mem;
	}
	if (rule_table_matches(db, &answers, query, nvars) < 0) {
		goto err_out_of_mem;
	}

	size_t num = grm_set_size(&answers);
	if (resp) {
		assert(count);
		*count = 0;
		if (num == 0) {
			*resp = NULL;
		} else {
			*resp = calloc(num, sizeof(**resp));
			if (!*resp) {
				goto err_out_of_mem;
			}
			if (convert_set_to_response(*resp, count, &answers) < 0) {
				free(*resp);
				goto err_out_of_mem;
			}
		}
	}

	grm_fact_set_free(&answers);
	return !!num;

err_out_of_mem:
	*resp = NULL;
	grm_set_free(&answers);
	grm_error_log_out_of_mem(error);
	return -1;
}

void grm_dl_free_facts (grm_fact_t **facts, size_t count)
{
	if (!facts) {
		return;
	}

	for (size_t i = 0; i < count; i++) {
		grm_fact_free(facts[i]);
	}
	free(facts);
}

/******************************************************************************
 * DATALOG KEYWORD HANDLERS
 *****************************************************************************/

/*
 * Implement the 'not' keyword.
 */
static int not_handler (grm_dl_t *db,
		grm_rule_t *rule,
		size_t idx,
		grm_query_t *literal,
		grm_fact_t **prev_vars,
		size_t nvars,
		size_t *count)
{
	for (size_t i = 0; i < nvars; i++) {
		// the 'not' keyword should never be called with variables
		assert(!grm_query_is_variable(prev_vars[i]));
	}

	literal += literal->child_off;

	grm_set_t all_facts;
	if (make_fact_union(&all_facts, db, literal->id) < 0) {
		return -1;
	}

	grm_fact_t *fact;
	GRM_FACT_SET_FOREACH(fact, &all_facts) {
		if (grm_query_match(prev_vars, nvars, literal, fact, false)) {
			grm_set_free(&all_facts);
			return 0;
		}
	}
	grm_set_free(&all_facts);
	return recurse_body_literals(db, rule, idx + 1, NULL, prev_vars, nvars, count);
}

/*
 * Implement the 'distinct' keyword.
 */
static int distinct_handler (grm_dl_t *db,
		grm_rule_t *rule,
		size_t idx,
		grm_query_t *literal,
		grm_fact_t **vars,
		size_t nvars,
		size_t *count)
{
	for (size_t i = 0; i < nvars; i++) {
		// the 'distinct' keyword should never be called with variables
		assert(!grm_query_is_variable(vars[i]));
	}

	grm_query_t *first, *second, *fact, *query;
	first = literal + literal->child_off;
	second = literal + literal->child_off + 1;
	if (grm_query_is_variable(first) && grm_query_is_variable(second)) {
		// (distinct ?x ?y)
		query = vars[-first->id];
		fact = vars[-second->id];
	} else if (grm_query_is_variable(first) && !grm_query_is_variable(second)) {
		// (distinct ?x atom)
		query = vars[-first->id];
		fact = second;
	} else if (!grm_query_is_variable(first) && grm_query_is_variable(second)) {
		// (distinct atom ?x)
		fact = first;
		query = vars[-second->id];
	} else {
		// (distinct atom1 atom2)
		fact = first;
		query = second;
	}

	if (grm_query_match(vars, nvars, query, fact, false)) {
		return 0;
	}
	return recurse_body_literals(db, rule, idx + 1, NULL, vars, nvars, count);
}

/*
 * Implement the 'or' keyword.
 */
static int or_handler (grm_dl_t *db,
		grm_rule_t *rule,
		size_t idx,
		grm_query_t *literal,
		grm_fact_t **vars,
		size_t nvars,
		size_t *count)
{
	grm_query_t *first = literal + literal->child_off;
	grm_query_t *second = literal + literal->child_off + 1;
	if (recurse_body_literals(db, rule, idx, first, vars, nvars, count) < 0 ||
			recurse_body_literals(db, rule, idx, second, vars, nvars, count) < 0) {
		return -1;
	}
	return 0;
}
