#include "keywd.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Return a heap allocated string of format "%s/%d" % (symbol, arity).
 * It is the caller's responsibility to free() the new string.
 * Returns NULL if insufficient memory.
 */
#define ARITY_BUFFER (sizeof("/999"))
char *grm_keywd_symbol_arity (const char *symbol, size_t arity)
{
	assert(arity < 1000);
	size_t buflen = strlen(symbol) + ARITY_BUFFER;
	char *newsym = calloc(buflen, sizeof(*newsym));
	if (!newsym) {
		return NULL;
	}
	snprintf(newsym, buflen, "%s/%lu", symbol, arity);
	return newsym;
}
