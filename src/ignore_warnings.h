#pragma once

/*
 * These macros allow us to ignore -Wunused-parameter or
 * -Wunused-but-set-variable errors when the argument is only being used in
 *  assert()s.  The value of _str is just to tell the human reader of the
 *  program what specific parameter we are ignoring.  It is used in the
 *  following way:
 *
 * START_IGNORE_UNUSED_PARAMETER("c")
 * void do_something (int a, int b, int c)
 * {
 * 	...
 * 	int x = foo(a, b);
 * 	assert(x < c); // <-- the only place 'c' is referenced
 * 	...
 * }
 * END_IGNORE_UNUSED_PARAMETER("c")
 */

#ifdef NDEBUG
#	define POP_DIAGNOSTIC_WARNINGS _Pragma("GCC diagnostic pop")
#	define START_IGNORE_UNUSED_PARAMETER(_str)				\
		_Pragma("GCC diagnostic push")					\
		_Pragma("GCC diagnostic ignored \"-Wunused-parameter\"")
#	define END_IGNORE_UNUSED_PARAMETER(_str) POP_DIAGNOSTIC_WARNINGS

	// clang (version 3.8) does not check for -Wunused-but-set-variable
#	ifndef __clang__
#		define START_IGNORE_UNUSED_VARIABLE(_str)				\
			_Pragma("GCC diagnostic push")					\
			_Pragma("GCC diagnostic ignored \"-Wunused-but-set-variable\"")
#		define END_IGNORE_UNUSED_VARIABLE(_str)	POP_DIAGNOSTIC_WARNINGS
#	else
#		define START_IGNORE_UNUSED_VARIABLE(_str)
#		define END_IGNORE_UNUSED_VARIABLE(_str)
#	endif	/* __clang__ */
#else
#	define START_IGNORE_UNUSED_PARAMETER(_str)
#	define END_IGNORE_UNUSED_PARAMETER(_str)

#	define START_IGNORE_UNUSED_VARIABLE(_str)
#	define END_IGNORE_UNUSED_VARIABLE(_str)
#endif	/* NDEBUG */
