#include "parser.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

int grm_parser_init (grm_parser_t *parser, const char *buffer)
{
	parser->buffer = strdup(buffer);
	if (!parser->buffer) {
		return -1;
	}
	return grm_lexer_init(&parser->lexer, buffer);
}

int grm_parser_free (grm_parser_t *parser)
{
	free((void *)parser->buffer);
	return grm_lexer_free(&parser->lexer);
}

/*
 * Make sure the arities for '<=', 'or', 'not', and 'distinct' are correct.
 */
static bool correct_arity (const grm_token_t *token, size_t arity, grm_error_t *error)
{
	if (token->type == GRM_RULE_DEF && arity < 2) {
		grm_error_log(error, GRM_ILLEGAL_RULE_DEF_ERR, token);
		return false;
	}
	if ((token->type == GRM_OR_KW && arity != 2) ||
			(token->type == GRM_NOT_KW && arity != 1) ||
			(token->type == GRM_DISTINCT_KW && arity != 2)) {
		grm_error_log(error, GRM_KEYWORD_ARITY_ERR, token);
		return false;
	}
	return true;
}

grm_ast_t *grm_parser_parse (grm_parser_t *parser, grm_error_t *error)
{
	grm_ast_t *root, *ptr;
	root = ptr = grm_ast_new_root();
	if (!root) {
		grm_error_log_out_of_mem(error);
		goto report_error;
	}

	grm_token_t token;
	token.type = GRM_EMPTY_TOKEN;
	grm_lexer_t *lex = &parser->lexer;
	while (grm_lexer_next_token(lex, &token) == 0) {
		grm_ast_t *parent;
		grm_token_type_t curr = grm_ast_get_token(ptr)->type;
		int empty = (curr == GRM_EMPTY_TOKEN);

		switch (token.type) {
		case GRM_OPEN_PAREN:
			if (ptr != root && empty) {
				// disallow a predicate to start a predicate:
				// ((a b) c) ; <--- NOT VALID
				grm_error_log(error, GRM_ILLEGAL_HEAD_ERR, &token);
				goto report_error;
			}

			ptr = grm_ast_add_child(ptr);
			if (!ptr) {
				grm_error_log_out_of_mem(error);
				goto report_error;
			}
			break;

		case GRM_CLOSE_PAREN:
			parent = grm_ast_get_parent(ptr);
			if (!parent) {
				// too many closed parens
				grm_error_log(error, GRM_EXTRA_CLOSING_ERR, &token);
				goto report_error;
			}

			if (empty) {
				// empty '()' is illegal
				grm_error_log(error, GRM_EMPTY_PREDICATE_ERR, &token);
				goto report_error;
			}

			if (!correct_arity(grm_ast_get_token(ptr),
						grm_ast_num_children(ptr),
						error)) {
				goto report_error;
			}

			ptr = parent;
			break;

		case GRM_RULE_DEF:
			if (grm_ast_get_parent(ptr) != root) {
				// '<=' has to occur after '(' at the top level
				grm_error_log(error, GRM_RULE_SYM_MISUSE_ERR, &token);
				goto report_error;
			}
			grm_ast_set_token(ptr, &token);
			break;

		case GRM_VAR_LIT:
			if (empty) {
				// variable cannot be first literal after '('
				grm_error_log(error, GRM_ILLEGAL_VAR_LIT_ERR, &token);
				goto report_error;
			}
			// fallthrough
		case GRM_ATOM_LIT:
			if (ptr == root) {
				// atom cannot be defined outside of a predicate
				grm_error_log(error, GRM_LONE_ATOM_ERR, &token);
				goto report_error;
			}

			parent = ptr;
			if (!empty) {
				ptr = grm_ast_add_child(parent);
				if (!ptr) {
					grm_error_log_out_of_mem(error);
					goto report_error;
				}
			}
			grm_ast_set_token(ptr, &token);
			ptr = parent;
			break;

		case GRM_OR_KW:
		case GRM_NOT_KW:
		case GRM_DISTINCT_KW:
			if (ptr == root || !empty) {
				// keywords must be the first atom in a
				// literal in the body of a rule
				grm_error_log(error, GRM_KEYWORD_MISUSE_ERR, &token);
				goto report_error;
			}

			parent = grm_ast_get_parent(ptr);
			grm_token_type_t ptype = grm_ast_get_token(parent)->type;
			if (ptype == GRM_RULE_DEF && grm_ast_num_children(parent) < 2) {
				// keywords cannot be in the head of a rule
				grm_error_log(error, GRM_KEYWORD_MISUSE_ERR, &token);
				goto report_error;
			}

			if (ptype != GRM_RULE_DEF && ptype != GRM_OR_KW) {
				// keywords must be the start of a literal in
				// the body of a rule or nested in an 'or'
				grm_error_log(error, GRM_KEYWORD_MISUSE_ERR, &token);
				goto report_error;
			}

			grm_ast_set_token(ptr, &token);
			break;

		default:
			// XXX should never reach here
			assert(0);
		}
	}
	assert(token.type == GRM_END_PROGRAM);
	if (ptr != root) {
		// not enough closed parens
		grm_error_log(error, GRM_EXTRA_OPENING_ERR, &token);
		goto report_error;
	}
	return root;

report_error:
	if (root) {
		grm_ast_free_root(root);
	}
	return NULL;
}
