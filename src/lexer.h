#pragma once

#include <stddef.h>

typedef enum {
	GRM_EMPTY_TOKEN,
	GRM_OPEN_PAREN,
	GRM_CLOSE_PAREN,
	GRM_ATOM_LIT,
	GRM_VAR_LIT,
	GRM_RULE_DEF,
	GRM_OR_KW,
	GRM_NOT_KW,
	GRM_DISTINCT_KW,
	GRM_END_PROGRAM,
} grm_token_type_t;

typedef struct {
	grm_token_type_t type;
	char *lexeme;
	size_t index;
} grm_token_t;

int grm_token_copy(grm_token_t *dst, const grm_token_t *src);

typedef struct {
	char *buffer;
	size_t index;
	size_t size;
	grm_token_type_t next_token;
} grm_lexer_t;

int grm_lexer_init(grm_lexer_t *, const char *);
int grm_lexer_next_token(grm_lexer_t *, grm_token_t *);
int grm_lexer_free(grm_lexer_t *);
