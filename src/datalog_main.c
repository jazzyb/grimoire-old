#include "datalog.h"
#include "error.h"
#include "keywd.h"
#include "vector.h"

#include <readline/history.h>
#include <readline/readline.h>

#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_INPUT_SIZE 1024	// arbitrary size for char buffers

// These all have to be global variables because they are updated by various
// readline callback functions:
grm_dl_t global_db;
enum { STATE_FACT, QUERY_DB, CONTINUE } mode, prev_mode;
char input[MAX_INPUT_SIZE];

static __attribute__((noreturn)) void print_usage_and_exit (char *bin, int exit_flag)
{
	fprintf(stderr, "usage: %s [-f <datalog-file>]\n", bin);
	exit(exit_flag);
}

static void parse_options (char **datalog_filename, int argc, char **argv)
{
	int opt;
	while ((opt = getopt(argc, argv, "f:h?")) != -1) {
		switch (opt) {
		case 'f':
			*datalog_filename = optarg;
			break;
		case 'h':
		case '?':
			print_usage_and_exit(argv[0], EXIT_SUCCESS);
		default:
			print_usage_and_exit(argv[0], EXIT_FAILURE);
		}
	}
}

enum {
	COUNT_ERR = 501,
};

static void print_error (FILE *out, char *buffer, grm_error_t *error)
{
	if (error->type == COUNT_ERR) {
		fprintf(out, "\ncount/1 is not an allowed fact or rule\n\n");
		return;
	}

	size_t line, col;
	fprintf(out, "\n");
	grm_error_line_column(&line, &col, buffer, error);
	if (line && col) {
		// print the line
		char *row = buffer;
		while (--line) {
			row = strstr(row, "\n");
			assert(row);
			row += 1;
		}
		char *end = strstr(row, "\n");
		if (end) {
			*end = '\0';
		}
		fprintf(out, "%s\n", row);

		// indicate the column
		while (--col) {
			fprintf(out, " ");
		}
		fprintf(out, "^\n");
	}

	// print the error
	char msg[MAX_INPUT_SIZE];
	grm_error_message(msg, sizeof(msg), buffer, error);
	fprintf(out, "%s\n\n", msg);
}

static int read_datalog_file (grm_dl_t *db, char *filename)
{
	// get the size of the file
	struct stat st;
	stat(filename, &st);
	char *buf = calloc(st.st_size, sizeof(*buf));
	if (!buf) {
		fprintf(stderr, "Cannot allocate %ld bytes to read '%s'\n",
				st.st_size, filename);
		return -1;
	}

	// read the file
	FILE *fp = fopen(filename, "r");
	if (!fp) {
		fprintf(stderr, "Cannot open '%s' for reading\n", filename);
		goto free_buffer;
	}
	if ((size_t)st.st_size != fread(buf, 1, st.st_size, fp)) {
		fprintf(stderr, "Error reading file '%s'\n", filename);
		goto close_file;
	}
	fclose(fp);

	// parse the datalog
	grm_error_t error;
	if (grm_dl_parse(db, buf, &error) < 0) {
		print_error(stderr, buf, &error);
		goto free_buffer;
	}
	free(buf);
	return 0;

close_file:
	fclose(fp);
free_buffer:
	free(buf);
	return -1;
}

static char *atom_generator (const char *text, int state)
{
	static size_t length, index;
	if (state == 0) {
		index = 0;
		length = strlen(text);
	}

	while (index < grm_vector_length(global_db.symbols)) {
		const char *sym = grm_dl_get_symbol(&global_db, index++);
		if (sym) {
			if (prev_mode == STATE_FACT && strcasecmp(sym, "count") == 0) {
				continue;
			}
			if (strncasecmp(sym, text, length) == 0) {
				return strdup(sym);
			}
		}
	}

	return NULL;
}

static char **atom_completion (const char *text,
		int __attribute__((unused)) start,
		int __attribute__((unused)) end)
{
	rl_attempted_completion_over = 1;
	return rl_completion_matches(text, atom_generator);
}

static int change_mode (int count, int key)
{
	if (mode == CONTINUE || rl_point != 0) {
		rl_insert(count, key);
	} else {
		assert(key == '+' || key == '?');
		mode = prev_mode = (key == '+') ? STATE_FACT : QUERY_DB;
		rl_pending_input = '\n';
	}
	return 0;
}

static int count_open_parens (const char *buffer)
{
	int paren_count = 0;
	for (size_t i = 0; paren_count >= 0 && i < strlen(buffer); i++) {
		if (buffer[i] == '(') {
			paren_count++;
		} else if (buffer[i] == ')') {
			paren_count--;
		}
	}
	return paren_count;
}

static int close_parens (int __attribute__((unused)) count,
		int __attribute__((unused)) key)
{
	int paren_count = count_open_parens(input) + count_open_parens(rl_line_buffer);
	if (paren_count > 0) {
		char closing_parens[MAX_INPUT_SIZE / 2] = {0};
		while (paren_count--) {
			closing_parens[paren_count] = ')';
		}
		rl_insert_text(closing_parens);
	}
	return 0;
}

static void sigint_handler (int __attribute__((unused)) status)
{
	printf("\n");
	*input = '\0';
	mode = prev_mode;
	rl_set_prompt(mode == STATE_FACT ? "+-: " : "?-: ");
	rl_on_new_line();
	rl_replace_line("", 0);
	rl_redisplay();
}

intptr_t COUNT_ID;

static grm_keywd_t cli_keywords[1] = {
	{ .id = 0, .sym = "count", .arity = 1 },
};

static bool validate (intptr_t id, grm_error_t *error)
{
	if (id == COUNT_ID) {
		grm_error_log(error, COUNT_ERR, NULL);
		return false;
	}
	return true;
}

static bool validate_fact (const grm_fact_t *fact,
		const grm_fact_tbl_t *tbl __attribute__((unused)),
		grm_error_t *error)
{
	return validate(fact->id, error);
}

static bool validate_rule (const grm_rule_t *rule,
		const grm_rule_tbl_t *tbl __attribute__((unused)),
		grm_error_t *error)
{
	return validate(rule->head->id, error);
}

static void initialize (grm_dl_t *db, char *datalog_filename)
{
	// initialize the database
	grm_dl_init(db);
	grm_dl_set_keywords(db, &COUNT_ID, cli_keywords, 1);
	grm_fact_tbl_add_validator(db->fact_table, COUNT_ID, &validate_fact);
	grm_rule_tbl_add_validator(db->rule_table, COUNT_ID, &validate_rule);
	// turn on readline history
	using_history();
	// turn on tab completion for atoms
	rl_attempted_completion_function = atom_completion;
	// key bindings
	rl_bind_key('+', change_mode);	// switch to STATE_FACT mode
	rl_bind_key('?', change_mode);	// switch to QUERY_DB mode
	rl_bind_keyseq("\\C-]", close_parens);	// close all pending parentheses
	// re-prompt on Ctrl-C
	signal(SIGINT, sigint_handler);
	// set initial input mode
	mode = prev_mode = datalog_filename ? QUERY_DB : STATE_FACT;
}

static void read_eval_print_loop (grm_dl_t *db)
{
	grm_error_t error;
	char *prompt = NULL, *line = NULL;
	bool counting = false;

	while (true) {
		if (line) {
			free(line);
			line = NULL;
		}

		if (mode != CONTINUE) {
			*input = '\0';
		}

		// figure out our prompt
		switch (mode) {
		case STATE_FACT:
			prompt = "+-: "; break;
		case QUERY_DB:
			prompt = "?-: "; break;
		case CONTINUE:
			prompt = "... "; break;
		default:
			// XXX should never reach here
			assert(0);
		}

		// read input
		line = readline(prompt);
		if (!line) {
			// quit on Ctrl-D
			break;
		} else if (*line == '\0') {
			continue;
		}
		add_history(line);
		strcat(input, line);
		strcat(input, "\n");

		// figure out if the input is complete
		if (count_open_parens(input) > 0) {
			mode = CONTINUE;
			continue;
		}

		mode = prev_mode;
		if (mode == STATE_FACT) {
			// update the database
			if (grm_dl_parse(db, input, &error) < 0) {
				print_error(stdout, input, &error);
			}
			continue;
		}

		// query database for facts
		grm_query_t *query = grm_dl_query_parse(db, input, &error);
		if (!query) {
			print_error(stdout, input, &error);
			continue;
		}
		counting = (query->id == COUNT_ID);

		grm_fact_t **facts;
		size_t count;
		if (grm_dl_match_query(db, &facts, &count,
					counting ? query + 1 : query,
					&error) < 0) {
			grm_query_free(query);
			print_error(stdout, input, &error);
			continue;
		}

		if (counting) {
			// just output the number of matches and free the facts
			printf("%lu\n", count);
			for (size_t i = 0; i < count; i++) {
				grm_fact_free(facts[i]);
			}
		} else {
			// output the matches
			printf("%s\n", count ? "True" : "False");
			for (size_t i = 0; i < count; i++) {
				char fact[MAX_INPUT_SIZE];
				grm_dl_fact_to_string(db, fact, sizeof(fact), facts[i]);
				printf("%s\n", fact);
				grm_fact_free(facts[i]);
			}
		}
		free(facts);
		grm_query_free(query);
	}
	printf("\n");
}

int main (int argc, char *argv[])
{
	char *datalog_filename = NULL;
	parse_options(&datalog_filename, argc, argv);
	initialize(&global_db, datalog_filename);

	if (datalog_filename) {
		if (read_datalog_file(&global_db, datalog_filename) < 0) {
			grm_dl_free(&global_db);
			return EXIT_FAILURE;
		}
	}

	read_eval_print_loop(&global_db);

	grm_dl_free(&global_db);
	return EXIT_SUCCESS;
}
