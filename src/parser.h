#pragma once

#include "ast.h"
#include "error.h"
#include "lexer.h"

typedef struct {
	grm_lexer_t lexer;
	const char *buffer;
} grm_parser_t;

int grm_parser_init(grm_parser_t *, const char *);
grm_ast_t *grm_parser_parse(grm_parser_t *, grm_error_t *);
int grm_parser_free(grm_parser_t *);
