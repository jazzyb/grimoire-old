#pragma once

#include <stddef.h>

typedef struct grm_vector_ grm_vector_t;

/*
 * Return a pointer to a new vector.  Returns NULL on error.
 * 'init_capacity' is the initial number of elements to store
 * 'item_size' is the size of each element
 */
grm_vector_t *grm_vector_alloc(size_t init_capacity, size_t item_size);

/*
 * Return a pointer to a vector of type 'type'.  Returns NULL on error.
 */
#define grm_vector_new(type) grm_vector_alloc(0, sizeof(type))

/*
 * Append a value onto the vector.  The vector's capacity will increase if
 * there is not enough space.  The data at 'ptr' will be memcpy()'ed onto the
 * end of the vector.
 * Returns -1 on error; 0 on success.
 */
int grm_vector_append(grm_vector_t *vector, void *ptr);

/*
 * Copies the data at 'ptr' into the particular index in the vector.  If the
 * vector does not have the capacity for the insertion, the capacity is
 * increased to add the item.
 * Returns -1 on error; 0 on success.
 */
int grm_vector_set(grm_vector_t *vector, size_t index, void *ptr);

/*
 * Returns a pointer to the data at 'index' in 'vector'.  Returns NULL if
 * index is greater than the capacity.
 */
void *grm_vector_get(grm_vector_t *vector, size_t index);

/*
 * Returns the number of items in the vector.
 */
size_t grm_vector_length(grm_vector_t *vector);

/*
 * Deallocate the vector.
 */
void grm_vector_free(grm_vector_t *vector);

/*
 * Erase all the entries in the vector without freeing it.
 */
void grm_vector_erase(grm_vector_t *vector);
