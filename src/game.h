#pragma once

#include "datalog.h"
#include "error.h"
#include "fact.h"

#include <stdbool.h>
#include <stdint.h>

typedef struct {
	grm_set_t *truths;	// all 'true' facts
	grm_set_t *deeds;	// all 'does' facts
} grm_state_t;

typedef struct grm_game_ grm_game_t;

/*
 * Initialize the game state.
 */
int grm_game_state_init(grm_state_t *state);

/*
 * Return a pointer to the game database.
 */
grm_dl_t *grm_game_database(grm_game_t *game);

/*
 * Initialize the game and start state based on rules.
 *
 * Returns NULL on error.
 */
grm_game_t *grm_game_start(grm_state_t *state, const char *rules, grm_error_t *error);

/*
 * Set 'next' to the next state based on 'game', 'current', and 'moves'.
 * 'moves' is an array of "legal" grm_fact_t structs -- one for each role in
 * the game.
 */
int grm_game_next_state(grm_state_t *next,
		grm_game_t *game,
		grm_state_t *current,
		grm_fact_t **moves);

/*
 * Free the game.
 */
void grm_game_free(grm_game_t *game);

/*
 * Free the game state.
 */
void grm_game_state_free(grm_state_t *state);

/*
 * GDL-I SPECIFIC ERRORS
 */
enum {
	GRM_DOES_DEFINED_ERR = 100,
	GRM_ILLEGAL_DOES_DEP_ERR,
	GRM_ILLEGAL_INIT_BODY_ERR,
	GRM_ILLEGAL_INIT_DEP_ERR,
	GRM_ILLEGAL_NEXT_BODY_ERR,
	GRM_ILLEGAL_ROLE_RULE_ERR,
	GRM_TRUE_DEFINED_ERR,
	GRM_UNDEFINED_RELATION_ERR,
};

/*
 * Print GDL-I or datalog error.
 */
int grm_game_error_message(char *dst, size_t limit, const char *buffer, grm_error_t *error);

/*
 * Return an array of the game roles and set 'count' to the number of roles in
 * the game.
 */
const intptr_t *grm_game_roles(grm_game_t *game, size_t *count);

/*
 * Return an array of all base/1 facts setting 'count' to the number of facts.
 */
const grm_fact_t **grm_game_propositions(grm_game_t *game, size_t *count);

/*
 * Return an array of all input/2 facts for the given role setting 'count' to
 * the number of facts.
 */
const grm_fact_t **grm_game_actions(grm_game_t *game, size_t *count, intptr_t role);

/*
 * Return an array of all init/1 facts setting 'count' to the number of facts.
 */
const grm_fact_t **grm_game_inits(grm_game_t *game, size_t *count);

/*
 * Return an array of all legal/2 facts for the given role in the current
 * state setting 'count' to the number of facts.
 */
grm_fact_t **grm_game_legal_moves(grm_game_t *game,
		grm_state_t *state,
		size_t *count,
		intptr_t role);

/*
 * Return the goal for the given role in the current state.  For a well-formed
 * game, this should be a number between 0 and 100 inclusive.
 */
int grm_game_reward(grm_game_t *game, grm_state_t *state, intptr_t role);

/*
 * Return whether or not the current state is terminal.
 */
bool grm_game_is_terminal(grm_game_t *game, grm_state_t *state);
