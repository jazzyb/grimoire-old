#include "lexer.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

int grm_token_copy (grm_token_t *dst, const grm_token_t *src)
{
	return dst != memcpy(dst, src, sizeof(*dst));
}

static void make_case_insensitive (char *buf, size_t size)
{
	char to_lower = 'a' - 'A';

	for (size_t i = 0; i < size; i++) {
		if (buf[i] >= 'A' && buf[i] <= 'Z') {
			buf[i] = buf[i] + to_lower;
		}
	}
}

int grm_lexer_init (grm_lexer_t *lexer, const char *buffer)
{
	lexer->index = 0;
	lexer->buffer = strdup(buffer);
	if (!lexer->buffer) {
		return -1;
	}
	lexer->size = strlen(lexer->buffer);
	make_case_insensitive(lexer->buffer, lexer->size);
	lexer->next_token = GRM_EMPTY_TOKEN;
	return 0;
}

int grm_lexer_free (grm_lexer_t *lexer)
{
	free(lexer->buffer);
	return 0;
}

static bool is_space (char c)
{
	return  c == ' '  ||
		c == '\f' ||
		c == '\n' ||
		c == '\r' ||
		c == '\t' ||
		c == '\v';
}

static bool is_not_literal_char (char c)
{
	return  c == '\0' ||
		c == '(' ||
		c == ')' ||
		c == ';' ||
		is_space(c);
}

static bool is_rule_def (const char *buf)
{
	return buf[0] == '<' && buf[1] == '=' && is_not_literal_char(buf[2]);
}

static bool is_or_keyword (const char *buf)
{
	return buf[0] == 'o' && buf[1] == 'r' && is_not_literal_char(buf[2]);
}

static bool is_not_keyword (const char *buf)
{
	return buf[0] == 'n' && buf[1] == 'o' && buf[2] == 't' && is_not_literal_char(buf[3]);
}

static bool is_distinct_keyword (const char *buf)
{
	return  buf[0] == 'd' &&
		buf[1] == 'i' &&
		buf[2] == 's' &&
		buf[3] == 't' &&
		buf[4] == 'i' &&
		buf[5] == 'n' &&
		buf[6] == 'c' &&
		buf[7] == 't' &&
		is_not_literal_char(buf[8]);
}

static void handle_comment (grm_lexer_t *lexer, size_t *i)
{
	for (; *i < lexer->size; (*i)++) {
		char c = lexer->buffer[*i];
		lexer->buffer[*i] = '\0';
		if (c == '\n') {
			return;
		}
	}
}

int grm_lexer_next_token (grm_lexer_t *lexer, grm_token_t *token)
{
	size_t i = lexer->index;

	token->lexeme = NULL;
	if (lexer->next_token != GRM_EMPTY_TOKEN) {
		token->type = lexer->next_token;
		token->index = lexer->index - 1;
		lexer->next_token = GRM_EMPTY_TOKEN;
		return (token->type == GRM_END_PROGRAM) ? 1 : 0;
	}
	token->type = GRM_EMPTY_TOKEN;

	for (; i < lexer->size; i++) {
		char c = lexer->buffer[i];

		if (is_space(c)) {
			lexer->buffer[i] = '\0';
			if (token->type != GRM_EMPTY_TOKEN) {
				goto found_token;
			}

		} else if (c == ';') {
			handle_comment(lexer, &i);
			if (token->type != GRM_EMPTY_TOKEN) {
				goto found_token;
			}

		} else if (c == '(' || c == ')') {
			lexer->buffer[i] = '\0';
			if (token->type != GRM_EMPTY_TOKEN) {
				lexer->next_token = (c == '(') ? GRM_OPEN_PAREN : GRM_CLOSE_PAREN;
				goto found_token;
			} else {
				token->type = (c == '(') ? GRM_OPEN_PAREN : GRM_CLOSE_PAREN;
				token->index = i;
				goto found_token;
			}

		} else if (token->type == GRM_EMPTY_TOKEN) {
			token->index = i;
			token->lexeme = lexer->buffer + i;
			if (c == '?') {
				token->type = GRM_VAR_LIT;
			} else if (is_rule_def(lexer->buffer + i)) {
				token->type = GRM_RULE_DEF;
			} else if (is_or_keyword(lexer->buffer + i)) {
				token->type = GRM_OR_KW;
			} else if (is_not_keyword(lexer->buffer + i)) {
				token->type = GRM_NOT_KW;
			} else if (is_distinct_keyword(lexer->buffer + i)) {
				token->type = GRM_DISTINCT_KW;
			} else {
				token->type = GRM_ATOM_LIT;
			}
		}
	}

	if (token->type != GRM_EMPTY_TOKEN) {
		// NOTE: This is probably an error condition since it means
		// that there is a top level literal outside of a predicate.
		lexer->next_token = GRM_END_PROGRAM;
		goto found_token;
	}

	token->type = GRM_END_PROGRAM;
	token->index = lexer->size;
	return 1;

found_token:
	lexer->index = i + 1;
	return 0;
}
