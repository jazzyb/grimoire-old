#include "error.h"

#include <stdio.h>
#include <string.h>

void grm_error_set (grm_error_t *error,
		grm_error_type_t type,
		const grm_token_t *token,
		void *user_data)
{
	error->type = type;
	error->user_data = user_data;
	if (token) {
		grm_token_copy(&error->token, token);
		if (error->type == GRM_KEYWORD_MISUSE_ERR ||
				error->type == GRM_KEYWORD_ARITY_ERR) {
			// set the lexeme to a constant string in case the
			// original buffer gets freed out from under us
			if (strcmp(token->lexeme, "not") == 0) {
				error->token.lexeme = "not";
			} else if (strcmp(token->lexeme, "distinct") == 0) {
				error->token.lexeme = "distinct";
			} else if (strcmp(token->lexeme, "or") == 0) {
				error->token.lexeme = "or";
			}
		}
	}
}

void grm_error_line_column (size_t *linep, size_t *colp,
		const char *buffer,
		grm_error_t *error)
{
	if (error->type == GRM_MEMORY_ERR || error->type == GRM_INVALID_QUERY_ERR) {
		*linep = *colp = 0;
		return;
	}

	size_t line = 1, col = 1;
	for (size_t i = 0; i < error->token.index; i++) {
		if (buffer[i] == '\n') {
			line += 1;
			col = 0;
		}
		col += 1;
	}
	*linep = line;
	*colp = col;
}

/*
 * MESSAGES FOR DATALOG PARSE ERRORS
 */
#define BODY_SAFETY_MSG (GRM_ERROR_LINE_COL_FORMAT "Variables in negative literals must also" \
		" be present in a positive literal in the same body")
#define EMPTY_PREDICATE_MSG (GRM_ERROR_LINE_COL_FORMAT "Empty '()' is not a valid predicate")
#define EXTRA_CLOSING_MSG (GRM_ERROR_LINE_COL_FORMAT "Closing parenthesis does not have matching" \
		" opening parenthesis")
#define EXTRA_OPENING_MSG (GRM_ERROR_LINE_COL_FORMAT "Missing closing parenthesis")
#define HEAD_SAFETY_MSG (GRM_ERROR_LINE_COL_FORMAT "Variables in the head of a rule must also" \
		" appear in a positive, non-arithmetic literal in the body")
#define ILLEGAL_FACT_MSG (GRM_ERROR_LINE_COL_FORMAT "Facts must only contain atoms")
#define ILLEGAL_HEAD_MSG (GRM_ERROR_LINE_COL_FORMAT "Predicate must start with an atom")
#define ILLEGAL_RULE_DEF_MSG (GRM_ERROR_LINE_COL_FORMAT "Rule must have one head and at least one" \
		" body predicate or literal")
#define ILLEGAL_VAR_LIT_MSG (GRM_ERROR_LINE_COL_FORMAT "Variable cannot be the first literal" \
		" in a predicate")
#define INVALID_QUERY_MSG ("Not a valid query")
#define KEYWORD_ARITY_MSG (GRM_ERROR_LINE_COL_FORMAT "Keyword '%s' has the wrong arity")
#define KEYWORD_MISUSE_MSG (GRM_ERROR_LINE_COL_FORMAT "Keyword '%s' can only begin a literal in" \
		" the body of a rule")
#define LONE_ATOM_MSG (GRM_ERROR_LINE_COL_FORMAT "Atom cannot be defined outside of a predicate")
#define MEMORY_MSG ("Out of memory")
#define RULE_SYM_MISUSE_MSG (GRM_ERROR_LINE_COL_FORMAT "Keyword '<=' is reserved for defining" \
		" rules at the top level")
// FIXME:  Good grief.  How confusing is the following:
#define UNRESTRICTED_RECURSION_MSG (GRM_ERROR_LINE_COL_FORMAT "New rule introduces a cycle" \
		" that breaks the following recursion restriction:  A variable" \
		" appears in a body literal that is in a cycle with its rule but" \
		" is absent from any literal NOT in a cycle with the rule")
#define UNSTRATIFIED_RULE_MSG (GRM_ERROR_LINE_COL_FORMAT "Rule introduces a negative cycle")

int grm_error_message (char *dst,
		size_t limit,
		const char *buffer,
		grm_error_t *error)
{
	const char *fmt = NULL;
	size_t line = 0, col = 0;
	grm_error_line_column(&line, &col, buffer, error);

	switch (error->type) {
	case GRM_BODY_SAFETY_ERR:
		fmt = BODY_SAFETY_MSG; break;
	case GRM_EMPTY_PREDICATE_ERR:
		fmt = EMPTY_PREDICATE_MSG; break;
	case GRM_EXTRA_CLOSING_ERR:
		fmt = EXTRA_CLOSING_MSG; break;
	case GRM_EXTRA_OPENING_ERR:
		fmt = EXTRA_OPENING_MSG; break;
	case GRM_HEAD_SAFETY_ERR:
		fmt = HEAD_SAFETY_MSG; break;
	case GRM_ILLEGAL_FACT_ERR:
		fmt = ILLEGAL_FACT_MSG; break;
	case GRM_ILLEGAL_HEAD_ERR:
		fmt = ILLEGAL_HEAD_MSG; break;
	case GRM_ILLEGAL_RULE_DEF_ERR:
		fmt = ILLEGAL_RULE_DEF_MSG; break;
	case GRM_ILLEGAL_VAR_LIT_ERR:
		fmt = ILLEGAL_VAR_LIT_MSG; break;
	case GRM_INVALID_QUERY_ERR:
		fmt = INVALID_QUERY_MSG; break;
	case GRM_KEYWORD_ARITY_ERR:
		return snprintf(dst, limit, KEYWORD_ARITY_MSG,
				line, col, error->token.lexeme);
	case GRM_KEYWORD_MISUSE_ERR:
		return snprintf(dst, limit, KEYWORD_MISUSE_MSG,
				line, col, error->token.lexeme);
	case GRM_LONE_ATOM_ERR:
		fmt = LONE_ATOM_MSG; break;
	case GRM_MEMORY_ERR:
		fmt = MEMORY_MSG; break;
	case GRM_RULE_SYM_MISUSE_ERR:
		fmt = RULE_SYM_MISUSE_MSG; break;
	case GRM_UNRESTRICTED_RECURSION_ERR:
		fmt = UNRESTRICTED_RECURSION_MSG; break;
	case GRM_UNSTRATIFIED_RULE_ERR:
		fmt = UNSTRATIFIED_RULE_MSG; break;
	default:
		return snprintf(dst, limit, "Unknown error: (%d)", error->type);
	}
	return snprintf(dst, limit, fmt, line, col);
}
