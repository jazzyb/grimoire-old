#pragma once

#include <stdbool.h>
#include <stddef.h>

typedef struct grm_queue_ grm_queue_t;

grm_queue_t *grm_queue_new(size_t capacity);
void grm_queue_free(grm_queue_t *q);
int grm_queue_push(grm_queue_t *q, const void *ptr);
void *grm_queue_pop(grm_queue_t *q);
bool grm_queue_empty(grm_queue_t *q);
bool grm_queue_contains(grm_queue_t *q, const void *ptr);
void grm_queue_reverse(grm_queue_t *q);
