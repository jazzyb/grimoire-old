#include "rule.h"

#include "ignore_warnings.h"
#include "keywd.h"
#include "queue.h"

#include <alloca.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

/*
 * NOTE  Ensure all GDL rules have the following restrictions:
 *
 * 1. (SAFETY) If a variable appears in the head of a rule, it must also
 *    appear in some positive, non-arithmetic literal in the body.
 *    -- head_variables_safe()
 *
 * 2. (SAFETY) If a variable appears in a negative literal in the body of a
 *    rule, it must also appear in some positive literal in the body.
 *    -- reorder_negative_literals()
 *
 * 3. (STRATIFICATION) There must be no cycles in the dependency graph for a
 *    rule set that contains a negation.  E.g., the following is illegal:
 *        (<= (p ?x) (q ?x))
 *        (<= (q ?x) (r ?x) (NOT (p ?x)))
 *    -- rule_is_stratified()
 *
 * 4. (RECURSION RESTRICTION) Given the following rule:
 *        (<= (p t_1 ... t_n)
 *            (b_1 ...)
 *            ...
 *            (q v_1 ... v_k)
 *            ...
 *            (b_m ...))
 *    WHERE q APPEARS IN A CYCLE WITH p.  Either:
 *      1. v_j is not a variable
 *      2. v_j is one of the variables in the head (p t_1 ... t_n)
 *      3. v_j is a variable in b_i where b_i is not in a cycle with p
 *    -- restricted_recursion()
 */

typedef struct {
	size_t front;
	size_t back;
	grm_query_t *queue[];
} iter_t;

static void iter_init (iter_t *iter, grm_query_t *query)
{
	iter->queue[0] = query;
	iter->queue[query->total_len] = NULL;
	iter->front = 0;
	iter->back = 1;
}

static grm_query_t *next_variable (iter_t *iter)
{
	grm_query_t *next = iter->queue[iter->front++];
	if (next) {
		for (size_t i = 0; i < next->arity; i++) {
			iter->queue[iter->back++] = next + next->child_off + i;
		}
	}
	return next;
}

#define FOREACH_QUERY(entry, query)								\
	iter_t *_iter = alloca(sizeof(*_iter) + sizeof(query) * ((query)->total_len + 1));	\
	for (iter_init(_iter, (grm_query_t *)(query)), entry = next_variable(_iter);		\
			entry;									\
			entry = next_variable(_iter))

#define FOREACH_VARIABLE(var, query)	\
	FOREACH_QUERY(var, query) if (grm_query_is_variable(var))

static bool is_negative (grm_query_t *q)
{
	if (q->id == GRM_OR_ID) {
		grm_query_t *child = q + q->child_off;
		return is_negative(child) || is_negative(child + 1);
	}
	return q->id == GRM_NOT_ID || q->id == GRM_DISTINCT_ID;
}

static bool variable_in_query (intptr_t var, const grm_query_t *query)
{
	const grm_query_t *entry;
	FOREACH_VARIABLE(entry, query) {
		if (var == entry->id) {
			return true;
		}
	}
	return false;
}

static bool found_variable_in_positive_literal (grm_query_t *var, grm_query_t **lit, size_t length)
{
	for (size_t i = 0; i < length; i++) {
		grm_query_t *literal = lit[i];
		if (is_negative(literal)) {
			continue;
		}
		if (variable_in_query(var->id, literal)) {
			return true;
		}
	}
	return false;
}

#define MAKE_VAR_TOKEN(_index)	\
	{ .type = GRM_VAR_LIT, .index = _index, .lexeme = NULL }

/*
 * Reorder the literals in the body such that negative literals ('not' and
 * 'distinct') follow positive literals with the same variables.  If a
 * negative literal contains a variable that does not occur in a positive
 * literal in the body, log a GRM_BODY_SAFETY_ERR and return -1.
 */
static int reorder_negative_literals (grm_rule_t *rule, grm_error_t *error)
{
	// separate the negative and positive literals
	grm_query_t *pos[rule->body_length];
	grm_query_t *neg[rule->body_length];
	size_t pcount = 0, ncount = 0;
	for (size_t i = 0; i < rule->body_length; i++) {
		grm_query_t *q = rule->body[i];
		if (is_negative(q)) {
			neg[ncount++] = q;
		} else {
			pos[pcount++] = q;
		}
	}

	// verify that every variable in a negative literal is also present in
	// a positive literal
	for (size_t i = 0; i < ncount; i++) {
		grm_query_t *var;
		FOREACH_VARIABLE(var, neg[i]) {
			if (!found_variable_in_positive_literal(var, pos, pcount)) {
				grm_token_t token = MAKE_VAR_TOKEN(var->token_index);
				grm_error_log(error, GRM_BODY_SAFETY_ERR, &token);
				return -1;
			}
		}
	}

	// FIXME Ideally, we'd want to have negative literals follow the
	// *exact* set of positive literals that contain the same variables to
	// reduce the number of facts we generate.  But it's just easier to
	// put all the negative literals at the end.
	memcpy(rule->body, pos, pcount * sizeof(*rule->body));
	memcpy(rule->body + pcount, neg, ncount * sizeof(*rule->body));
	return 0;
}

/*
 * Make sure all variables in the head also appear in a positive literal in
 * the body.
 */
static bool head_variables_safe (grm_rule_t *rule, grm_error_t *error)
{
	grm_query_t *var;
	FOREACH_VARIABLE(var, rule->head) {
		if (!found_variable_in_positive_literal(var, rule->body, rule->body_length)) {
			grm_token_t token = MAKE_VAR_TOKEN(var->token_index);
			grm_error_log(error, GRM_HEAD_SAFETY_ERR, &token);
			return false;
		}
	}
	return true;
}

/*
 * Count the number of times 'id' occurs in the list of queries.
 */
static size_t count_ids (grm_query_t **queries, size_t length, intptr_t id)
{
	size_t count = 0;
	for (size_t i = 0; i < length; i++) {
		grm_query_t *q = queries[i];
		for (size_t j = 0; j < q->total_len; j++) {
			if (q[j].id == id) {
				count += 1;
			}
		}
	}
	return count;
}

/*
 * Count the individual literals in the body of the rule.
 */
static size_t num_individual_literals (grm_rule_t *rule)
{
	return  rule->body_length
		+ count_ids(rule->body, rule->body_length, GRM_OR_ID)
		- count_ids(rule->body, rule->body_length, GRM_DISTINCT_ID);
}

/*
 * Initialize our dependency graph info.  This structure will be used to
 * verify stratification and recursion restrictions.
 */
static int init_dg_entry (grm_dg_info_t *info,
		size_t *depcnt,
		grm_query_t *head,
		grm_query_t *literal)
{
	if (literal->id == GRM_DISTINCT_ID) {
		// skip distinct literals
		return 0;

	} else if (literal->id == GRM_OR_ID) {
		// process both 'or' children
		grm_query_t *child = literal + literal->child_off;
		if (init_dg_entry(info, depcnt, head, child) < 0) {
			return -1;
		}
		if (init_dg_entry(info, depcnt, head, child + 1) < 0) {
			return -1;
		}
		return 0;
	}

	size_t count = 0;
	intptr_t tmpbuf[literal->total_len];
	grm_query_t *entry;
	FOREACH_VARIABLE(entry, literal) {
		if (!variable_in_query(entry->id, head)) {
			tmpbuf[count++] = entry->id;
		}
	}

	info += (*depcnt)++;
	info->negative = (literal->id == GRM_NOT_ID);
	info->id = info->negative ? literal[literal->child_off].id : literal->id;
	if (count == 0) {
		return 0;
	}

	info->vars = calloc(count, sizeof(*info->vars));
	if (!info->vars) {
		return -1;
	}
	memcpy(info->vars, tmpbuf, count * sizeof(*info->vars));
	info->nvars = count;
	return 0;
}

/*
 * Make the dependency graph info.  See reasoning in init_dg_entry() above.
 */
static int make_dependency_graph_info (grm_rule_t *rule)
{
	rule->dep_length = num_individual_literals(rule);
	rule->dg_info = calloc(rule->dep_length, sizeof(*rule->dg_info));
	if (!rule->dg_info) {
		return -1;
	}

	size_t depcnt = 0;
	for (size_t i = 0; i < rule->body_length; i++) {
		if (init_dg_entry(rule->dg_info, &depcnt, rule->head, rule->body[i]) < 0) {
			goto err_free_dg_info;
		}
		assert(depcnt <= rule->dep_length);
	}
	return 0;

err_free_dg_info:
	for (size_t i = 0; i < rule->dep_length; i++) {
		if (rule->dg_info[i].vars) {
			free(rule->dg_info[i].vars);
		}
	}
	free(rule->dg_info);
	return -1;
}

#define SET_QUERY_CTX(rule)			\
	{ .rel_id = &(rule)->next_var_id,	\
	  .var_id_map = (rule)->var_ids }

grm_rule_t *grm_rule_new (grm_fact_ctx_t *fctx, const grm_ast_t *root, grm_error_t *error)
{
	assert(grm_ast_get_token(root)->type == GRM_RULE_DEF);

	grm_rule_t *new = calloc(1, sizeof(*new));
	if (!new) {
		grm_error_log_out_of_mem(error);
		return NULL;
	}

	new->body_length = grm_ast_num_children(root) - 1;
	new->body = calloc(new->body_length, sizeof(*new->body));
	if (!new->body) {
		grm_error_log_out_of_mem(error);
		goto err_free_rule;
	}

	new->var_ids = grm_vector_new(intptr_t);
	if (!new->var_ids) {
		grm_error_log_out_of_mem(error);
		goto err_free_body_preds;
	}

	grm_query_ctx_t qctx = SET_QUERY_CTX(new);
	new->head = grm_query_new(fctx, &qctx, grm_ast_get_child(root, 0), error);
	if (!new->head) {
		goto err_free_var_ids;
	}

	for (size_t i = 0; i < new->body_length; i++) {
		grm_ast_t *tree = grm_ast_get_child(root, i + 1);
		new->body[i] = grm_query_new(fctx, &qctx, tree, error);
		if (!new->body[i]) {
			goto err_free_queries;
		}
	}

	if (reorder_negative_literals(new, error) < 0) {
		goto err_free_queries;
	}
	if (!head_variables_safe(new, error)) {
		goto err_free_queries;
	}
	if (make_dependency_graph_info(new) < 0) {
		grm_error_log_out_of_mem(error);
		goto err_free_queries;
	}
	return new;

err_free_queries:
	for (size_t i = 0; i < new->body_length; i++) {
		if (new->body[i]) {
			grm_query_free(new->body[i]);
		}
	}
	grm_query_free(new->head);
err_free_var_ids:
	grm_vector_free(new->var_ids);
err_free_body_preds:
	free(new->body);
err_free_rule:
	free(new);
	return NULL;
}

void grm_rule_free (grm_rule_t *rule)
{
	for (size_t i = 0; i < rule->dep_length; i++) {
		if (rule->dg_info[i].vars) {
			free(rule->dg_info[i].vars);
		}
	}
	for (size_t i = 0; i < rule->body_length; i++) {
		grm_query_free(rule->body[i]);
	}
	free(rule->dg_info);
	grm_query_free(rule->head);
	grm_vector_free(rule->var_ids);
	free(rule->body);
	free(rule);
}

grm_rule_tbl_t *grm_rule_tbl_new (void)
{
	grm_rule_tbl_t *new = calloc(1, sizeof(*new));
	if (!new) {
		return NULL;
	}
	new->rules = grm_vector_new(grm_rule_entry_t*);
	if (!new->rules) {
		free(new);
		return NULL;
	}
	new->validators = grm_vector_new(grm_rule_validator_t);
	if (!new->validators) {
		grm_vector_free(new->rules);
		free(new);
		return NULL;
	}
	return new;
}

void grm_rule_tbl_free (grm_rule_tbl_t *rule_table)
{
	for (size_t i = 0; i < grm_vector_length(rule_table->rules); i++) {
		grm_rule_entry_t *row = grm_rule_tbl_get(rule_table, i);
		while (row) {
			grm_rule_entry_t *tmp = row;
			row = row->next;
			// XXX Do we want to free the rules themselves?
			grm_rule_free(tmp->rule);
			free(tmp);
		}
	}
	grm_vector_free(rule_table->rules);
	grm_vector_free(rule_table->validators);
	free(rule_table);
}

int grm_rule_tbl_add_validator (grm_rule_tbl_t *tbl, intptr_t id, grm_rule_validator_t validate)
{
	return grm_vector_set(tbl->validators, id, &validate);
}

typedef struct stack_ {
	struct stack_ *prev;
	intptr_t id;
} stack_t;

/*
 * Return whether or not we've seen this ID in the stack before.
 */
static bool id_in_stack (intptr_t id, stack_t *stack)
{
	for (stack_t *node = stack; node; node = node->prev) {
		if (node->id == id) {
			return true;
		}
	}
	return false;
}

/*
 * If the new rule introduced a new cycle, then update the dependency graph.
 */
START_IGNORE_UNUSED_VARIABLE("rc")
static void set_cycle (grm_vector_t *cycles,
		grm_dg_info_t *dgi,
		grm_queue_t *updates,
		grm_rule_t *rule)
{
	// XXX We do not check the return value (and bubble the error up
	// through the call stack) for the vector or queue operations in order
	// to simplify the code.  We just *assume* we have enough memory.
	if (!dgi->cycle) {
		dgi->cycle = true;
		int rc = grm_vector_append(cycles, &dgi);
		assert(rc == 0);
		if (!grm_queue_contains(updates, rule)) {
			rc = grm_queue_push(updates, rule);
			assert(rc == 0);
		}
	}
}
END_IGNORE_UNUSED_VARIABLE("rc")

/*
 * Undo any new body literals that were marked as cycles by set_cycle().
 */
static void reset_cycles (grm_vector_t *cycles)
{
	size_t length = grm_vector_length(cycles);
	for (size_t i = 0; i < length; i++) {
		grm_dg_info_t *dgi = *(grm_dg_info_t **)grm_vector_get(cycles, i);
		dgi->cycle = false;
	}
}

enum {
	NOT_CYCLE,
	POS_CYCLE,
	NEG_CYCLE
};

/*
 * Identify if the next literal in the body creates a recursive cycle or not.
 * If it is a cycle, determine whether it involves negation or not.
 */
static int type_of_cycle (intptr_t start,
		intptr_t id,
		grm_vector_t *tbl,
		grm_vector_t *cycles,
		grm_queue_t *updates,
		stack_t *prev)
{
	int cycle = NOT_CYCLE;
	stack_t stack = { .prev = prev, .id = id };
	grm_rule_entry_t **rowp = grm_vector_get(tbl, id);
	if (!rowp) {
		return false;
	}
	for (grm_rule_entry_t *row = *rowp; row; row = row->next) {
		grm_rule_t *rule = row->rule;
		for (size_t i = 0; i < rule->dep_length; i++) {
			grm_dg_info_t *dgi = rule->dg_info + i;
			if (dgi->id == start) {
				set_cycle(cycles, dgi, updates, rule);
				return dgi->negative ? NEG_CYCLE : POS_CYCLE;
			}
			if (id_in_stack(dgi->id, &stack)) {
				// we've found a recursive cycle, but it
				// doesn't involve the new rule so ignore it
				continue;
			}

			int result = type_of_cycle(start, dgi->id, tbl, cycles, updates, &stack);
			if (result == NEG_CYCLE) {
				return NEG_CYCLE;
			} else if (result == POS_CYCLE) {
				if (dgi->negative) {
					return NEG_CYCLE;
				}
				set_cycle(cycles, dgi, updates, rule);
				cycle = POS_CYCLE;
			}
		}
	}
	return cycle;
}

#define MAKE_RULE_TOKEN(_index)	\
	{ .type = GRM_RULE_DEF, .index = _index, .lexeme = NULL }

/*
 * If a rule introduces a negative recursive cycle, then the rule is not
 * "stratified".  All rules must be stratified.
 */
static bool rule_is_stratified (grm_rule_t *rule,
		grm_vector_t *tbl,
		grm_vector_t *cycles,
		grm_queue_t *updates,
		grm_error_t *error)
{
	intptr_t start = rule->head->id;
	for (size_t i = 0; i < rule->dep_length; i++) {
		grm_dg_info_t *dgi = rule->dg_info + i;
		if (dgi->id == start && dgi->negative) {
			goto unstratified_err;
		}
		if (dgi->id == start) {
			dgi->cycle = true;
			continue;
		}

		int cycle = type_of_cycle(start, dgi->id, tbl, cycles, updates, NULL);
		if (NEG_CYCLE == cycle || (dgi->negative && POS_CYCLE == cycle)) {
			goto unstratified_err;
		} else if (POS_CYCLE == cycle) {
			dgi->cycle = true;
		}
	}
	return true;

unstratified_err: ;
	grm_token_t token = MAKE_RULE_TOKEN(rule->head->token_index);
	grm_error_log(error, GRM_UNSTRATIFIED_RULE_ERR, &token);
	return false;
}

/*
 * Find the variable in the non-cycle-literal.
 */
static bool var_in_noncycle (intptr_t var, grm_dg_info_t **noncycles, size_t count)
{
	for (size_t i = 0; i < count; i++) {
		grm_dg_info_t *dgi = noncycles[i];
		for (size_t j = 0; j < dgi->nvars; j++) {
			if (var == dgi->vars[j]) {
				return true;
			}
		}
	}
	return false;
}

/*
 * Verify that every variable in a cycle-literal is present in some
 * non-cycle-literal in the rule body.
 */
static bool rule_restricts_recursion (grm_dg_info_t **cycles,
		size_t ccount,
		grm_dg_info_t **noncycles,
		size_t nccount)
{
	for (size_t i = 0; i < ccount; i++) {
		grm_dg_info_t *dgi = cycles[i];
		for (size_t j = 0; j < dgi->nvars; j++) {
			if (!var_in_noncycle(dgi->vars[j], noncycles, nccount)) {
				return false;
			}
		}
	}
	return true;
}

/*
 * For every rule whose cycles got updated by the new rule, ensure that they
 * still follow the recursion restriction.
 */
static bool restricted_recursion (grm_queue_t *updated_rules)
{
	grm_rule_t *rule = grm_queue_pop(updated_rules);
	while (rule) {
		// separate the cycles and non-cycles in the dependency graph
		grm_dg_info_t *cycles[rule->dep_length];
		size_t ccount = 0;
		grm_dg_info_t *noncycles[rule->dep_length];
		size_t nccount = 0;

		for (size_t i = 0; i < rule->dep_length; i++) {
			if (rule->dg_info[i].cycle) {
				cycles[ccount++] = rule->dg_info + i;
			} else {
				noncycles[nccount++] = rule->dg_info + i;
			}
		}

		if (!rule_restricts_recursion(cycles, ccount, noncycles, nccount)) {
			return false;
		}

		rule = grm_queue_pop(updated_rules);
	}
	return true;
}

/*
 * Return true if the rule is stratified and it fits the recursion restriction
 * from above.  Return false if either of the verifications fail.
 */
static bool passes_verification (grm_rule_t *rule,
		grm_rule_tbl_t *rule_table,
		grm_error_t *error)
{
	bool success = false;
	grm_vector_t *cycles = grm_vector_alloc(100, sizeof(rule->dg_info));
	grm_queue_t *updates = grm_queue_new(100);
	if (!cycles || !updates) {
		grm_error_log_out_of_mem(error);
		goto exit_and_cleanup;
	}
	grm_queue_push(updates, rule);

	if (!rule_is_stratified(rule, rule_table->rules, cycles, updates, error)) {
		goto exit_and_cleanup;
	}
	// XXX: call to restricted_recursion() must follow rule_is_stratified()
	// because the latter calculates recursive cycles that are needed by
	// the former
	if (!restricted_recursion(updates)) {
		grm_token_t token = MAKE_RULE_TOKEN(rule->head->token_index);
		grm_error_log(error, GRM_UNRESTRICTED_RECURSION_ERR, &token);
		goto exit_and_cleanup;
	}

	// check any user-defined validators
	grm_rule_validator_t *validate = grm_vector_get(rule_table->validators, rule->head->id);
	if (validate && *validate && !((*validate)(rule, rule_table, error))) {
		goto exit_and_cleanup;
	}

	success = true;

exit_and_cleanup:
	if (cycles) {
		if (!success) {
			// rule_is_stratified() updates cycle information in
			// the dependency graph; undo those changes if we fail
			reset_cycles(cycles);
		}
		grm_vector_free(cycles);
	}
	if (updates) {
		grm_queue_free(updates);
	}
	return success;
}

int grm_rule_tbl_add (grm_rule_tbl_t *rule_table, grm_rule_t *rule, grm_error_t *error)
{
	if (!passes_verification(rule, rule_table, error)) {
		return -1;
	}

	grm_rule_entry_t *new = calloc(1, sizeof(*new));
	if (!new) {
		grm_error_log_out_of_mem(error);
		return -1;
	}
	new->next = NULL;
	new->rule = rule;

	intptr_t idx = rule->head->id;
	grm_rule_entry_t *row = grm_rule_tbl_get(rule_table, idx);
	if (!row) {
		if (grm_vector_set(rule_table->rules, idx, &new) < 0) {
			free(new);
			grm_error_log_out_of_mem(error);
			return -1;
		}
		return 0;
	}

	while (row->next) {
		row = row->next;
	}
	row->next = new;
	return 0;
}

grm_rule_entry_t *grm_rule_tbl_get (grm_rule_tbl_t *tbl, intptr_t id)
{
	grm_rule_entry_t **rowp = grm_vector_get(tbl->rules, id);
	return rowp ? *rowp : NULL;
}

bool grm_rule_tbl_exists (grm_rule_tbl_t *tbl, intptr_t id, size_t *index)
{
	grm_rule_entry_t *entry = grm_rule_tbl_get(tbl, id);
	if (!entry) {
		return false;
	}
	if (index) {
		*index = entry->rule->head[0].token_index;
	}
	return true;
}

static size_t get_body_index (grm_rule_t *rule, intptr_t id)
{
	for (size_t i = 0; i < rule->body_length; i++) {
		grm_query_t *body = rule->body[i];
		for (size_t j = 0; j < body->total_len; j++) {
			if (body[j].id == id) {
				return body[j].token_index;
			}
		}
	}

	// XXX We should only call this function if we already know that the
	// id is in the rule body.  Should never fall out of the above loop.
	assert(0);
	return -1;
}

bool grm_rule_tbl_is_cycle (grm_rule_tbl_t *tbl, intptr_t r1, intptr_t r2)
{
	return  grm_rule_tbl_depends_on(tbl, r1, r2, NULL) &&
		grm_rule_tbl_depends_on(tbl, r2, r1, NULL);
}

static bool recursive_depends_on (grm_rule_tbl_t *tbl,
		intptr_t id,
		intptr_t needle,
		size_t *index,
		stack_t *prev)
{
	stack_t stack = { .prev = prev, .id = id };
	grm_rule_t *rule;
	GRM_RULE_FOREACH(rule, tbl, id) {
		for (size_t i = 0; i < rule->dep_length; i++) {
			intptr_t next_id = rule->dg_info[i].id;
			if (next_id == needle) {
				if (index) {
					*index = get_body_index(rule, next_id);
				}
				return true;
			}
			if (id_in_stack(next_id, &stack)) {
				// don't follow recursive cycles
				continue;
			}
			if (recursive_depends_on(tbl, next_id, needle, index, &stack)) {
				return true;
			}
		}
	}
	return false;
}

bool grm_rule_tbl_depends_on (grm_rule_tbl_t *tbl,
		intptr_t principal_id,
		intptr_t dependent_id,
		size_t *index)
{
	return recursive_depends_on(tbl, principal_id, dependent_id, index, NULL);
}

bool grm_rule_tbl_is_in_body (grm_rule_tbl_t *tbl, intptr_t id, size_t *index)
{
	for (size_t i = 0; i < grm_vector_length(tbl->rules); i++) {
		grm_rule_entry_t *row = grm_rule_tbl_get(tbl, i);
		while (row) {
			grm_rule_t *rule = row->rule;
			for (size_t j = 0; j < rule->dep_length; j++) {
				if (rule->dg_info[j].id == id) {
					if (index) {
						*index = get_body_index(rule, id);
					}
					return true;
				}
			}
			row = row->next;
		}
	}
	return false;
}
