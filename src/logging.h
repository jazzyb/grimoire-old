#pragma once

#include <syslog.h>

#ifdef LOGGING
#	define grm_log(level, fmt, ...) syslog(level, "%s +%d: " fmt, __FILE__, __LINE__, __VA_ARGS__)
#else
#	define grm_log(level, fmt, ...)
#endif
