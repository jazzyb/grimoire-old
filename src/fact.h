#pragma once

#include "ast.h"
#include "error.h"
#include "htable.h"
#include "vector.h"

#include <stdbool.h>
#include <stdint.h>

/******************************************************************************
 * FACTS
 *****************************************************************************/

typedef struct {
	grm_vector_t *symbols;
	grm_dict_t *sym_id_map;
	intptr_t *next_id;
} grm_fact_ctx_t;

typedef struct {
	intptr_t id;
	size_t arity;
	size_t total_len;
	size_t child_off;
	size_t token_index;
} grm_fact_t;

grm_fact_t *grm_fact_new(grm_fact_ctx_t *ctx, const grm_ast_t *root, grm_error_t *err);
void grm_fact_free(grm_fact_t *);
grm_ast_t *grm_fact_to_ast(grm_fact_ctx_t *ctx, const grm_fact_t *fact);

/******************************************************************************
 * QUERIES
 *****************************************************************************/

typedef grm_fact_t grm_query_t;

typedef struct {
	intptr_t *rel_id;
	grm_vector_t *var_id_map;
} grm_query_ctx_t;

grm_query_t *grm_query_new(grm_fact_ctx_t *fctx,
		grm_query_ctx_t *qctx,
		const grm_ast_t *root,
		grm_error_t *err);
void grm_query_free(grm_query_t *);
bool grm_query_match(grm_fact_t **answer,
		size_t size,
		grm_query_t *q,
		grm_fact_t *f,
		bool initialize_answers);
grm_ast_t *grm_query_to_ast(grm_fact_ctx_t *fctx, grm_query_ctx_t *qctx, const grm_fact_t *fact);
grm_fact_t *grm_fact_derive(grm_fact_t **answer, size_t size, const grm_query_t *q);
grm_fact_t *grm_fact_dup(grm_fact_t *fact);

static inline bool grm_query_is_variable (const grm_query_t *q) { return q->id <= 0; }

/******************************************************************************
 * FACT_SETS
 *****************************************************************************/

typedef grm_set_t grm_fact_set_t;

#define grm_fact_set_init grm_set_init
int grm_fact_set_free(grm_fact_set_t *set);

int grm_fact_set_add(grm_fact_set_t *set, grm_fact_t *fact);
int grm_fact_set_erase(grm_fact_set_t *set);

#define GRM_FACT_SET_FOREACH(fact, set)				\
	grm_fact_t **_fp;					\
	GRM_HTABLE_FOREACH(NULL, _fp, set) if ((fact = *_fp))

/******************************************************************************
 * FACT_TABLE
 *****************************************************************************/

typedef struct {
	grm_vector_t *facts;
	grm_vector_t *validators;
} grm_fact_tbl_t;

grm_fact_tbl_t *grm_fact_tbl_new(void);
void grm_fact_tbl_free(grm_fact_tbl_t *tbl);

typedef bool (*grm_fact_validator_t) (const grm_fact_t *, const grm_fact_tbl_t *, grm_error_t *);

int grm_fact_tbl_add_validator(grm_fact_tbl_t *tbl, intptr_t fact_id, grm_fact_validator_t validate);

/*
 * Free all the facts in the given fact table.
 */
void grm_fact_tbl_erase_all(grm_fact_tbl_t *tbl);
int grm_fact_tbl_add(grm_fact_tbl_t *tbl, grm_fact_t *fact, grm_error_t *error);
int grm_fact_tbl_set(grm_fact_tbl_t *tbl, intptr_t fact_id, grm_fact_set_t *set);
int grm_fact_tbl_unset(grm_fact_tbl_t *tbl, intptr_t fact_id);
grm_fact_set_t *grm_fact_tbl_get(grm_fact_tbl_t *tbl, intptr_t fact_id);
bool grm_fact_tbl_exists(grm_fact_tbl_t *tbl, intptr_t fact_id, size_t *index);

#define GRM_FACT_TBL_FOREACH(set, tbl) 				\
	for (size_t _i = (set = grm_fact_tbl_get(tbl, 0), 0);	\
			_i < grm_vector_length(tbl->facts);	\
			set = grm_fact_tbl_get(tbl, ++_i))	\
		if (set)
