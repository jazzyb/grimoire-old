#pragma once

#include "lexer.h"

#include <stdbool.h>
#include <stddef.h>

#define GRM_ERROR_LINE_COL_FORMAT "line:%lu:col:%lu: "

typedef enum {
	GRM_BODY_SAFETY_ERR = 1,
	GRM_EMPTY_PREDICATE_ERR,
	GRM_EXTRA_CLOSING_ERR,
	GRM_EXTRA_OPENING_ERR,
	GRM_HEAD_SAFETY_ERR,
	GRM_ILLEGAL_FACT_ERR,
	GRM_ILLEGAL_HEAD_ERR,
	GRM_ILLEGAL_RULE_DEF_ERR,
	GRM_ILLEGAL_VAR_LIT_ERR,
	GRM_INVALID_QUERY_ERR,
	GRM_KEYWORD_ARITY_ERR,
	GRM_KEYWORD_MISUSE_ERR,
	GRM_LONE_ATOM_ERR,
	GRM_MEMORY_ERR,
	GRM_RULE_SYM_MISUSE_ERR,
	GRM_UNRESTRICTED_RECURSION_ERR,
	GRM_UNSTRATIFIED_RULE_ERR,
	GRM_ERROR_LAST,
} grm_error_type_t;

typedef struct {
	int type;
	grm_token_t token;
	void *user_data;
} grm_error_t;

void grm_error_set(grm_error_t *, grm_error_type_t, const grm_token_t *, void *);
void grm_error_line_column(size_t *, size_t *, const char *, grm_error_t *);
int grm_error_message(char *, size_t, const char *, grm_error_t *);

static inline void grm_error_log (grm_error_t *error,
		grm_error_type_t type,
		const grm_token_t *token)
{
	if (error) {
		grm_error_set(error, type, token, NULL);
	}
}

static inline void grm_error_log_user_defined (grm_error_t *error,
		grm_error_type_t type,
		const grm_token_t *token,
		void *user_data)
{
	if (error) {
		grm_error_set(error, type, token, user_data);
	}
}

static inline bool grm_error_is_user_defined (grm_error_t *error)
{
	return error->type > GRM_ERROR_LAST;
}

static inline void grm_error_log_out_of_mem (grm_error_t *error)
{
	grm_error_log(error, GRM_MEMORY_ERR, NULL);
}
