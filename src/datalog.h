#pragma once

#include "ast.h"
#include "error.h"
#include "htable.h"
#include "keywd.h"
#include "rule.h"
#include "vector.h"

#include <stdint.h>

typedef struct {
	intptr_t next_id;
	grm_vector_t *symbols;
	grm_dict_t sym_id_map;
	grm_fact_tbl_t *fact_table;
	grm_rule_tbl_t *rule_table;
	grm_fact_tbl_t *derived_fact_table;
} grm_dl_t;

int grm_dl_init(grm_dl_t *db);
int grm_dl_free(grm_dl_t *db);

/*
 * Initializes any special keywords for the database (such as GDL-I and II
 * relations like 'role/1' and 'goal/2').  If new_ids is not NULL, for each
 * keywords[i] sets new_ids[i] to the new id for the keyword.
 *
 * Returns 0 on success; -1 if we run out of memory.
 */
int grm_dl_set_keywords(grm_dl_t *db, intptr_t *new_ids, grm_keywd_t *keywords, size_t size);

/*
 * Update the database with a new fact or rule.
 *
 * Returns 0 on success.  Returns -1 on error and updates the grm_error_t
 * structure.
 */
int grm_dl_update(grm_dl_t *db, const grm_ast_t *tree, grm_error_t *error);

/*
 * Update the database with a new fact or rule.
 *
 * Returns 0 on success.  Returns -1 on error and updates the grm_error_t
 * structure.
 */
int grm_dl_parse(grm_dl_t *db, const char *input, grm_error_t *error);

/*
 * Generate a datalog query from the given database and AST.
 *
 * Returns the new query or NULL on error.
 */
grm_query_t *grm_dl_query_new(grm_dl_t *db, const grm_ast_t *tree, grm_error_t *error);

/*
 * Generate a datalog query from the given database and string.
 *
 * Returns the new query or NULL on error.
 */
grm_query_t *grm_dl_query_parse(grm_dl_t *db, const char *string, grm_error_t *error);

/*
 * Convert the given fact to its string representation.
 *
 * Return the number of bytes written on success and -1 on error.
 */
int grm_dl_fact_to_string(grm_dl_t *db, char *dst, size_t size, const grm_fact_t *src);

/*
 * Find the symbol for the given id.
 */
const char *grm_dl_get_symbol(grm_dl_t *db, intptr_t id);

/*
 * Query the database for facts that match the given query.  Answers are
 * placed in the response array if not NULL.  Space is allocated from the heap
 * for the response array, and it is up to the caller to free the memory.
 * 'count' will be updated with the number of items in the response array.
 *
 * Returns 1 if the query was a match, 0 if there was no match, and -1 for an
 * error.
 */
int grm_dl_match_query(grm_dl_t *db,
		grm_fact_t ***response,
		size_t *count,
		grm_query_t *query,
		grm_error_t *error);

/*
 * Free an array of facts, e.g. the response array returned from
 * grm_dl_match_query()
 */
void grm_dl_free_facts(grm_fact_t **facts, size_t count);
