#pragma once

#include "lexer.h"

#include <stddef.h>

typedef struct grm_ast_ grm_ast_t;

/*
 * Return the root of a new AST.  Returns NULL on error.
 */
grm_ast_t *grm_ast_new_root(void);

/*
 * Add a child node to 'parent'.
 * Return a pointer to the newly created child node, or NULL on error.
 */
grm_ast_t *grm_ast_add_child(grm_ast_t *parent);

/*
 * Copy the contents of 'token' into 'node'.
 */
int grm_ast_set_token(grm_ast_t *node, const grm_token_t *token);

/*
 * Return a pointer to the token in 'node'.
 */
const grm_token_t *grm_ast_get_token(const grm_ast_t *node);

/*
 * Return a pointer to the parent of 'child'.  Returns NULL if 'child' has no
 * parent.
 */
grm_ast_t *grm_ast_get_parent(const grm_ast_t *child);

/*
 * Return the number of children for 'parent'.
 */
size_t grm_ast_num_children(const grm_ast_t *parent);

/*
 * Return the number of all nodes (children, grand-children, etc.) in the
 * tree, including the root.
 */
size_t grm_ast_num_nodes(const grm_ast_t *tree);

/*
 * Return a pointer to the 'n'th child of 'parent'.  Return NULL if no such
 * child exists.
 */
grm_ast_t *grm_ast_get_child(const grm_ast_t *parent, size_t n);

/*
 * Write the string representation of 'tree' to 'dst', limiting the output to
 * 'size' bytes.
 */
int grm_ast_to_string(char *dst, size_t size, const grm_ast_t *tree);

/*
 * Deallocate the tree.
 */
int grm_ast_free_root(grm_ast_t *tree);

/*
 * Iterate through all the children of 'parent'.
 */
#define GRM_AST_FOREACH_CHILD(child, parent)				\
	for (size_t _i = (child = grm_ast_get_child(parent, 0), 0);	\
			child;						\
			child = grm_ast_get_child(parent, ++_i))
