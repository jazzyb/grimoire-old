#pragma once

#include "ast.h"
#include "error.h"
#include "fact.h"
#include "vector.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct {
	intptr_t id;
	bool negative;
	bool cycle;
	intptr_t *vars;
	size_t nvars;
} grm_dg_info_t;

typedef struct {
	grm_query_t *head;
	grm_query_t **body;
	size_t body_length;

	// query context variables:
	intptr_t next_var_id;
	grm_vector_t *var_ids;

	// dependency graph metadata:
	grm_dg_info_t *dg_info;
	size_t dep_length;
} grm_rule_t;

grm_rule_t *grm_rule_new(grm_fact_ctx_t *ctx, const grm_ast_t *root, grm_error_t *err);
void grm_rule_free(grm_rule_t *);

typedef struct {
	grm_vector_t *rules;
	grm_vector_t *validators;
} grm_rule_tbl_t;

typedef struct rule_entry_ {
	struct rule_entry_ *next;
	grm_rule_t *rule;
	int derived_fact_count;
} grm_rule_entry_t;

typedef bool (*grm_rule_validator_t) (const grm_rule_t *, const grm_rule_tbl_t *, grm_error_t *);

grm_rule_tbl_t *grm_rule_tbl_new(void);
void grm_rule_tbl_free(grm_rule_tbl_t *tbl);
int grm_rule_tbl_add_validator(grm_rule_tbl_t *tbl, intptr_t rule_id, grm_rule_validator_t validate);
int grm_rule_tbl_add(grm_rule_tbl_t *tbl, grm_rule_t *rule, grm_error_t *error);
grm_rule_entry_t *grm_rule_tbl_get(grm_rule_tbl_t *tbl, intptr_t id);
bool grm_rule_tbl_exists(grm_rule_tbl_t *tbl, intptr_t id, size_t *index);
bool grm_rule_tbl_is_cycle(grm_rule_tbl_t *tbl, intptr_t r1, intptr_t r2);
bool grm_rule_tbl_depends_on(grm_rule_tbl_t *tbl, intptr_t r1, intptr_t r2, size_t *index);
bool grm_rule_tbl_is_in_body(grm_rule_tbl_t *tbl, intptr_t id, size_t *index);

#define GRM_RULE_ROW_FOREACH(row, table, id)	\
	for (row = grm_rule_tbl_get(table, id); row; row = row->next)

#define GRM_RULE_FOREACH(rulep, table, id)				\
	for (grm_rule_entry_t *row = grm_rule_tbl_get(table, id);	\
			row && (rulep = row->rule);			\
			row = row->next)
