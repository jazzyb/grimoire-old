#include "htable.h"

#include "ignore_warnings.h"
#include "logging.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// TODO Since we can know all of our possible keys in advance, we should
// eventually look into perfect hashing:
// https://en.wikipedia.org/wiki/Perfect_hash_function#Minimal_perfect_hash_function

typedef struct grm_htable_entry_ {
	// NOTE:  'value' must be first in struct for iterators to work
	const void *value;
	const unsigned char *key;
	size_t keylen;
	uint32_t crc32;
	struct grm_htable_entry_ *next;
	struct grm_htable_entry_ *prev;
} grm_htable_entry_t;

// NOTE:  All the following code assumes DEFAULT_TABLE_SIZE is a power of 2.
#define DEFAULT_TABLE_SIZE 32
#define RESIZE_LIMIT(size) ((size) / 4 * 3)

int grm_htable_init (grm_htable_t *htbl)
{
	htbl->buckets = calloc(DEFAULT_TABLE_SIZE, sizeof(*htbl->buckets));
	if (!htbl->buckets) {
		return -1;
	}
	htbl->size = DEFAULT_TABLE_SIZE;
	htbl->resize_limit = RESIZE_LIMIT(htbl->size);
	htbl->num_entries = 0;
	return 0;
}

static void free_bucket_list (grm_htable_entry_t *list)
{
	if (list->next) {
		free_bucket_list(list->next);
	}
	free(list);
}

static void free_buckets (grm_htable_entry_t *buckets, size_t size)
{
	for (size_t i = 0; i < size; i++) {
		if (buckets[i].next) {
			free_bucket_list(buckets[i].next);
		}
	}
	free(buckets);
}

int grm_htable_free (grm_htable_t *htbl)
{
	free_buckets(htbl->buckets, htbl->size);
	return 0;
}

/*
 * The following macros calculate the CRC32C table at compile time.  They are
 * equivalent to the following runtime algorithm:
 *
 * for (uint32_t i = 0; i < 256; i++) {
 *     uint32_t tmp = i;
 *     for (uint32_t j = 0; j < 8; j++) {
 *         if (tmp & 1) {
 *             tmp = (tmp >> 1) ^ 0x1EDC6F41;
 *         } else {
 *             tmp >>= 1;
 *         }
 *     }
 *     crc32_table[i] = tmp;
 * }
 */
#define POLYNOMIAL 0x1EDC6F41
#define ITER(x) (((x) >> 1) ^ (((x) & 1) ? POLYNOMIAL : 0))
#define CRC(x) ITER(ITER(ITER(ITER(ITER(ITER(ITER(ITER((uint32_t)(x)))))))))
#define CRC_4(x)   CRC(x), CRC(x + 1), CRC(x + 2), CRC(x + 3)
#define CRC_16(x)  CRC_4(x), CRC_4(x + 4), CRC_4(x + 8), CRC_4(x + 12)
#define CRC_64(x)  CRC_16(x), CRC_16(x + 16), CRC_16(x + 32), CRC_16(x + 48)
#define CRC_256(x) CRC_64(x), CRC_64(x + 64), CRC_64(x + 128), CRC_64(x + 192)

static const uint32_t crc32_table[256] = { CRC_256(0) };

static uint32_t crc32_hash (const unsigned char *buf, size_t buflen)
{
	uint32_t crc = 0xFFFFFFFF;
	for (size_t i = 0; i < buflen; i++) {
		crc = (crc >> 8) ^ crc32_table[(crc ^ buf[i]) & 0xFF];
	}
	return crc ^ 0xFFFFFFFF;
}

/*
 * Sanity check that the CRC32 checksum is working as intended.
 */
#ifndef NDEBUG
#	define VERIFY_ME									\
	{											\
		static bool _verified = false;							\
		if (!_verified) {								\
			unsigned char passport[] = "My voice is my passport.";			\
			assert(0xF2273CF4 == crc32_hash(passport, strlen((char *)passport)));	\
			_verified = true;							\
		}										\
	}
#else
#	define VERIFY_ME
#endif

/*
 * NOTE:  Index calculation relies on size being a power of two.  If size is
 * changed to be prime, e.g., then use modulo instead.
 */
#define CALCULATE_INDEX(crc, size) ((crc) & ((size) - 1))

/*
 * Set the pointers to the values of the index and CRC from the hash function.
 */
static void calculate_crc_index (size_t *index,
		uint32_t *crc,
		size_t size,
		const unsigned char *key,
		size_t keylen)
{
	VERIFY_ME;
	*crc = crc32_hash(key, keylen);
	*index = CALCULATE_INDEX(*crc, size);
}

static void save_entry (grm_htable_entry_t *entry,
		uint32_t crc,
		const unsigned char *key,
		size_t keylen,
		const void *value)
{
	entry->next = NULL;
	entry->prev = NULL;
	entry->crc32 = crc;
	entry->key = key;
	entry->keylen = keylen;
	entry->value = value;
}

static grm_htable_entry_t *new_htable_entry (uint32_t crc,
		const unsigned char *key,
		size_t keylen,
		const void *value)
{
	grm_htable_entry_t *new = malloc(sizeof(*new));
	if (!new) {
		return NULL;
	}
	save_entry(new, crc, key, keylen, value);
	return new;
}

START_IGNORE_UNUSED_PARAMETER("key, keylen")
static bool key_already_set (grm_htable_entry_t *entry,
		uint32_t crc,
		const unsigned char *key,
		size_t keylen)
{
	if (entry->crc32 == crc) {
		// If this assertion fails, we have a CRC32 hash collision.
		assert(entry->keylen == keylen && memcmp(entry->key, key, keylen) == 0);
		return true;
	}
	return false;
}
END_IGNORE_UNUSED_PARAMETER("key, keylen")

static int list_append (grm_htable_entry_t *list, grm_htable_entry_t *entry)
{
	grm_htable_entry_t *ptr = list;
	while (ptr->next) {
		if (key_already_set(ptr, entry->crc32, entry->key, entry->keylen)) {
			return 1;
		}
		ptr = ptr->next;
	}
	if (key_already_set(ptr, entry->crc32, entry->key, entry->keylen)) {
		return 1;
	}
	ptr->next = entry;
	entry->prev = ptr;
	return 0;
}

static int bucket_set (grm_htable_entry_t *bucket,
		uint32_t crc,
		const unsigned char *key,
		size_t keylen,
		const void *value)
{
	if (bucket->value == NULL) {
		save_entry(bucket, crc, key, keylen, value);
	} else if (key_already_set(bucket, crc, key, keylen)) {
		return 1;
	} else {
		grm_htable_entry_t *entry = new_htable_entry(crc, key, keylen, value);
		if (!entry) {
			return -1;
		}
		if (list_append(bucket, entry) == 1) {
			free(entry);
			return 1;
		}
	}
	return 0;
}

#define HTABLE_ENTRY_FOREACH(_entry, _htbl)						\
	grm_htable_entry_t *_b;								\
	for (_b = (_htbl)->buckets; _b != (_htbl)->buckets + (_htbl)->size; _b++)	\
		for (_entry = _b; _entry; _entry = _entry->next)			\
			if (_entry->value != NULL)

static int resize_htable (grm_htable_t *htbl)
{
	grm_log(LOG_DEBUG, "Table:%p RESIZING...", (void *)htbl);
	size_t new_size = htbl->size << 1;
	grm_htable_entry_t *new = calloc(new_size, sizeof(*new));
	if (!new) {
		return -1;
	}
	grm_log(LOG_DEBUG, "Table:%p Size(new):%lu", (void *)htbl, new_size);

	grm_htable_entry_t *entry;
	HTABLE_ENTRY_FOREACH(entry, htbl) {
		size_t index = CALCULATE_INDEX(entry->crc32, new_size);
		grm_log(LOG_DEBUG, "Table:%p Key:%p Index(new):%lu",
				(void *)htbl, (void *)entry->key, index);
		if (bucket_set(new + index, entry->crc32, entry->key,
					entry->keylen, entry->value) != 0) {
			free_buckets(new, new_size);
			return -1;
		}
	}

	free_buckets(htbl->buckets, htbl->size);
	htbl->buckets = new;
	htbl->size = new_size;
	htbl->resize_limit = RESIZE_LIMIT(new_size);
	grm_log(LOG_DEBUG, "Table:%p ...DONE resizing", (void *)htbl);
	return 0;
}

int grm_htable_set (grm_htable_t *htbl,
		const unsigned char *key,
		size_t keylen,
		const void *value)
{
	if (value == NULL) {
		return -1;
	}

	if (htbl->num_entries >= htbl->resize_limit) {
		if (resize_htable(htbl) != 0) {
			return -1;
		}
	}

	uint32_t crc;
	size_t index;
	calculate_crc_index(&index, &crc, htbl->size, key, keylen);
	int rc = bucket_set(htbl->buckets + index, crc, key, keylen, value);
	if (rc != 0) {
		return rc;
	}

	htbl->num_entries += 1;
	grm_log(LOG_DEBUG, "Table:%p Size:%lu Key:%p Index:%lu",
			(void *)htbl, htbl->size, (void *)key, index);
	return 0;
}

static void *find_entry (grm_htable_entry_t *list, uint32_t crc)
{
	if (list == NULL || list->value == NULL) {
		return NULL;
	} else if (list->crc32 == crc) {
		return list;
	} else {
		return find_entry(list->next, crc);
	}
}

void *grm_htable_get (grm_htable_t *htbl, const unsigned char *key, size_t keylen)
{
	uint32_t crc;
	size_t index;
	calculate_crc_index(&index, &crc, htbl->size, key, keylen);
	grm_htable_entry_t *entry = find_entry(htbl->buckets + index, crc);
	if (!entry) {
		return NULL;
	}
	assert(entry->keylen == keylen && memcmp(entry->key, key, keylen) == 0);
	return (void *)entry->value;
}

static void delete_list_entry (grm_htable_entry_t *node)
{
	if (node->prev == NULL && node->next == NULL) {
		// the only entry in this bucket
		memset(node, 0, sizeof(*node));

	} else if (node->prev == NULL) {
		// the first entry in the list
		grm_htable_entry_t *next = node->next;
		if (next->next) {
			next->next->prev = node;
		}
		next->prev = NULL;
		memcpy(node, next, sizeof(*node));
		free(next);

	} else {
		node->prev->next = node->next;
		if (node->next) {
			node->next->prev = node->prev;
		}
		free(node);
	}
}

int grm_htable_del (grm_htable_t *htbl, const unsigned char *key, size_t keylen)
{
	uint32_t crc;
	size_t index;
	calculate_crc_index(&index, &crc, htbl->size, key, keylen);
	grm_htable_entry_t *entry = find_entry(htbl->buckets + index, crc);
	if (!entry) {
		return 1;
	}
	assert(entry->keylen == keylen && memcmp(entry->key, key, keylen) == 0);
	delete_list_entry(entry);
	return 0;
}

static grm_htable_entry_t *next_valid_bucket (grm_htable_t *htbl, grm_htable_entry_t *bucket)
{
	do {
		bucket += 1;
	} while (bucket != htbl->buckets + htbl->size && bucket->value == NULL);
	return (bucket == htbl->buckets + htbl->size) ? NULL : bucket;
}

static grm_htable_iter_t get_key_and_iterator(grm_htable_entry_t *entry, unsigned char **keyp)
{
	if (!entry) {
		if (keyp) {
			*keyp = NULL;
		}
		return NULL;
	}

	if (keyp) {
		*keyp = (unsigned char *)entry->key;
	}
	return (grm_htable_iter_t)entry;
}

grm_htable_iter_t grm_htable_begin (grm_htable_t *htbl, unsigned char **keyp)
{
	grm_htable_entry_t *entry = next_valid_bucket(htbl, htbl->buckets - 1);
	return get_key_and_iterator(entry, keyp);
}

grm_htable_iter_t grm_htable_next (grm_htable_t *htbl,
		grm_htable_iter_t iter,
		unsigned char **keyp)
{
	if (!iter) {
		return NULL;
	}

	grm_htable_entry_t *entry = (grm_htable_entry_t *)iter;
	if (entry->next) {
		return get_key_and_iterator(entry->next, keyp);
	}

	// We have finished traversing the list.  Go back to the beginning and
	// continue iterating through the bucket array.
	while (entry->prev) {
		entry = entry->prev;
	}
	entry = next_valid_bucket(htbl, entry);
	return get_key_and_iterator(entry, keyp);
}

int grm_dict_set (grm_dict_t *dict, const char *key, const void *data)
{
	return grm_htable_set(dict, (const unsigned char *)key, strlen(key), data);
}

void *grm_dict_get (grm_dict_t *dict, const char *key)
{
	return grm_htable_get(dict, (const unsigned char *)key, strlen(key));
}

int grm_dict_del (grm_dict_t *dict, const char *key)
{
	return grm_htable_del(dict, (const unsigned char *)key, strlen(key));
}

int grm_set_add (grm_set_t *set, const void *data, size_t length)
{
	return grm_htable_set(set, (const unsigned char *)data, length, data);
}

int grm_set_union (grm_set_t *s1, grm_set_t *s2)
{
	grm_htable_entry_t *entry;
	HTABLE_ENTRY_FOREACH(entry, s2) {
		if (grm_htable_set(s1, entry->key, entry->keylen, entry->value) < 0) {
			return -1;
		}
	}
	return 0;
}

int grm_set_diff (grm_set_t *s1, grm_set_t *s2)
{
	grm_htable_entry_t *entry;
	HTABLE_ENTRY_FOREACH(entry, s2) {
		grm_htable_del(s1, entry->key, entry->keylen);
	}
	return 0;
}
