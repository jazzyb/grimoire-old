#include "vector.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

struct grm_vector_ {
	size_t capacity;
	size_t num_items;
	size_t item_size;
	uint8_t *buffer;
};

#define DEFAULT_CAPACITY 8

grm_vector_t *grm_vector_alloc (size_t init_capacity, size_t item_size)
{
	grm_vector_t *new = malloc(sizeof(*new));
	if (!new) {
		return NULL;
	}
	new->capacity = (init_capacity) ? init_capacity : DEFAULT_CAPACITY;
	new->buffer = calloc(new->capacity, item_size);
	if (!new->buffer) {
		free(new);
		return NULL;
	}
	new->item_size = item_size;
	new->num_items = 0;
	return new;
}

int grm_vector_append (grm_vector_t *v, void *ptr)
{
	return grm_vector_set(v, v->num_items, ptr);
}

#define ITEM_PTR(v, index) ((void *)((v)->buffer + ((index) * (v)->item_size)))

static void zero_memory (grm_vector_t *v, size_t old_cap, size_t new_cap)
{
	memset(ITEM_PTR(v, old_cap), 0, (new_cap - old_cap) * v->item_size);
}

#define CAPACITY_MULTIPLE 2

static int maybe_extend_capacity (grm_vector_t *v, size_t index)
{
	if (index < v->capacity) {
		return 0;
	}

	size_t new_cap = v->capacity * CAPACITY_MULTIPLE;
	while (index >= new_cap) {
		new_cap *= CAPACITY_MULTIPLE;
	}
	uint8_t *tmp = realloc(v->buffer, new_cap * v->item_size);
	if (!tmp) {
		return -1;
	}
	v->buffer = tmp;
	zero_memory(v, v->capacity, new_cap);
	v->capacity = new_cap;
	return 0;
}

int grm_vector_set (grm_vector_t *v, size_t index, void *src)
{
	if (maybe_extend_capacity(v, index) < 0) {
		return -1;
	}

	void *dst = ITEM_PTR(v, index);
	memcpy(dst, src, v->item_size);
	if (index >= v->num_items) {
		v->num_items = index + 1;
	}
	return 0;
}

void *grm_vector_get (grm_vector_t *v, size_t index)
{
	if (index >= v->num_items) {
		return NULL;
	}
	return ITEM_PTR(v, index);
}

size_t grm_vector_length (grm_vector_t *v)
{
	return v->num_items;
}

void grm_vector_free (grm_vector_t *v)
{
	free(v->buffer);
	free(v);
}

void grm_vector_erase (grm_vector_t *v)
{
	zero_memory(v, 0, v->capacity);
	v->num_items = 0;
}
